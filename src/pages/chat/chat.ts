import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';

import { FotoPage } from "../foto/foto";

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  userId:any = {};
  key:any = {};
  message:string = '';
  messages: Observable<any[]>;
  usuario:any = {};
  name:any = {};
  nanyId:any;
  data:any ={};
  ordersN:any = {};
  ordersU:any = {};
  user:any;
  review:any;
  nombre:any;
  dateTime:any;
  photo:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public afDB: AngularFireDatabase,
              public afAuth:AngularFireAuth,
              public camera: Camera,
              public modalCtrl: ModalController) {

              this.userId = this.afAuth.auth.currentUser.uid;
              console.log(this.userId);
              this.key = this.navParams.get("key");
              console.log(this.key);
              this.name = this.navParams.get("name");
              this.nanyId = this.navParams.get("nanyId");
              this.user = this.navParams.get("user");


              this.messages = afDB.list('chat' + '/' + this.key).snapshotChanges().map(data => {
                return data.map(c => ({ ...c.payload.val() }));
              });


              this.afDB.object('orders/'+this.userId+'/'+this.key+'/data/').valueChanges().subscribe(data =>{
                this.ordersU = data;
                console.log(this.ordersU);
              });

              this.afDB.object('orders_n/'+this.nanyId+'/'+this.key+'/data/').valueChanges().subscribe(data =>{
                this.ordersN = data;
                console.log(this.ordersN.numChil);
              });




  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad ChatPage');
  }
  mostrar_camara(idPlayerNannyUser){

    this.camera.getPicture({
      quality : 100,
      destinationType : this.camera.DestinationType.DATA_URL,
      sourceType : this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 720,
      targetHeight: 720,
      saveToPhotoAlbum: true
    }).then(foto => {

            let nombreArchivo:string = new Date().valueOf().toString();
            let fotoref = firebase.storage().ref('usuarios/chat/'+nombreArchivo);
            fotoref.putString(foto, 'base64', {contentType: 'image/jpg'}).then(foto_guardad => {
              let foto = foto_guardad.downloadURL;
              this.sendMessagePhoto(idPlayerNannyUser,foto);
            });
    }, error => {
      // Log an error to the console if something goes wrong.
      console.log("ERROR -> " + JSON.stringify(error));
    });

  }

  verImagen(foto){
    let modal = this.modalCtrl.create( FotoPage, { 'foto': foto } );
    modal.present();
  }

  sendMessage(idPlayerNannyUser){
      this.review = this.message;
      this.nombre = this.name;
      this.dateTime = new Date().toISOString();
      this.afDB.list('/chat'+ '/' + this.key ).push({
        idUser: this.userId,
        mensaje: this.message,
        name: this.name,
        time: this.dateTime,
        type: 0
      }).then( mensaje => {
        // mensaje enviado !
        console.log('mensaje enviado !', this.review);

        this.afDB.object('users/'+this.userId+'/'+'meta/').valueChanges().subscribe(data =>{
          this.data = data;
          if(! this.data){
            this.data={}
          }

          if (this.data.user == 0) {
            console.log('mensaje enviado user !', this.review);
            let noti =  {
              app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
              include_player_ids: [idPlayerNannyUser],
              data: {"foo": "bar"},
              contents: {en: this.review},
              headings: {en: this.nombre}
            }

            window["plugins"].OneSignal.postNotification(noti,
              function(successResponse) {
                console.log("Notification Post Success:", successResponse);
              },
              function (failedResponse) {
                console.log("Notification Post Failed: ", failedResponse);
                alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
              }
            );

          }else{
            this.afDB.object('products_meta/'+this.nanyId).valueChanges().subscribe(data =>{
              this.data = data;
              if(! this.data){
                this.data={}
              }
              console.log('mensaje enviado nanny!', this.review);
              let noti =  {
                app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                include_player_ids: [idPlayerNannyUser],
                data: {"foo": "bar"},
                contents: {en: this.review},
                headings: {en: this.nombre}
              }

              window["plugins"].OneSignal.postNotification(noti,
                function(successResponse) {
                  console.log("Notification Post Success:", successResponse);
                },
                function (failedResponse) {
                  console.log("Notification Post Failed: ", failedResponse);
                  alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
                }
              );
            });
          }

          console.log(this.data.displayName);
        });

      });
      this.message = '';
  }

  sendMessagePhoto(idPlayerNannyUser,foto){
      this.review = foto;
      this.nombre = this.name;
      this.photo = foto;
      this.dateTime = new Date().toISOString();
      this.afDB.list('/chat'+ '/' + this.key ).push({
        idUser: this.userId,
        mensaje: this.photo,
        name: this.name,
        time: this.dateTime,
        type: 1
      }).then( mensaje => {
        // mensaje enviado !
        console.log('mensaje enviado !', this.review);

        this.afDB.object('users/'+this.userId+'/'+'meta/').valueChanges().subscribe(data =>{
          this.data = data;
          if(! this.data){
            this.data={}
          }

          if (this.data.user == 0) {
            console.log('mensaje enviado user !', this.review);
            let noti =  {
              app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
              include_player_ids: [idPlayerNannyUser],
              data: {"foo": "bar"},
              contents: {en: "Foto :camera:"},
              big_picture: this.review ,
              ios_attachments: {id1: this.review }

            }

            window["plugins"].OneSignal.postNotification(noti,
              function(successResponse) {
                console.log("Notification Post Success:", successResponse);
              },
              function (failedResponse) {
                console.log("Notification Post Failed: ", failedResponse);
                alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
              }
            );

          }else{
            this.afDB.object('products_meta/'+this.nanyId).valueChanges().subscribe(data =>{
              this.data = data;
              if(! this.data){
                this.data={}
              }
              console.log('mensaje enviado nanny!', this.review);
              let noti =  {
                app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                include_player_ids: [idPlayerNannyUser],
                data: {"foo": "bar"},
                contents: {en: "Foto :camera:" },
                big_picture: this.review ,
                ios_attachments: {id1: this.review }
              }

              window["plugins"].OneSignal.postNotification(noti,
                function(successResponse) {
                  console.log("Notification Post Success:", successResponse);
                },
                function (failedResponse) {
                  console.log("Notification Post Failed: ", failedResponse);
                  alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
                }
              );
            });
          }

          console.log(this.data.displayName);
        });

      });
      this.message = '';
  }

}
