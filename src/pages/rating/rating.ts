import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';

import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-rating',
  templateUrl: 'rating.html',
})
export class RatingPage {

  nanyId:any = {};
  userId:any = {};
  key:any = {};
  calif:any = {};
  nanyImage:any = {};
  nannyP:any = {};
  nannyR:any = {};
  nannyRi:any;
  valorC:any;
  data:any = {};
  valor:number;
  count:any;

  @Input() numStars:number = 5;
  @Input() value: number = 0;
  @Input() readOnly: boolean = false;
  @Output() ionClick: EventEmitter<number> = new EventEmitter<number>();

  stars:string[] = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public afDB: AngularFireDatabase) {

                this.nanyId = this.navParams.get("nanyId");
                this.userId = this.navParams.get("userId");
                this.key    = this.navParams.get("key");
                this.data.note = "";
                this.valor = 0;

                this.afDB.object('products_images/'+this.nanyId).valueChanges().subscribe(data =>{
                  this.nanyImage = data;
                  if(! this.nanyImage){
                    this.nanyImage={}
                  }
                  console.log(this.nanyImage);
                });

                this.afDB.object('products_meta/'+this.nanyId).valueChanges().subscribe(data =>{
                  this.nannyP = data;
                  if(! this.nannyP){
                    this.nannyP={}
                  }
                  console.log(this.nannyP);
                });

                this.afDB.object('rating/'+this.nanyId+'/').valueChanges().subscribe(data =>{
                  this.nannyR = data;

                  if(! this.nannyR){
                    this.nannyRi = false;
                    console.log(this.nannyRi);
                  }else{
                    this.nannyRi = true;
                    console.log(this.nannyRi);
                  }

                });

              }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RatingPage');
  }

  ngAfterViewInit(){
    this.calc();
  }

  calc(){
    this.stars = [];
    let tmp = this.value;
    for (let i = 0; i < this.numStars; i++, tmp--) {
      if(tmp >= 1)
        this.stars.push("star");
      else if(tmp > 0 && tmp < 1 )
        this.stars.push("star-half");
      else this.stars.push("star-outline");

    }
  }

  starClicked(index){
    // console.log(index);
    if(!this.readOnly) {
      this.value = index + 1;
      this.ionClick.emit(this.value);
      this.calc();
      console.log(this.value);
      this.calificar(this.value);
    }
  }

  calificar(valor){

    this.valor = valor;
    console.log('Calificación ' + valor);

  }

  guardar(valor){

    let calificacion = valor;

    this.afDB.database.ref('orders'+'/'+this.userId+'/'+this.key+'/data/').update({service: valor});

    this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+this.key+'/data/').update({service: valor});

      if(this.nannyRi == false){

          this.valorC = 5 + calificacion;
          this.count = 2;

      }else{

          this.valorC = this.nannyR.service + calificacion;
          this.count = 1 + this.nannyR.count;

      }

      let pato = './assets/imgs/icon-pato.png'

      this.afDB.database.ref('rating'+'/'+this.nanyId+'/'+this.key).update({service: this.valor, count: this.count , nota: this.data.note, pato: pato});

      this.afDB.database.ref('rating'+'/'+this.nanyId).update({service: this.valorC, count: this.count});

    this.afDB.database.ref('users'+'/'+this.userId+'/'+'meta/').update({status: 0});

    this.navCtrl.setRoot( HomePage );

  }


}
