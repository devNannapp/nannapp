import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
// import { Observable } from 'rxjs/Observable';
import { OneSignal } from '@ionic-native/onesignal';

// import { PerfilPage } from '../perfil/perfil';
import { HomePage } from '../home/home';
// import { RatingPage } from '../rating/rating';
import { StartservicePage } from '../startservice/startservice';
import { RatingPage } from '../rating/rating';


import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Platform } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  displayName: string;
  codigo : string;
  photo: string;
  usuario: any = {};
  uid : string;
  orderU:any = {};
  userId: string;
  nanyId:any = {};
  key:any = {};
  ordersU:any = {};
  nannyP:any = {};
  nanyImage:any = {};
  ordersN:any = {};
  status:any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public afDB: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private fb: Facebook,
    private platform: Platform,
    public  oneSignal: OneSignal) {

      afAuth.authState.subscribe(user => {
        if (!user) {

          this.displayName = null;
          return;

        }else{

              this.uid = this.afAuth.auth.currentUser.uid;

              this.afDB.object('users/'+this.uid+'/'+'meta').valueChanges().subscribe(data =>{
                this.usuario = data;
                if(! this.usuario){

                  this.displayName = user.displayName;
                  this.photo = user.photoURL;
                  this.codigo = user.uid;

                  const usersFirebase = this.afDB.object('/users/'+this.codigo+'/'+'meta');
                        usersFirebase.set({
                          displayName: this.displayName,
                          foto: this.photo,
                          user: 0,
                          status: 0
                        });

                  this.bienvenida();
                  this.navCtrl.setRoot( HomePage );


                }else if (this.usuario.status == 2) {

                    this.navCtrl.setRoot( StartservicePage, { 'nanyId' : this.usuario.product, 'userId' : this.uid, 'key' : this.usuario.key, 'user': this.usuario.user  } );

                }else if (this.usuario.status == 0){
                    
                    this.navCtrl.setRoot( HomePage );
                    console.log('hola');

                }else if (this.usuario.status == 5){
                    this.navCtrl.setRoot( RatingPage, { 'nanyId': this.usuario.product, 'userId': this.uid, 'key': this.usuario.key } );
                    console.log('rating');
                }
              });

        }


      });

    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  signInWithFacebook() {
    if (this.platform.is('cordova')) {
      return this.fb.login(['email', 'public_profile']).then(res => {
        const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
        return firebase.auth().signInWithCredential(facebookCredential);
      })
    }
    else {
      return this.afAuth.auth
        .signInWithPopup(new firebase.auth.FacebookAuthProvider())
        .then(res => console.log(res));
    }
  }

  signOut() {
    this.afAuth.auth.signOut();
  }



  bienvenida(){
    this.oneSignal.getIds().then((id) => {
      console.log('MI ID de la suerte'+id.userId);

          var idPlayer = id.userId;

          this.afDB.database.ref('users'+'/'+this.uid+'/').update({idPlayer});

          let noti =  {
                        app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                        include_player_ids: [id.userId],
                        data: {"foo": "bar"},
                        contents: {en: "Nuestras nannies certificadas cuidarán de los niños."},
                        headings: {en: "¡Bienvenido a Nannapp!"}
                      }

          window["plugins"].OneSignal.postNotification(noti,
      			function(successResponse) {
        				console.log("Notification Post Success:", successResponse);
      			},
      			function (failedResponse) {
        				console.log("Notification Post Failed: ", failedResponse);
      			}
    			);
    });
  }

  

}
