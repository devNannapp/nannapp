import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

import { CategoryPage } from '../category/category';
import { ReservedPage } from '../reserved/reserved';
import { HistorialPage } from '../historial/historial';

import { RequestformPage } from '../requestform/requestform';
import { AlertController } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';

import { Platform } from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public usuario = {};
  uid : string;
  tarifas :any={};
  nannyP:any = {};
  standNanny:any;
  newID:any;
  bannerImage:any = {};

  idPlayerUser:any;

  constructor(public navCtrl: NavController,
              private afAuth: AngularFireAuth,
              public alertCtrl: AlertController,
              public  oneSignal: OneSignal,
              public afDB: AngularFireDatabase,
              public plt: Platform) {
                
                this.uid = this.afAuth.auth.currentUser.uid;
                
                let route='products_meta/P201804250046482';
                this.newID=localStorage["newID"];
                console.log("*******************Local storage value = "+this.newID);

                this.afDB.object('tarifas').valueChanges().subscribe(data =>{
                  this.tarifas = data;
                  console.log(this.tarifas);
                });

                this.afDB.object('users/'+ this.uid +'/meta/standNanny').valueChanges().subscribe(data =>{
                  this.standNanny = data;
                  console.log(this.standNanny);
                });
/*
                this.afDB.object('users/'+this.uid+'/newID').valueChanges().subscribe(data =>{
                  this.newID = data;
                  console.log(this.newID);
                });
*/
                this.afDB.object('banner_images/').valueChanges().subscribe(data =>{
                  this.bannerImage = data;
                });

                this.afDB.object('users/'+this.uid+'/'+'meta').valueChanges().subscribe(data =>{
                  this.usuario = data;
                  route='products_meta/'+this.standNanny;
                  console.log('ruta '+route);
                  

                  

                  this.afDB.object(route).valueChanges().subscribe(data =>{
                    this.nannyP = data;
                    //console.log("this nannyP stand "+this.nannyP.title);
                  });
                  
                });
                
                if((plt.is('ios') || plt.is('android')) && this.newID == undefined){
                  console.log('iniciando la notificación, new ID= '+this.newID);
                  
                  this.promptNotification();
                  
                }else if((plt.is('ios') || plt.is('android'))){
                  this.startN();
                }
                
                this.afDB.object('users/'+ this.uid +'/idPlayer').valueChanges().subscribe(data =>{
                  this.idPlayerUser = data;
                  console.log(this.idPlayerUser);
                });


                

               }

  
  goToCategory(category:any){
    console.log( category );
    this.navCtrl.push( CategoryPage, { 'category' : category } );
  }

  goToReserved(nanyId, val){
    console.log(nanyId, val)
    this.navCtrl.push( ReservedPage, { 'nanyId' : nanyId, 'userId' : this.uid, 'home': val } );
  }

  goToHistorial(nanyId, val){
    console.log(nanyId, val)
    this.navCtrl.push( HistorialPage, { 'nanyId' : nanyId, 'userId' : this.uid, 'home': val } );
  }

  goRequestForm(nanyId, idPlayer, idPlayerUser){
    
      let prompt = this.alertCtrl.create({
      title: '<h4>PROGRAMA TU CUIDADO</h4>',
      message: '<h6>Numero de niños que requieren Nanny.</h6> <br>',
      inputs : [
      {
          type:'radio',
          label:'1',
          value:'1'
      },
      {
          type:'radio',
          label:'2',
          value:'2'
      },
      {
          type:'radio',
          label:'3',
          value:'3'
      }],
      buttons : [
      {
          text: "Cancelar",
          handler: childrens => {
          console.log(childrens);
          }
      },
      {
          text: "Aceptar",
          handler: childrens => {
          console.log(childrens);
          console.log("***************id Player User"+idPlayerUser);
          var price;
          if(this.nannyP.tarifa == 'BASIC'){
            price=this.tarifas.traditionalBasic;
            console.log("*****la tarifa traditional basic es "+ price);
          }else if(this.nannyP.tarifa == 'LUXE'){
            price=this.tarifas.traditionalLuxe;
            console.log("*****la tarifa traditional luxe es "+ price);
          
          }
          this.navCtrl.push( RequestformPage, { 'nanyId' : nanyId, 'childrens': childrens, 'price' : price , 'category': 'stand100', 'idPlayer': idPlayer, 'idPlayerUser': idPlayerUser} );
          }
      }]});
      prompt.present()
    
  }

  startN(){
    this.oneSignal.getIds().then((id) => {
      console.log('MI ID de la suerte'+id.userId);

          var idPlayer = id.userId;
          var newID=true;

          this.afDB.database.ref('users'+'/'+this.uid+'/').update({idPlayer});
          this.afDB.database.ref('users'+'/'+this.uid+'/').update({newID});
          
          let noti =  {
                        app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                        include_player_ids: [id.userId],
                        data: {"foo": "bar"},
                        contents: {en: "Ahora, déjanos ser tu nanny de la guarda."},
                        headings: {en: "Notificaciones activadas"}
                      }


          window["plugins"].OneSignal.postNotification(noti,
      			function(successResponse) {
        				console.log("Notification Post Success:", successResponse);
      			},
      			function (failedResponse) {
        				console.log("Notification Post Failed: ", failedResponse);
      			}
    			);
    });

  }
promptNotification(){
  let prompt = this.alertCtrl.create({
    title: '<h4>Activa notificaciones.</h4>',
    message: '<h6>Para el óptimo funcionamiento de la app requerimos que actives las notificaciones.</h6> <br>',
    
    buttons : [
    
    {
        text: "Aceptar",
        handler: childrens => {
        this.startN();
        localStorage['newID']=true;
        }
    }]});
    prompt.present()
}



}
