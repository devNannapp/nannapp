import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import { InfoPage } from '../info/info';
import { IdPage } from "../id/id";
import { ComprobantePage } from "../comprobante/comprobante";

@IonicPage()
@Component({
  selector: 'page-informacion',
  templateUrl: 'informacion.html',
})
export class InformacionPage {

  userId:any = {};
  usuario = {};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public afDB: AngularFireDatabase,
              private afAuth: AngularFireAuth,
              public  modalCtrl: ModalController) {

                this.userId = this.afAuth.auth.currentUser.uid;

                this.afDB.object('users/'+this.userId+'/'+'info/').valueChanges().subscribe(data =>{
                  this.usuario = data;
                  if(! this.usuario){
                    this.usuario={}
                  }
                  console.log(this.usuario);
                });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InformacionPage');
  }

  goInfo(){

    let modal = this.modalCtrl.create( InfoPage, { 'userId': this.userId } );
    modal.present();

  }

  close(){
  this.viewCtrl.dismiss();
  }

  modalId(){

    let modal = this.modalCtrl.create( IdPage, { 'userId': this.userId });
    modal.present();

  }

  modalComprobante(){

    let modal = this.modalCtrl.create( ComprobantePage, { 'userId': this.userId });
    modal.present();

  }

}
