import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';

import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireObject } from 'angularfire2/database/interfaces';
import { AngularFireAuth } from 'angularfire2/auth'

@Injectable()
export class UbicacionProvider {

  usuario: AngularFireObject<any>;
  uid:any;

  constructor(private afDB: AngularFireDatabase,
              private geolocation: Geolocation,
              private afAuth: AngularFireAuth) {
    console.log('Hello UbicacionProvider Provider');


  }

  iniciarUser(){

    this.uid = this.afAuth.auth.currentUser.uid;
    this.usuario = this.afDB.object( 'users/' + this.uid + '/meta/' );
    
  }

  iniciarGeolocalizacion(){

    this.geolocation.getCurrentPosition().then((resp) => {

      // resp.coords.latitude
      // resp.coords.longitude
      console.log(resp.coords);



      let watch = this.geolocation.watchPosition();

      watch.subscribe((data) => {
        // data can be a set of coordinates, or an error (if an error occurred).
        // data.coords.latitude
        // data.coords.longitude
        console.log('watch: ', data.coords );

        this.usuario.update({lat: data.coords.latitude, lng: data.coords.longitude});

      });

    }).catch((error) => {

      console.log('Error getting location', error);

    });


  }

}
