webpackJsonp([22],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NannyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__requestform_requestform__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__request_request__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__comments_comments__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__reserved_reserved__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__home_home__ = __webpack_require__(49);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










// import { Observable } from 'rxjs/Observable';
var NannyPage = /** @class */ (function () {
    function NannyPage(navCtrl, navParams, afDB, afAuth, loadingCtrl, alertCtrl, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.nannyP = {};
        this.nanyImage = {};
        this.nanyId = {};
        this.category = {};
        this.price = {};
        this.usersFa = {};
        //extra variables
        this.nanyTrip = {};
        this.tarifas = {};
        this.nanyId = this.navParams.get("nanyId");
        this.category = this.navParams.get("category");
        this.uid = this.afAuth.auth.currentUser.uid;
        this.afDB.object('tarifas').valueChanges().subscribe(function (data) {
            _this.tarifas = data;
            console.log(_this.tarifas);
        });
        this.afDB.object('users/' + this.uid + '/meta/standHours').valueChanges().subscribe(function (data) {
            _this.standHoursOfUser = data;
        });
        if (this.category == 'trip') {
            this.afDB.object('products_trip-meta/' + this.nanyId).valueChanges().subscribe(function (data) {
                _this.nanyTrip = data;
                console.log(_this.nanyTrip);
            });
        }
        console.log(this.nanyId);
        this.afDB.object('products_meta/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nannyP = data;
            if (_this.category == 'traditional') {
                //if(this.nannyP.tarifa == 'BASIC'){
                //  this.price=this.tarifas.traditionalBasic;
                //  console.log("la tarifa traditional basic es "+ this.tarifas.traditionalBasic);
                //}else if(this.nannyP.tarifa == 'LUXE'){
                _this.price = _this.tarifas.traditionalLuxe;
                console.log("la tarifa traditional luxe es " + _this.tarifas.traditionalLuxe);
                //}
            }
            else if (_this.category == 'stand') {
                //if(this.nannyP.tarifa == 'BASIC'){
                //  this.price=this.tarifas.standBasic;
                //  console.log("la tarifa day basic es "+ this.tarifas.standBasic);
                //}else if(this.nannyP.tarifa == 'LUXE'){
                _this.price = _this.tarifas.standLuxe;
                //  console.log("la tarifa day luxe es "+ this.tarifas.standLuxe);
                //}
            }
            else if (_this.category == 'dream') {
                //if(this.nannyP.tarifa == 'BASIC'){
                //  this.price=this.tarifas.dreamBasic;
                //  console.log("la tarifa dream basic es "+ this.tarifas.dreamLuxe);
                //}else if(this.nannyP.tarifa == 'LUXE'){
                _this.price = _this.tarifas.dreamLuxe;
                //console.log("la tarifa dream luxe es "+ this.tarifas.standLuxe);
                //}
            }
            else if (_this.category == 'week') {
                //if(this.nannyP.tarifa == 'BASIC'){
                //  this.price=this.tarifas.semanaBasic;
                //  console.log("la tarifa week basic es "+ this.tarifas.semanaBasic);
                //}else if(this.nannyP.tarifa == 'LUXE'){
                _this.price = _this.tarifas.semanaLuxe;
                //  console.log("la tarifa week luxe es "+ this.tarifas.semanaLuxe);
                //}
            }
            else if (_this.category == 'trip') {
                if (_this.nanyTrip.tarifa == 'NATIONAL') {
                    _this.price = _this.tarifas.tripNacional;
                    console.log("la tarifa trip nacional 1 día es " + _this.tarifas.tripNacional);
                }
                else if (_this.nanyTrip.tarifa == 'INTERNATIONAL') {
                    _this.price = _this.tarifas.tripInternacional;
                    console.log("la tarifa trip internacional 1 día es " + _this.tarifas.tripInternacional);
                }
            }
            else if (_this.category == 'party') {
                if (_this.nannyP.tarifa == 'BASIC') {
                }
                else if (_this.nannyP.tarifa == 'LUXE') {
                }
            }
            else if (_this.category == 'special') {
                _this.price = _this.tarifas.special;
                console.log("la tarifa special es " + _this.tarifas.special);
            }
            else if (_this.category == 'grand') {
                _this.price = _this.tarifas.grand;
                console.log("la tarifa grand es " + _this.tarifas.grand);
            }
            else if (_this.category == 'stand100') {
                //if(this.nannyP.tarifa == 'BASIC'){
                //  this.price=this.tarifas.traditionalBasic;
                //  console.log("la tarifa traditional basic es "+ this.tarifas.traditionalBasic);
                //}else if(this.nannyP.tarifa == 'LUXE'){
                _this.price = _this.tarifas.traditionalLuxe;
                //  console.log("la tarifa traditional luxe es "+ this.tarifas.traditionalLuxe);
                //}
            }
            /*if(this.category == 'dream'){
              this.price = this.nannyP.tarifaDre;
              console.log(this.price);
            }else if(this.category == 'traditional'){
              this.price = this.nannyP.tarifaTra;
              console.log(this.price);
            }else if(this.category == 'stand'){
              this.price = this.nannyP.tarifaSta;
              console.log(this.price);
            }else if(this.category == 'special'){
              this.price = this.nannyP.tarifaSG;
              console.log(this.price);
            }else if(this.category == 'grand'){
              this.price = this.nannyP.tarifaSG;
              console.log(this.price);
            }
            console.log(this.nannyP);*/
        });
        this.afDB.object('rating/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nanyRo = data;
            if (!_this.nanyRo) {
                _this.promedio = '4.90';
            }
            else {
                var resultado = (Math.round(_this.nanyRo.service / _this.nanyRo.count * 100) / 100).toString();
                if ((_this.nanyRo.service / _this.nanyRo.count * 100) % 100 == 0) {
                    resultado += '.00';
                }
                _this.promedio = resultado;
                console.log(_this.nanyRo.service);
            }
        });
        this.nanyR = afDB.list('rating/' + this.nanyId).snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.afDB.object('products_images/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nanyImage = data;
            console.log(_this.nanyImage);
        });
        this.afDB.object('users/' + this.uid + '/favorite/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.usersFa = data;
            if (!_this.usersFa) {
                _this.usersFa = false;
            }
            console.log(_this.usersFa);
        });
        this.afDB.object('users/' + this.uid + '/idPlayer').valueChanges().subscribe(function (data) {
            _this.idPlayerUser = data;
            console.log(_this.idPlayerUser);
        });
        //   this.usersFa = afDB.list('users/' + this.uid + '/favorite' + this.nanyId).snapshotChanges().map(data => {
        //     return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        //   });
    }
    NannyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NannyPage');
    };
    NannyPage.prototype.goRequest = function (nanyId) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__request_request__["a" /* RequestPage */], { 'nanyId': nanyId });
    };
    NannyPage.prototype.goRequestForm = function (nanyId, idPlayer, idPlayerUser) {
        var _this = this;
        if (this.category != 'grand' && this.category != 'stand') {
            var prompt_1 = this.alertCtrl.create({
                title: '<h4>PROGRAMA TU CUIDADO</h4>',
                message: '<h6>Numero de niños que requieren Nanny.</h6> <br>',
                inputs: [
                    {
                        type: 'radio',
                        label: '1',
                        value: '1'
                    },
                    {
                        type: 'radio',
                        label: '2',
                        value: '2'
                    },
                    {
                        type: 'radio',
                        label: '3',
                        value: '3'
                    }
                ],
                buttons: [
                    {
                        text: "Cancelar",
                        handler: function (childrens) {
                            console.log(childrens);
                        }
                    },
                    {
                        text: "Aceptar",
                        handler: function (childrens) {
                            console.log(childrens);
                            console.log(idPlayerUser);
                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__requestform_requestform__["a" /* RequestformPage */], { 'nanyId': nanyId, 'childrens': childrens, 'price': _this.price, 'category': _this.category, 'idPlayer': idPlayer, 'idPlayerUser': idPlayerUser });
                        }
                    }
                ]
            });
            prompt_1.present();
        }
        else if (this.category == 'grand') {
            var prompt_2 = this.alertCtrl.create({
                title: '<h4>PROGRAMA TU CUIDADO</h4>',
                message: '<h6>Numero de adultos mayores que requieren Nanny.</h6> <br>',
                inputs: [
                    {
                        type: 'radio',
                        label: '1',
                        value: '1'
                    },
                    {
                        type: 'radio',
                        label: '2',
                        value: '2'
                    }
                ],
                buttons: [
                    {
                        text: "Cancelar",
                        handler: function (childrens) {
                            console.log(childrens);
                        }
                    },
                    {
                        text: "Aceptar",
                        handler: function (childrens) {
                            console.log(childrens);
                            console.log(idPlayerUser);
                            console.log("nanny.ts  precio pasado " + _this.price);
                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__requestform_requestform__["a" /* RequestformPage */], { 'nanyId': nanyId, 'childrens': childrens, 'price': _this.price, 'category': _this.category, 'idPlayer': idPlayer, 'idPlayerUser': idPlayerUser });
                        }
                    }
                ]
            });
            prompt_2.present();
        }
        else {
            var prompt_3 = this.alertCtrl.create({
                title: '<h4>PAGA TU PAQUETE</h4>',
                message: '<h6>Al pagar tu paquete podrás empezar a reservar y usar tus horas de cuidado.</h6> <br>',
                inputs: [
                    {
                        type: 'radio',
                        label: '50 horas',
                        value: '50'
                    },
                    {
                        type: 'radio',
                        label: '100 horas',
                        value: '100'
                    }
                ],
                buttons: [
                    {
                        text: "Cancelar",
                    },
                    {
                        text: "Aceptar",
                        handler: function (childrens) {
                            if (childrens == 50) {
                                _this.price = _this.tarifas.month;
                            }
                            else {
                                _this.price = _this.tarifas.standLuxe;
                            }
                            var pagado = false;
                            var key = _this.afDB.createPushId();
                            var data = {
                                idUser: _this.uid,
                                idNanny: _this.nanyId,
                                tarifa: _this.price,
                                category: _this.category,
                                costo: _this.price,
                                //pago:         'payment',
                                status: 0,
                                horas: childrens
                            };
                            _this.afDB.database.ref('orders' + '/' + _this.uid + '/' + key).set({ data: data });
                            //this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+ key).set({data});
                            if (!pagado) {
                                var standHours = _this.standHoursOfUser;
                                if (data.horas == 50) {
                                    standHours = 50 + parseInt(_this.standHoursOfUser);
                                }
                                else {
                                    standHours = 100 + parseInt(_this.standHoursOfUser);
                                }
                                console.log("******************** horas stand pasadas de la nanny" + standHours);
                                var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__request_request__["a" /* RequestPage */], { 'nanyId': _this.nanyId, 'userId': _this.uid, 'key': key, 'standHours': standHours });
                                modal.present();
                            }
                            else {
                                var modal = _this.modalCtrl.create(/*ReservedPage*/ __WEBPACK_IMPORTED_MODULE_8__home_home__["a" /* HomePage */], { 'nanyId': _this.nanyId, 'userId': _this.uid });
                                modal.present();
                                var status_1 = 3;
                                var standNanny = _this.nanyId;
                                var standHours = _this.standHoursOfUser;
                                if (data.horas == 50) {
                                    standHours = 50 + parseInt(_this.standHoursOfUser);
                                }
                                else {
                                    standHours = 100 + parseInt(_this.standHoursOfUser);
                                }
                                _this.afDB.database.ref('users/' + _this.uid + '/meta').update({ standNanny: standNanny });
                                _this.afDB.database.ref('users/' + _this.uid + '/meta').update({ standHours: standHours });
                                _this.afDB.database.ref('orders' + '/' + _this.uid + '/' + key + '/data').update({ status: status_1 });
                            }
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__reserved_reserved__["a" /* ReservedPage */], { 'nanyId': _this.nanyId, 'userId': _this.uid });
                        }
                    }
                ]
            });
            prompt_3.present();
        }
    };
    NannyPage.prototype.favorite = function (valor) {
        this.uid = this.afAuth.auth.currentUser.uid;
        this.usuario = this.afDB.object('users/' + this.uid + '/favorite/' + this.nanyId);
        this.usuario.update({ marked: valor });
    };
    NannyPage.prototype.goComments = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__comments_comments__["a" /* CommentsPage */], { 'nanyId': this.nanyId });
        modal.present();
    };
    NannyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-nanny',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/nanny/nanny.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>nanny</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6>\n        <img src="{{nanyImage.screenshot1}}" style="width:90%;border-radius:50%;">\n          <br><br>\n          <small id="promedio">{{promedio}}</small> <img src="./assets/imgs/icon-pato.png" id="icon-pato">\n          <ion-icon id="icon" ios="ios-chatbubbles" md="md-chatbubbles" (click)="goComments()"></ion-icon>\n      </ion-col>\n      <ion-col col-6>\n        <div text-center>\n          <br>\n          <h6>{{nannyP.title}}</h6>\n\n\n          <div *ngIf="category==\'traditional\'">\n            <h3 style="color:#e2c81f">{{nannyP.nickname}}</h3>\n          </div>\n          <div *ngIf="category==\'day\'">\n              <h3 style="color:#ff9100">{{nannyP.nickname}}</h3>\n          </div>\n          <div *ngIf="category==\'stand\'">\n            <h3 style="color:#ff006f">{{nannyP.nickname}}</h3>\n          </div>\n          <div *ngIf="category==\'dream\'">\n            <h3 style="color:#8a00ff">{{nannyP.nickname}}</h3>\n          </div>\n          \n          <div *ngIf="category==\'trip\'">\n              <h3 style="color:#97bb2c">{{nannyP.nickname}}</h3>\n          </div>\n          <div *ngIf="category==\'party\'">\n              <h3 style="color:#ff7800">{{nannyP.nickname}}</h3>\n          </div>\n          <div *ngIf="category==\'special\'">\n              <h3 style="color:#008aff">{{nannyP.nickname}}</h3>\n          </div>\n          <div *ngIf="category==\'grand\'">\n              <h3 style="color:#860133">{{nannyP.nickname}}</h3>\n          </div>\n          <h6> <small>{{nannyP.certificado}}</small> </h6>\n        </div>\n      </ion-col>\n    </ion-row>\n    <br><br>\n    <ion-row>\n      <ion-col col-6>\n        <h6>Profesión:</h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{nannyP.description}}</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6>Idiomas:</h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{nannyP.idioma}}</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6>Edad:</h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{nannyP.age}} años</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6>Experiencia:</h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{nannyP.tagsString}}</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6>Edad a cuidar:</h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{nannyP.childrenage}} años</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6>Pet friendly:</h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{nannyP.pet}}</h6>\n      </ion-col>\n    </ion-row>\n    <!--\n    <ion-row>\n        <ion-col col-6>\n          <h6>Zonas de servicio:</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{nannyP.zonas}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col col-6>\n            <h6>Horarios de servicio:</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{nannyP.horarios}}</h6>\n          </ion-col>\n        </ion-row>\n\n        -->\n  </ion-grid>\n</ion-content>\n<ion-footer>\n  <ion-toolbar>\n    <ion-grid>\n      <ion-row>\n        <ion-col col-4>\n          <div *ngIf="usersFa.marked == false || usersFa == false " text-center (click)="favorite(true)">\n            Nanny Favorita\n            <ion-icon class="false" name="heart-outline" md="md-heart-outline"></ion-icon>\n          </div>\n          <div *ngIf="usersFa.marked == true" text-center (click)="favorite(false)">\n            Nanny Favorita\n            <ion-icon class="true" name="heart" md="md-heart"></ion-icon>\n          </div>\n        </ion-col>\n        <ion-col col-4></ion-col>\n        <ion-col col-4>\n          <div (click)="goRequestForm(nanyId,nannyP.idPlayer,idPlayerUser)" text-center>\n            Solicitar\n            <ion-icon class="solicitar" ios="ios-arrow-round-forward" md="md-arrow-round-forward"></ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/nanny/nanny.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], NannyPage);
    return NannyPage;
}());

//# sourceMappingURL=nanny.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IdPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var IdPage = /** @class */ (function () {
    function IdPage(navCtrl, navParams, viewCtrl, afDB, afAuth, camera, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.camera = camera;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.userId = {};
        this.dataUser = {};
        this.userId = this.navParams.get("userId");
        console.log(this.userId);
        this.typeUser = this.afAuth.auth.currentUser.uid;
        this.afDB.object('users/' + this.userId + '/' + 'info/id').valueChanges().subscribe(function (data) {
            _this.data = data;
            if (!_this.data) {
                _this.data = false;
            }
            console.log(_this.data);
        });
        this.afDB.object('users/' + this.typeUser + '/meta/user').valueChanges().subscribe(function (data) {
            _this.dataUser = data;
            if (!_this.dataUser) {
                _this.dataUser = false;
            }
            console.log(_this.dataUser);
        });
    }
    IdPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad IdPage');
    };
    IdPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    IdPage.prototype.mostrar_camara = function () {
        var _this = this;
        this.camera.getPicture({
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 720,
            targetHeight: 720,
            saveToPhotoAlbum: true
        }).then(function (foto) {
            _this.mostrar_toast('Subiendo archivo');
            var nombreArchivo = new Date().valueOf().toString();
            var fotoref = __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref('usuarios/comprobante/' + nombreArchivo);
            fotoref.putString(foto, 'base64', { contentType: 'image/jpg' }).then(function (foto_guardad) {
                __WEBPACK_IMPORTED_MODULE_5_firebase__["database"]().ref('users/' + _this.userId + '/info').update({ id: foto_guardad.downloadURL });
                _this.mostrar_toast('Archivo subido con exito');
            });
        }, function (error) {
            // Log an error to the console if something goes wrong.
            console.log("ERROR -> " + JSON.stringify(error));
        });
    };
    IdPage.prototype.seleccionar_foto = function () {
        var _this = this;
        this.camera.getPicture({
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 720,
            targetHeight: 720,
            saveToPhotoAlbum: true
        }).then(function (foto) {
            _this.mostrar_toast('Subiendo archivo');
            var nombreArchivo = new Date().valueOf().toString();
            var fotoref = __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref('usuarios/comprobante/' + nombreArchivo);
            fotoref.putString(foto, 'base64', { contentType: 'image/jpg' }).then(function (foto_guardad) {
                __WEBPACK_IMPORTED_MODULE_5_firebase__["database"]().ref('usuarios/' + _this.userId + '/info').update({ id: foto_guardad.downloadURL });
                _this.mostrar_toast('Archivo subido con exito');
            });
        }, function (error) {
            // Log an error to the console if something goes wrong.
            console.log("ERROR -> " + JSON.stringify(error));
        });
    };
    IdPage.prototype.mostrar_toast = function (mensaje) {
        this.toastCtrl.create({
            message: mensaje,
            duration: 2000
        }).present();
    };
    IdPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-id',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/id/id.html"*/'\n<ion-header>\n  <ion-navbar>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>id</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n    <ion-item>\n      <div *ngIf="data">\n        <img src="{{data}}">\n      </div>\n      <div *ngIf="!data">\n          <lazy-img inputSrc="./assets/imgs/ine.png"></lazy-img>\n      </div>\n    </ion-item>\n  </ion-list>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col *ngIf="dataUser != 1">\n        <button ion-button block icon-left color="dark" (click)="mostrar_camara()"><ion-icon name="camera"></ion-icon>Camara</button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/id/id.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */]])
    ], IdPage);
    return IdPage;
}());

//# sourceMappingURL=id.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComprobantePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ComprobantePage = /** @class */ (function () {
    function ComprobantePage(navCtrl, navParams, viewCtrl, afDB, afAuth, camera, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.camera = camera;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.userId = {};
        this.dataUser = {};
        this.userId = this.navParams.get("userId");
        this.typeUser = this.afAuth.auth.currentUser.uid;
        this.afDB.object('users/' + this.userId + '/' + 'info/comprobante').valueChanges().subscribe(function (data) {
            _this.data = data;
            if (!_this.data) {
                _this.data = false;
            }
            console.log(_this.data);
        });
        this.afDB.object('users/' + this.typeUser + '/meta/user').valueChanges().subscribe(function (data) {
            _this.dataUser = data;
            if (!_this.dataUser) {
                _this.dataUser = false;
            }
            console.log(_this.dataUser, 'que es esto');
        });
    }
    ComprobantePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ComprobantePage');
    };
    ComprobantePage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    ComprobantePage.prototype.mostrar_camara = function () {
        var _this = this;
        this.camera.getPicture({
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 720,
            targetHeight: 720,
            saveToPhotoAlbum: true
        }).then(function (foto) {
            _this.mostrar_toast('Subiendo archivo');
            var nombreArchivo = new Date().valueOf().toString();
            var fotoref = __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref('usuarios/comprobante/' + nombreArchivo);
            fotoref.putString(foto, 'base64', { contentType: 'image/jpg' }).then(function (foto_guardad) {
                __WEBPACK_IMPORTED_MODULE_5_firebase__["database"]().ref('users/' + _this.userId + '/info').update({ comprobante: foto_guardad.downloadURL });
                _this.mostrar_toast('Archivo subido con exito');
            });
        }, function (error) {
            // Log an error to the console if something goes wrong.
            console.log("ERROR -> " + JSON.stringify(error));
        });
    };
    ComprobantePage.prototype.seleccionar_foto = function () {
        var _this = this;
        this.camera.getPicture({
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 720,
            targetHeight: 720,
            saveToPhotoAlbum: true
        }).then(function (foto) {
            _this.mostrar_toast('Subiendo archivo');
            var nombreArchivo = new Date().valueOf().toString();
            var fotoref = __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref('usuarios/comprobante/' + nombreArchivo);
            fotoref.putString(foto, 'base64', { contentType: 'image/jpg' }).then(function (foto_guardad) {
                __WEBPACK_IMPORTED_MODULE_5_firebase__["database"]().ref('usuarios/' + _this.userId + '/info').update({ comprobante: foto_guardad.downloadURL });
                _this.mostrar_toast('Archivo subido con exito');
            });
        }, function (error) {
            // Log an error to the console if something goes wrong.
            console.log("ERROR -> " + JSON.stringify(error));
        });
    };
    ComprobantePage.prototype.mostrar_toast = function (mensaje) {
        this.toastCtrl.create({
            message: mensaje,
            duration: 2000
        }).present();
    };
    ComprobantePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-comprobante',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/comprobante/comprobante.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>comprobante</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n    <ion-item>\n      <div *ngIf="data">\n        <img src="{{data}}">\n      </div>\n      <div *ngIf="!data">\n        <lazy-img inputSrc="./assets/imgs/comprobante.png"></lazy-img>\n      </div>\n    </ion-item>\n  </ion-list>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col *ngIf="dataUser != 1">\n        <button ion-button block icon-left color="dark" (click)="mostrar_camara()"><ion-icon name="camera"></ion-icon>Camara</button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/comprobante/comprobante.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */]])
    ], ComprobantePage);
    return ComprobantePage;
}());

//# sourceMappingURL=comprobante.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistorialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal_modal__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__startservice_startservice__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__requestform_requestform__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__ = __webpack_require__(15);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HistorialPage = /** @class */ (function () {
    function HistorialPage(navCtrl, navParams, afDB, afAuth, modalCtrl, viewCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.usuario = {};
        this.nanyId = {};
        this.nannyP = {};
        this.ordersN = {};
        this.ordersU = {};
        this.usua = {};
        this.home = {};
        this.nanyId = this.navParams.get("nanyId");
        console.log('El id de la nany', this.nanyId);
        this.userId = this.afAuth.auth.currentUser.uid;
        console.log(this.userId);
        this.home = this.navParams.get("home");
        console.log(this.home);
        this.afDB.object('products_meta/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nannyP = data;
            console.log(_this.nannyP);
        });
        this.afDB.object('users/' + this.userId + '/' + 'meta').valueChanges().subscribe(function (data) {
            _this.usuario = data;
            console.log(_this.usuario);
        });
        this.usua = afDB.list('users/').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.ordersN = afDB.list('orders_n/' + this.nanyId).snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.ordersU = afDB.list('orders/' + this.userId).snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.items = afDB.list('products_meta/').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
    }
    HistorialPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReservedPage');
        this.viewCtrl.showBackButton(false);
    };
    HistorialPage.prototype.modal_detalles = function (key, userId) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__modal_modal__["a" /* ModalPage */], { 'nanyId': this.nanyId, 'userId': this.userId, 'key': key });
        modal.present();
    };
    HistorialPage.prototype.modal_detallesU = function (key, nanyId) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__modal_modal__["a" /* ModalPage */], { 'nanyId': nanyId, 'userId': this.userId, 'key': key });
        modal.present();
    };
    HistorialPage.prototype.aceptService = function (key, userId) {
        var status = 1;
        this.afDB.database.ref('orders' + '/' + userId + '/' + key + '/data/').update({ status: status });
        this.afDB.database.ref('orders_n' + '/' + this.nanyId + '/' + key + '/data/').update({ status: status });
    };
    HistorialPage.prototype.cancelService = function (key, userId) {
        var status = 4;
        this.afDB.database.ref('orders' + '/' + userId + '/' + key + '/data/').update({ status: status });
        this.afDB.database.ref('orders_n' + '/' + this.nanyId + '/' + key + '/data/').update({ status: status });
    };
    HistorialPage.prototype.startService = function (key, userId) {
        var status = 2;
        var product = this.nanyId;
        this.afDB.database.ref('orders' + '/' + userId + '/' + key + '/data/').update({ status: status });
        this.afDB.database.ref('orders_n' + '/' + this.nanyId + '/' + key + '/data/').update({ status: status });
        this.afDB.database.ref('users' + '/' + userId + '/' + 'meta/').update({ status: status, key: key, product: product });
        this.afDB.database.ref('users' + '/' + this.userId + '/' + 'meta/').update({ status: status, key: key, product: product });
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__startservice_startservice__["a" /* StartservicePage */], { 'nanyId': this.nanyId, 'userId': userId, 'key': key });
    };
    HistorialPage.prototype.goToHome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    HistorialPage.prototype.goRequestForm = function (nanyId, childrens, price, category) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__requestform_requestform__["a" /* RequestformPage */], { 'nanyId': nanyId, 'childrens': childrens, 'price': price, 'category': category });
    };
    HistorialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-historial',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/historial/historial.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle hideBackButton="true">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>historial</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div *ngIf="usuario.user == 0">\n    <div *ngFor="let orderU of ordersU | async">\n      <div *ngIf="orderU.data.status == 3 || orderU.data.status == 4">\n        \n        <ion-card>\n          <ion-card-header>\n            <ion-title>\n              <ion-grid>\n                <!-- <ion-row>\n                  <ion-col col-4></ion-col>\n                  <ion-col col-4>\n                      Estatus\n                  </ion-col>\n                  <ion-col col-4></ion-col>\n                </ion-row> -->\n                <ion-row>\n                  <ion-col col-12>\n                    <div *ngIf="orderU.data.status == 0">\n                      <button disabled ion-button round color="dark">Pendiente</button>\n                    </div>\n                    <div *ngIf="orderU.data.status == 1">\n                      <button disabled ion-button round color="secondary">Aceptado</button>\n                    </div>\n                    <div *ngIf="orderU.data.status == 4">\n                      <button disabled ion-button round color="danger">Cancelado</button>\n                    </div>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n              <ion-grid>\n                <ion-row>\n                  <ion-col>\n                    <div text-wrap *ngFor="let item of items | async">\n                      <div *ngIf="item.key == orderU.data.idNanny">\n                        {{item.title}}\n                      </div>\n                    </div>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-title>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-grid>\n              <ion-row>\n                <ion-col col-12>\n                  <div text-center>\n                    <p style="text-transform: uppercase;">\n                      Reservado para el día {{ orderU.data.myDate | date: \'dd-MM-yyyy\' }} a las {{ orderU.data.myTime }}.\n                    </p>\n                  </div>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-6>\n                  <button ion-button block color="dark" (click)="modal_detallesU(orderU.key, orderU.data.idNanny)">Detalles</button>\n                </ion-col>\n                <ion-col col-6>\n                  <button ion-button block class="reordenar" (click)="goRequestForm(orderU.data.idNanny, orderU.data.numChil,orderU.data.tarifa,orderU.data.category)">Reordenar</button>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-card-content>\n        </ion-card>\n      </div>\n    </div>\n  </div>\n  <div *ngIf="usuario.user == 1">\n    <div *ngFor="let orderN of ordersN | async">\n      <div *ngIf="orderN.data.status == 3 || orderN.data.status == 4">\n        <ion-card>\n          <ion-card-header>\n            <ion-title>\n              <ion-row>\n                <ion-col col-12>\n                  <div *ngIf="orderN.data.status == 0">\n                    <button disabled ion-button round color="dark">Pendiente</button>\n                  </div>\n                  <div *ngIf="orderN.data.status == 1">\n                    <button disabled ion-button round color="secondary">Aceptado</button>\n                  </div>\n                  <div *ngIf="orderN.data.status == 4">\n                    <button disabled ion-button round color="danger">Cancelado</button>\n                  </div>\n                </ion-col>\n              </ion-row>\n              <div text-wrap *ngFor="let us of this.usua | async">\n                <div *ngIf="orderN.data.idUser == us.key">\n                  {{us.meta.displayName }}\n                </div>\n              </div>\n            </ion-title>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-grid>\n              <ion-row>\n                <ion-col col-12>\n                  <div text-center>\n                    <p style="text-transform: uppercase;">\n                      Reservado para el día <br> {{orderN.data.myDate | date: \'dd-MM-yyyy\'}} a las {{orderN.data.myTime}}.\n                    </p>\n                  </div>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>\n                  <div text-wrap *ngFor="let us of this.usua | async">\n                    <div *ngIf="orderN.data.idUser == us.key">\n                      <button ion-button block color="dark" (click)="modal_detalles(orderN.key,us.key)">Detalles</button>\n                    </div>\n                  </div>\n                </ion-col>\n                <ion-col col-4>\n                  <div *ngIf="orderN.data.status == 0">\n                    <div text-wrap *ngFor="let us of this.usua | async">\n                      <div *ngIf="orderN.data.idUser == us.key">\n                        <button ion-button block color="secondary" (click)="aceptService(orderN.key,us.key)">Aceptar</button>\n                      </div>\n                    </div>\n                  </div>\n                  <div *ngIf="orderN.data.status == 1">\n                    <div text-wrap *ngFor="let us of this.usua | async">\n                      <div *ngIf="orderN.data.idUser == us.key">\n                        <button ion-button block color="dark" (click)="startService(orderN.key,us.key)">Comenzar</button>\n                      </div>\n                    </div>\n                  </div>\n                </ion-col>\n                <!-- <ion-col col-4>\n                  <div text-wrap *ngFor="let us of this.usua | async">\n                    <div *ngIf="orderN.data.idUser == us.key">\n                      <button ion-button block color="danger" (click)="cancelService(orderN.key,us.key)">Cancelar</button>\n                    </div>\n                  </div>\n                </ion-col> -->\n              </ion-row>\n            </ion-grid>\n          </ion-card-content>\n        </ion-card>\n      </div>\n    </div>\n  </div>\n</ion-content>\n<div *ngIf="home == \'home\'">\n  <ion-footer>\n    <ion-toolbar>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-12>\n            <div (click)="goToHome()" text-center>\n              <ion-icon ios="ios-home" md="md-home"></ion-icon>\n              NANNAPP\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-toolbar>\n  </ion-footer>\n</div>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/historial/historial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
    ], HistorialPage);
    return HistorialPage;
}());

//# sourceMappingURL=historial.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InformacionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__info_info__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__id_id__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__comprobante_comprobante__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var InformacionPage = /** @class */ (function () {
    function InformacionPage(navCtrl, navParams, viewCtrl, afDB, afAuth, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.modalCtrl = modalCtrl;
        this.userId = {};
        this.usuario = {};
        this.userId = this.afAuth.auth.currentUser.uid;
        this.afDB.object('users/' + this.userId + '/' + 'info/').valueChanges().subscribe(function (data) {
            _this.usuario = data;
            if (!_this.usuario) {
                _this.usuario = {};
            }
            console.log(_this.usuario);
        });
    }
    InformacionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InformacionPage');
    };
    InformacionPage.prototype.goInfo = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__info_info__["a" /* InfoPage */], { 'userId': this.userId });
        modal.present();
    };
    InformacionPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    InformacionPage.prototype.modalId = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__id_id__["a" /* IdPage */], { 'userId': this.userId });
        modal.present();
    };
    InformacionPage.prototype.modalComprobante = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__comprobante_comprobante__["a" /* ComprobantePage */], { 'userId': this.userId });
        modal.present();
    };
    InformacionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-informacion',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/informacion/informacion.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>información</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-row>\n      <ion-col col-6>\n        <h6><b>Ciudad</b></h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{usuario.ciudad}}</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6><b>Delegación</b></h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{usuario.delegacion}}</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6><b>Colonia</b></h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{usuario.colonia}}</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6><b>Calle</b></h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{usuario.calle}}</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6><b>Número exterior</b></h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{usuario.numEx}}</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6><b>Número interior</b></h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{usuario.numIn}}</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6><b>Código postal</b></h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{usuario.cp}}</h6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <h6><b>Teléfono celular</b></h6>\n      </ion-col>\n      <ion-col col-6>\n        <h6 style="font-weight:normal">{{usuario.movil}}</h6>\n      </ion-col>\n    </ion-row>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col col-12>\n          <button ion-button block color="dark" (click)="goInfo()">Editar perfil.</button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <br><br>\n    <ion-grid>\n      <ion-row>\n        <ion-col col-6>\n          <h6><b>Identificación oficial</b></h6>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <ion-icon class="ion-icon" name="contact" md="md-contact" (click)="modalId()"></ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n      <br>\n      <ion-row>\n        <ion-col col-6>\n          <h6><b>Comprob. de domicilio</b></h6>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center >\n            <ion-icon class="ion-icon" name="paper" md="md-paper" (click)="modalComprobante()"></ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/informacion/informacion.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], InformacionPage);
    return InformacionPage;
}());

//# sourceMappingURL=informacion.js.map

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__cache_img_cache_img_service__ = __webpack_require__(571);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__cache_img_cache_img_service__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__nanny_nanny__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__informacion_informacion__ = __webpack_require__(105);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CategoryPage = /** @class */ (function () {
    function CategoryPage(navCtrl, navParams, afDB, afAuth, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.modalCtrl = modalCtrl;
        this.category = {};
        this.filterNum = 1;
        this.category = this.navParams.get("category");
        console.log(this.category);
        this.uid = this.afAuth.auth.currentUser.uid;
        //if(this.filterNum==0){
        //this.itemsTraditional = afDB.list('products_traditional_meta',ref => ref.orderByChild('nickname')).snapshotChanges().map(data => {
        //  return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        //});
        //}else{
        //this.itemsTraditional = afDB.list('products_traditional_meta',ref => ref.orderByChild('age')).snapshotChanges().map(data => {
        //return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        //});
        //}
        this.itemsTraditional = afDB.list('products_traditional_meta').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.itemsSpecial = afDB.list('products_special_meta').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.itemsGrand = afDB.list('products_grand_meta').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.itemsTrip = afDB.list('products_trip-meta').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.itemsStand = afDB.list('products_stand_meta').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.itemsStandSemana = afDB.list('products_standSemana_meta').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.itemsDream = afDB.list('products_dream_meta').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.itemsParty = afDB.list('products_party_meta').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.items = afDB.list('products_meta').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.itemsImage = afDB.list('products_images').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.usersFa = afDB.list('users/' + this.uid + '/favorite').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.afDB.object('users/' + this.uid + '/' + 'orden/').valueChanges().subscribe(function (data) {
            _this.data = data;
            if (!_this.data) {
                _this.data = false;
            }
            console.log(_this.data);
        });
    }
    CategoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CategoryPage');
    };
    CategoryPage.prototype.nanny = function (nanyId) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__nanny_nanny__["a" /* NannyPage */], { 'nanyId': nanyId, 'category': this.category });
    };
    CategoryPage.prototype.favorite = function (key) {
        this.uid = this.afAuth.auth.currentUser.uid;
        this.usuario = this.afDB.object('users/' + this.uid + '/favorite/' + key);
        this.usuario.update({ marked: true });
    };
    // goToCategory(category:any){
    //   console.log( category );
    //   this.navCtrl.push( CategoryPage, { 'category' : category } );
    // }
    CategoryPage.prototype.goToInfo = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__informacion_informacion__["a" /* InformacionPage */], { 'userId': this.uid });
        modal.present();
    };
    CategoryPage.prototype.filterBy = function () {
        //0 by nickname 
        //1 by age
        //2 antiguedad
        if (this.filterNum == 1) {
            this.filterNum = 0;
        }
        else {
            this.filterNum = 1;
        }
        if (this.filterNum == 0) {
            this.itemsTraditional = this.afDB.list('products_traditional_meta', function (ref) { return ref.orderByChild('nickname'); }).snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsSpecial = this.afDB.list('products_special_meta', function (ref) { return ref.orderByChild('nickname'); }).snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsGrand = this.afDB.list('products_grand_meta', function (ref) { return ref.orderByChild('nickname'); }).snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsTrip = this.afDB.list('products_trip-meta', function (ref) { return ref.orderByChild('nickname'); }).snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsStand = this.afDB.list('products_stand_meta', function (ref) { return ref.orderByChild('nickname'); }).snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsStandSemana = this.afDB.list('products_standSemana_meta', function (ref) { return ref.orderByChild('nickname'); }).snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsDream = this.afDB.list('products_dream_meta', function (ref) { return ref.orderByChild('nickname'); }).snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsParty = this.afDB.list('products_party_meta', function (ref) { return ref.orderByChild('nickname'); }).snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
        }
        else if (this.filterNum == 1) {
            this.itemsTraditional = this.afDB.list('products_traditional_meta').snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsSpecial = this.afDB.list('products_special_meta').snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsGrand = this.afDB.list('products_grand_meta').snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsTrip = this.afDB.list('products_trip-meta').snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsStand = this.afDB.list('products_stand_meta').snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsStandSemana = this.afDB.list('products_standSemana_meta').snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsDream = this.afDB.list('products_dream_meta').snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
            this.itemsParty = this.afDB.list('products_party_meta').snapshotChanges().map(function (data) {
                return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
            });
        }
    };
    CategoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-category',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/category/category.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{category}}</ion-title>\n    \n  </ion-navbar>\n \n</ion-header>\n\n\n<ion-content padding>\n\n  <div *ngIf="data == false">\n    <div text-center>\n      Completa tus datos personales para poder solicitar un cuidado. <br>\n      <button  ion-button (click)="goToInfo()" block color="dark"><ion-icon ios="ios-contact" md="md-contact"></ion-icon></button>\n    </div>\n  </div>\n  <!--<div>\n          <button ion-fab (click)="filterBy()"><ion-icon ios="ios-reorder" md="md-reorder"></ion-icon></button>\n  </div>\n  -->\n          <div *ngIf="category == \'traditional\'">\n              <div *ngIf="data != false">\n                  <ion-grid class="card-background-page">\n                    <ion-row>\n              <ion-col col-6 *ngFor="let item of itemsTraditional | async">\n                \n                <div *ngIf="item != null">\n                  <div>\n                    <br><br>\n                    <ion-card >\n                      <div *ngFor="let itemImage of itemsImage | async">\n                      <div *ngIf="item.key == itemImage.key">\n                        <ion-card class="type">\n                            <img src="{{itemImage.screenshot1}}" (click)="nanny(item.key)" style="border-radius:0px;"/>\n                          <!--<div class="card-title">{{item.tarifa}}</div>-->\n                        </ion-card>\n                      </div>\n                      </div>\n                      <ion-card-content>\n                        <ion-card-title>\n                          <div text-center>\n                            <h3 style="color:#e2c81f"><b>{{item.nickname}}</b></h3>\n                          </div>\n                        </ion-card-title>\n                      </ion-card-content>\n                    </ion-card>\n                </div>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n      </div>\n      <!--<ion-fab top right>\n        <button ion-fab color="traditional" (click)="filterBy()">\n          <div *ngIf="filterNum == 1">\n            <img src="./assets/imgs/alphabet.png" style="vertical-align: middle; align-items: center" />\n          </div>\n          <div *ngIf="filterNum == 0">\n            <img src="./assets/imgs/map.png"  style="vertical-align: middle; align-items: center"/>\n          </div>\n        </button>\n      </ion-fab>-->\n      \n  <div *ngIf="category == \'dream\'">\n      <div *ngIf="data != false">\n          <ion-grid class="card-background-page">\n            <ion-row>\n      <ion-col col-6 *ngFor="let item of itemsDream | async">\n        \n        <div *ngIf="item != null">\n          <div>\n            <ion-card >\n              <div *ngFor="let itemImage of itemsImage | async">\n              <div *ngIf="item.key == itemImage.key">\n                <ion-card class="type">\n                    <img src="{{itemImage.screenshot1}}" (click)="nanny(item.key)" style="border-radius:0px;"/>\n                  <!--<div class="card-title">{{item.tarifa}}</div>-->\n                </ion-card>\n              </div>\n              </div>\n              <ion-card-content>\n                <ion-card-title>\n                  <div text-center>\n                    <h3 style="color:#8a00ff"><b>{{item.nickname}}</b></h3>\n                  </div>\n                </ion-card-title>\n              </ion-card-content>\n            </ion-card>\n        </div>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</div>\n</div>\n<div *ngIf="category == \'special\'">\n    <div *ngIf="data != false">\n        <ion-grid class="card-background-page">\n          <ion-row>\n    <ion-col col-6 *ngFor="let item of itemsSpecial | async">\n      \n      <div *ngIf="item != null">\n        <div>\n          <ion-card >\n            <div *ngFor="let itemImage of itemsImage | async">\n            <div *ngIf="item.key == itemImage.key">\n              <ion-card class="type">\n                  <img src="{{itemImage.screenshot1}}" (click)="nanny(item.key)" style="border-radius:0px;"/>\n                <!--<div class="card-title">{{item.tarifa}}</div>-->\n              </ion-card>\n            </div>\n            </div>\n            <ion-card-content>\n              <ion-card-title>\n                <div text-center>\n                  <h3 style="color:#008aff"><b>{{item.nickname}}</b></h3>\n                </div>\n              </ion-card-title>\n            </ion-card-content>\n          </ion-card>\n      </div>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</div>\n</div>\n<div *ngIf="category == \'grand\'">\n    <div *ngIf="data != false">\n        <ion-grid class="card-background-page">\n          <ion-row>\n    <ion-col col-6 *ngFor="let item of itemsGrand | async">\n      \n      <div *ngIf="item != null">\n        <div>\n          <ion-card >\n            <div *ngFor="let itemImage of itemsImage | async">\n            <div *ngIf="item.key == itemImage.key">\n              <ion-card class="type">\n                  <img src="{{itemImage.screenshot1}}" (click)="nanny(item.key)" style="border-radius:0px;"/>\n                <!--<div class="card-title">{{item.tarifa}}</div>-->\n              </ion-card>\n            </div>\n            </div>\n            <ion-card-content>\n              <ion-card-title>\n                <div text-center>\n                  <h3 style="color:#ff9100"><b>{{item.nickname}}</b></h3>\n                </div>\n              </ion-card-title>\n            </ion-card-content>\n          </ion-card>\n      </div>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</div>\n</div>\n<div *ngIf="category == \'trip\'">\n    <div *ngIf="data != false">\n        <ion-grid class="card-background-page">\n          <ion-row>\n    <ion-col col-6 *ngFor="let item of itemsTrip | async">\n      \n      <div *ngIf="item != null">\n        <div>\n          <ion-card >\n            <div *ngFor="let itemImage of itemsImage | async">\n            <div *ngIf="item.key == itemImage.key">\n              <ion-card class="type">\n                  <img src="{{itemImage.screenshot1}}" (click)="nanny(item.key)" style="border-radius:0px;"/>\n                <div class="card-title">{{item.tarifa}}</div>\n              </ion-card>\n            </div>\n            </div>\n            <ion-card-content>\n              <ion-card-title>\n                <div text-center>\n                  <h3 style="color:#97bb2c"><b>{{item.nickname}}</b></h3>\n                </div>\n              </ion-card-title>\n            </ion-card-content>\n          </ion-card>\n      </div>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</div>\n</div>\n\n<div *ngIf="category == \'day\'">\n    <div *ngIf="data != false">\n        <ion-grid class="card-background-page">\n          <ion-row>\n    <ion-col col-6 *ngFor="let item of itemsStand | async">\n      \n      <div *ngIf="item != null">\n        <div>\n          <ion-card >\n            <div *ngFor="let itemImage of itemsImage | async">\n            <div *ngIf="item.key == itemImage.key">\n              <ion-card class="type">\n                  <img src="{{itemImage.screenshot1}}" (click)="nanny(item.key)" style="border-radius:0px;"/>\n                <!--<div class="card-title">{{item.tarifa}}</div>-->\n              </ion-card>\n            </div>\n            </div>\n            <ion-card-content>\n              <ion-card-title>\n                <div text-center>\n                  <h3 style="color:#ff9100"><b>{{item.nickname}}</b></h3>\n                </div>\n              </ion-card-title>\n            </ion-card-content>\n          </ion-card>\n      </div>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</div>\n</div>\n<!--\n<div *ngIf="category == \'week\'">\n    <div *ngIf="data != false">\n        <ion-grid class="card-background-page">\n          <ion-row>\n    <ion-col col-6 *ngFor="let item of itemsStandSemana | async">\n      \n      <div *ngIf="item != null">\n        <div>\n          <ion-card >\n            <div *ngFor="let itemImage of itemsImage | async">\n            <div *ngIf="item.key == itemImage.key">\n              <ion-card class="type">\n                  <img src="{{itemImage.screenshot1}}" (click)="nanny(item.key)" style="border-radius:0px;"/>\n                <div class="card-title">{{item.tarifa}}</div>\n              </ion-card>\n            </div>\n            </div>\n            <ion-card-content>\n              <ion-card-title>\n                <div text-center>\n                  <h3 style="color:#ff006f"><b>{{item.nickname}}</b></h3>\n                </div>\n              </ion-card-title>\n            </ion-card-content>\n          </ion-card>\n      </div>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</div>\n</div>\n-->\n<div *ngIf="category == \'stand\'">\n    <div *ngIf="data != false">\n        <ion-grid class="card-background-page">\n          <ion-row>\n    <ion-col col-6 *ngFor="let item of itemsStand | async">\n      \n      <div *ngIf="item != null">\n        <div>\n          <ion-card >\n            <div *ngFor="let itemImage of itemsImage | async">\n            <div *ngIf="item.key == itemImage.key">\n              <ion-card class="type">\n                  <img src="{{itemImage.screenshot1}}" (click)="nanny(item.key)" style="border-radius:0px;"/>\n                <!--<div class="card-title">{{item.tarifa}}</div>-->\n              </ion-card>\n            </div>\n            </div>\n            <ion-card-content>\n              <ion-card-title>\n                <div text-center>\n                  <h3 style="color:#ff006f"><b>{{item.nickname}}</b></h3>\n                </div>\n              </ion-card-title>\n            </ion-card-content>\n          </ion-card>\n      </div>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</div>\n</div>\n\n<div *ngIf="category == \'party\'">\n    <div *ngIf="data != false">\n        <ion-grid class="card-background-page">\n          <ion-row>\n    <ion-col col-6 *ngFor="let item of itemsParty | async">\n      \n      <div *ngIf="item != null">\n        <div>\n          <ion-card >\n            <div *ngFor="let itemImage of itemsImage | async">\n            <div *ngIf="item.key == itemImage.key">\n              <ion-card class="type">\n                  <img src="{{itemImage.screenshot1}}" (click)="nanny(item.key)" style="border-radius:0px;"/>\n                <!--<div class="card-title">{{item.tarifa}}</div>-->\n              </ion-card>\n            </div>\n            </div>\n            <ion-card-content>\n              <ion-card-title>\n                <div text-center>\n                  <h3 style="color:#ff7800"><b>{{item.nickname}}</b></h3>\n                </div>\n              </ion-card-title>\n            </ion-card-content>\n          </ion-card>\n      </div>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</div>\n</div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/category/category.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], CategoryPage);
    return CategoryPage;
}());

//# sourceMappingURL=category.js.map

/***/ }),

/***/ 183:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__foto_foto__ = __webpack_require__(184);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ChatPage = /** @class */ (function () {
    function ChatPage(navCtrl, navParams, afDB, afAuth, camera, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.camera = camera;
        this.modalCtrl = modalCtrl;
        this.userId = {};
        this.key = {};
        this.message = '';
        this.usuario = {};
        this.name = {};
        this.data = {};
        this.ordersN = {};
        this.ordersU = {};
        this.userId = this.afAuth.auth.currentUser.uid;
        console.log(this.userId);
        this.key = this.navParams.get("key");
        console.log(this.key);
        this.name = this.navParams.get("name");
        this.nanyId = this.navParams.get("nanyId");
        this.user = this.navParams.get("user");
        this.messages = afDB.list('chat' + '/' + this.key).snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({}, c.payload.val())); });
        });
        this.afDB.object('orders/' + this.userId + '/' + this.key + '/data/').valueChanges().subscribe(function (data) {
            _this.ordersU = data;
            console.log(_this.ordersU);
        });
        this.afDB.object('orders_n/' + this.nanyId + '/' + this.key + '/data/').valueChanges().subscribe(function (data) {
            _this.ordersN = data;
            console.log(_this.ordersN.numChil);
        });
    }
    ChatPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatPage');
    };
    ChatPage.prototype.mostrar_camara = function (idPlayerNannyUser) {
        var _this = this;
        this.camera.getPicture({
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.CAMERA,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 720,
            targetHeight: 720,
            saveToPhotoAlbum: true
        }).then(function (foto) {
            var nombreArchivo = new Date().valueOf().toString();
            var fotoref = __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref('usuarios/chat/' + nombreArchivo);
            fotoref.putString(foto, 'base64', { contentType: 'image/jpg' }).then(function (foto_guardad) {
                var foto = foto_guardad.downloadURL;
                _this.sendMessagePhoto(idPlayerNannyUser, foto);
            });
        }, function (error) {
            // Log an error to the console if something goes wrong.
            console.log("ERROR -> " + JSON.stringify(error));
        });
    };
    ChatPage.prototype.verImagen = function (foto) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__foto_foto__["a" /* FotoPage */], { 'foto': foto });
        modal.present();
    };
    ChatPage.prototype.sendMessage = function (idPlayerNannyUser) {
        var _this = this;
        this.review = this.message;
        this.nombre = this.name;
        this.dateTime = new Date().toISOString();
        this.afDB.list('/chat' + '/' + this.key).push({
            idUser: this.userId,
            mensaje: this.message,
            name: this.name,
            time: this.dateTime,
            type: 0
        }).then(function (mensaje) {
            // mensaje enviado !
            console.log('mensaje enviado !', _this.review);
            _this.afDB.object('users/' + _this.userId + '/' + 'meta/').valueChanges().subscribe(function (data) {
                _this.data = data;
                if (!_this.data) {
                    _this.data = {};
                }
                if (_this.data.user == 0) {
                    console.log('mensaje enviado user !', _this.review);
                    var noti = {
                        app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                        include_player_ids: [idPlayerNannyUser],
                        data: { "foo": "bar" },
                        contents: { en: _this.review },
                        headings: { en: _this.nombre }
                    };
                    window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                        console.log("Notification Post Success:", successResponse);
                    }, function (failedResponse) {
                        console.log("Notification Post Failed: ", failedResponse);
                        alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
                    });
                }
                else {
                    _this.afDB.object('products_meta/' + _this.nanyId).valueChanges().subscribe(function (data) {
                        _this.data = data;
                        if (!_this.data) {
                            _this.data = {};
                        }
                        console.log('mensaje enviado nanny!', _this.review);
                        var noti = {
                            app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                            include_player_ids: [idPlayerNannyUser],
                            data: { "foo": "bar" },
                            contents: { en: _this.review },
                            headings: { en: _this.nombre }
                        };
                        window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                            console.log("Notification Post Success:", successResponse);
                        }, function (failedResponse) {
                            console.log("Notification Post Failed: ", failedResponse);
                            alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
                        });
                    });
                }
                console.log(_this.data.displayName);
            });
        });
        this.message = '';
    };
    ChatPage.prototype.sendMessagePhoto = function (idPlayerNannyUser, foto) {
        var _this = this;
        this.review = foto;
        this.nombre = this.name;
        this.photo = foto;
        this.dateTime = new Date().toISOString();
        this.afDB.list('/chat' + '/' + this.key).push({
            idUser: this.userId,
            mensaje: this.photo,
            name: this.name,
            time: this.dateTime,
            type: 1
        }).then(function (mensaje) {
            // mensaje enviado !
            console.log('mensaje enviado !', _this.review);
            _this.afDB.object('users/' + _this.userId + '/' + 'meta/').valueChanges().subscribe(function (data) {
                _this.data = data;
                if (!_this.data) {
                    _this.data = {};
                }
                if (_this.data.user == 0) {
                    console.log('mensaje enviado user !', _this.review);
                    var noti = {
                        app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                        include_player_ids: [idPlayerNannyUser],
                        data: { "foo": "bar" },
                        contents: { en: "Foto :camera:" },
                        big_picture: _this.review,
                        ios_attachments: { id1: _this.review }
                    };
                    window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                        console.log("Notification Post Success:", successResponse);
                    }, function (failedResponse) {
                        console.log("Notification Post Failed: ", failedResponse);
                        alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
                    });
                }
                else {
                    _this.afDB.object('products_meta/' + _this.nanyId).valueChanges().subscribe(function (data) {
                        _this.data = data;
                        if (!_this.data) {
                            _this.data = {};
                        }
                        console.log('mensaje enviado nanny!', _this.review);
                        var noti = {
                            app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                            include_player_ids: [idPlayerNannyUser],
                            data: { "foo": "bar" },
                            contents: { en: "Foto :camera:" },
                            big_picture: _this.review,
                            ios_attachments: { id1: _this.review }
                        };
                        window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                            console.log("Notification Post Success:", successResponse);
                        }, function (failedResponse) {
                            console.log("Notification Post Failed: ", failedResponse);
                            alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
                        });
                    });
                }
                console.log(_this.data.displayName);
            });
        });
        this.message = '';
    };
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-chat',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/chat/chat.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>chat</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div id="chatMessages">\n      <div *ngFor="let message of messages | async">\n        <div [class]="message.idUser == userId ? \'innerMessage messageRight\' : \'innerMessage messageLeft\'">\n          <div class="username">\n            {{message.name}}\n          </div>\n          <div class="messageContent">\n            <div *ngIf="message.type == 0">\n              {{message.mensaje}}\n            </div>\n            <div *ngIf="message.type == 1">\n              <img src="{{message.mensaje}}" width="150" height="150" (click)="verImagen(message.mensaje)" >\n            </div>\n          </div>\n          <div text-right>\n            {{message.time | date:\'H:mm\'}}\n          </div>\n        </div>\n      </div>\n  </div>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n    <div id="footer">\n      <div class="elem">\n        <ion-input type="text" [(ngModel)]="message">\n        </ion-input>\n      </div>\n      <div class="elemt" *ngIf="user == 1">\n        <button ion-fab icon-only color="dark" (click)="mostrar_camara(ordersN.idPlayerUser)"> <ion-icon ios="ios-camera" md="md-camera"></ion-icon> </button>\n      </div>\n      <div class="elemt" *ngIf="user == 0">\n        <button ion-fab icon-only color="dark" (click)="mostrar_camara(ordersU.idPlayerNanny)"> <ion-icon ios="ios-camera" md="md-camera"></ion-icon> </button>\n      </div>\n      <div class="elem" *ngIf="user == 0">\n        <button ion-fab icon-only color="dark" (click)="sendMessage(ordersU.idPlayerNanny)"><ion-icon name="send"></ion-icon> </button>\n      </div>\n      <div class="elem" *ngIf="user == 1">\n        <button ion-fab icon-only color="dark" (click)="sendMessage(ordersN.idPlayerUser)"><ion-icon name="send"></ion-icon> </button>\n      </div>\n    </div>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/chat/chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 184:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FotoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FotoPage = /** @class */ (function () {
    function FotoPage(navCtrl, navParams, modalCtrl, viewCtrl, sanitizer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.sanitizer = sanitizer;
        this.foto = this.navParams.get("foto");
        this.image = this.sanitizer.bypassSecurityTrustStyle("url(" + this.foto + ")");
    }
    FotoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FotoPage');
    };
    FotoPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    FotoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-foto',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/foto/foto.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding [style.background-image]="image">\n\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/foto/foto.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["c" /* DomSanitizer */]])
    ], FotoPage);
    return FotoPage;
}());

//# sourceMappingURL=foto.js.map

/***/ }),

/***/ 185:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(12);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CommentsPage = /** @class */ (function () {
    function CommentsPage(navCtrl, navParams, viewCtrl, afDB) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.afDB = afDB;
        this.nanyId = this.navParams.get("nanyId");
        console.log(this.nanyId);
        this.nanyR = afDB.list('rating/' + this.nanyId).snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
    }
    CommentsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CommentsPage');
    };
    CommentsPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    CommentsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-comments',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/comments/comments.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Comentarios</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12>\n        <div text-center>\n          <h3><strong>Notas de agradecimiento de los usuarios</strong></h3>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col *ngFor="let nany of nanyR | async" col-12>\n            <h3><img src={{nany.pato}} id="pato"> {{nany.nota}}</h3>\n        </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/comments/comments.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], CommentsPage);
    return CommentsPage;
}());

//# sourceMappingURL=comments.js.map

/***/ }),

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var InfoPage = /** @class */ (function () {
    function InfoPage(navCtrl, navParams, viewCtrl, afDB, afAuth, fb, loadingCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.fb = fb;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.data = {};
        this.userId = {};
        this.usuario = {};
        this.userId = this.afAuth.auth.currentUser.uid;
        this.myForm = this.fb.group({
            ciudad: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            delegacion: ['', []],
            colonia: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            calle: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            numEx: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            numIn: ['', []],
            cp: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            movil: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]]
        });
        this.afDB.object('users/' + this.userId + '/' + 'info/').valueChanges().subscribe(function (data) {
            _this.data = data;
            if (!_this.data) {
                _this.data = {};
                _this.data.delegacion = '';
                _this.data.numIn = '';
            }
            console.log(_this.data);
        });
    }
    InfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InfoPage');
    };
    InfoPage.prototype.showAlertC = function () {
        var alert = this.alertCtrl.create({
            title: 'Por el momento solo tenemos servicio en: <br> Ciudad de México <br> y <br> Estado de México',
            buttons: ['Aceptar']
        });
        alert.present();
    };
    InfoPage.prototype.addInfo = function () {
        var loader = this.loadingCtrl.create({
            spinner: 'crescent',
            content: "Actualizando información",
            duration: 3000
        });
        loader.present();
        var info = {
            ciudad: this.data.ciudad,
            delegacion: this.data.delegacion,
            colonia: this.data.colonia,
            calle: this.data.calle,
            numEx: this.data.numEx,
            numIn: this.data.numIn,
            cp: this.data.cp,
            movil: this.data.movil
        };
        var orden = {
            ciudad: this.data.ciudad,
            delegacion: this.data.delegacion,
            colonia: this.data.colonia,
            calle: this.data.calle,
            numEx: this.data.numEx,
            numIn: this.data.numIn,
            cp: this.data.cp,
        };
        this.afDB.database.ref('users' + '/' + this.userId + '/').update({ info: info });
        this.afDB.database.ref('users' + '/' + this.userId + '/').update({ orden: orden });
        this.viewCtrl.dismiss();
    };
    InfoPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    InfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-info',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/info/info.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>editar información</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <form [formGroup]="myForm" novalidate>\n    <ion-list>\n      <ion-item>\n        <ion-label floating color="dark">Ciudad<span class="color">*</span></ion-label>\n        <ion-select name="ciudad" formControlName="ciudad"\n          [(ngModel)]="data.ciudad"\n          okText=Aceptar\n          cancelText=Cancelar\n          (click)="showAlertC()">\n          <ion-option value="Ciudad de México">Ciudad de México</ion-option>\n          <ion-option value="Estado de México">Estado de México</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label floating color="dark">Delegación</ion-label>\n        <ion-input type="text" name="delegacion" formControlName="delegacion"\n        [(ngModel)]="data.delegacion"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating color="dark">Colonia<span class="color">*</span></ion-label>\n        <ion-input type="text" name="colonia" formControlName="colonia"\n        [(ngModel)]="data.colonia"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating color="dark">Calle<span class="color">*</span></ion-label>\n        <ion-input type="text" name="calle" formControlName="calle"\n        [(ngModel)]="data.calle"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating color="dark">Número exterior<span class="color">*</span></ion-label>\n        <ion-input type="text" name="numEx" formControlName="numEx"\n        [(ngModel)]="data.numEx"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating color="dark">Número interior</ion-label>\n        <ion-input type="text" name="numIn" formControlName="numIn"\n        [(ngModel)]="data.numIn"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating color="dark">Código postal<span class="color">*</span></ion-label>\n        <ion-input type="number" name="cp" formControlName="cp"\n        [(ngModel)]="data.cp"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating color="dark">Teléfono celular<span class="color">*</span></ion-label>\n        <ion-input type="number" name="movil" formControlName="movil"\n        [(ngModel)]="data.movil"></ion-input>\n      </ion-item>\n    </ion-list>\n    <button ion-button block type="button" (click)="addInfo()" [disabled]="!myForm.valid" color="dark">Guardar</button>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/info/info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], InfoPage);
    return InfoPage;
}());

//# sourceMappingURL=info.js.map

/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_email_composer__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl, navParams, emailComposer, callNumber) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.emailComposer = emailComposer;
        this.callNumber = callNumber;
        this.data = {};
    }
    ContactPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactPage');
    };
    ContactPage.prototype.sendEmail = function () {
        var email = {
            to: 'contacto@nannapp.com.mx',
            cc: 'nannappcompany@gmail.com',
            subject: this.data.asunto,
            body: this.data.mensaje
        };
        this.emailComposer.open(email);
    };
    ContactPage.prototype.call = function (telephoneNumber) {
        this.callNumber.callNumber(telephoneNumber, true);
        this.callNumber.isCallSupported()
            .then(function (response) {
            if (response == true) {
                // do something
            }
            else {
                // do something else
            }
        });
    };
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/contact/contact.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle hideBackButton="true">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Contáctanos</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-item>\n    <ion-label floating color="dark">Asunto</ion-label>\n    <ion-input type="text" [(ngModel)]="data.asunto"  name="asunto"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating color="dark">Mensaje</ion-label>\n    <ion-textarea name="mensaje"\n    [(ngModel)]="data.mensaje" name="mensaje" autocomplete="on" autocorrect="on" rows="5" cols="50"></ion-textarea>\n  </ion-item>\n  <ion-col col-12>\n    <button ion-button block color="dark" (click)="sendEmail()">Enviar Email</button>\n  </ion-col>\n  <br><br>\n  <ion-col col-12>\n    <button ion-button block color="dark" (click)="call(\'5521296063\')">Hablar a Nannapp</button>\n  </ion-col>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/contact/contact.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_email_composer__["a" /* EmailComposer */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__["a" /* CallNumber */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 188:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RatingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RatingPage = /** @class */ (function () {
    function RatingPage(navCtrl, navParams, viewCtrl, afDB) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.afDB = afDB;
        this.nanyId = {};
        this.userId = {};
        this.key = {};
        this.calif = {};
        this.nanyImage = {};
        this.nannyP = {};
        this.nannyR = {};
        this.data = {};
        this.numStars = 5;
        this.value = 0;
        this.readOnly = false;
        this.ionClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.stars = [];
        this.nanyId = this.navParams.get("nanyId");
        this.userId = this.navParams.get("userId");
        this.key = this.navParams.get("key");
        this.data.note = "";
        this.valor = 0;
        this.afDB.object('products_images/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nanyImage = data;
            if (!_this.nanyImage) {
                _this.nanyImage = {};
            }
            console.log(_this.nanyImage);
        });
        this.afDB.object('products_meta/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nannyP = data;
            if (!_this.nannyP) {
                _this.nannyP = {};
            }
            console.log(_this.nannyP);
        });
        this.afDB.object('rating/' + this.nanyId + '/').valueChanges().subscribe(function (data) {
            _this.nannyR = data;
            if (!_this.nannyR) {
                _this.nannyRi = false;
                console.log(_this.nannyRi);
            }
            else {
                _this.nannyRi = true;
                console.log(_this.nannyRi);
            }
        });
    }
    RatingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RatingPage');
    };
    RatingPage.prototype.ngAfterViewInit = function () {
        this.calc();
    };
    RatingPage.prototype.calc = function () {
        this.stars = [];
        var tmp = this.value;
        for (var i = 0; i < this.numStars; i++, tmp--) {
            if (tmp >= 1)
                this.stars.push("star");
            else if (tmp > 0 && tmp < 1)
                this.stars.push("star-half");
            else
                this.stars.push("star-outline");
        }
    };
    RatingPage.prototype.starClicked = function (index) {
        // console.log(index);
        if (!this.readOnly) {
            this.value = index + 1;
            this.ionClick.emit(this.value);
            this.calc();
            console.log(this.value);
            this.calificar(this.value);
        }
    };
    RatingPage.prototype.calificar = function (valor) {
        this.valor = valor;
        console.log('Calificación ' + valor);
    };
    RatingPage.prototype.guardar = function (valor) {
        var calificacion = valor;
        this.afDB.database.ref('orders' + '/' + this.userId + '/' + this.key + '/data/').update({ service: valor });
        this.afDB.database.ref('orders_n' + '/' + this.nanyId + '/' + this.key + '/data/').update({ service: valor });
        if (this.nannyRi == false) {
            this.valorC = 5 + calificacion;
            this.count = 2;
        }
        else {
            this.valorC = this.nannyR.service + calificacion;
            this.count = 1 + this.nannyR.count;
        }
        var pato = './assets/imgs/icon-pato.png';
        this.afDB.database.ref('rating' + '/' + this.nanyId + '/' + this.key).update({ service: this.valor, count: this.count, nota: this.data.note, pato: pato });
        this.afDB.database.ref('rating' + '/' + this.nanyId).update({ service: this.valorC, count: this.count });
        this.afDB.database.ref('users' + '/' + this.userId + '/' + 'meta/').update({ status: 0 });
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Number)
    ], RatingPage.prototype, "numStars", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Number)
    ], RatingPage.prototype, "value", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Boolean)
    ], RatingPage.prototype, "readOnly", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], RatingPage.prototype, "ionClick", void 0);
    RatingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-rating',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/rating/rating.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Evaluación</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12>\n        <div text-center>\n          <h2>Califica el cuidado de tu Nanny</h2>\n          <img src="{{nanyImage.screenshot1}}" style="width:40%;border-radius:50%;">\n          <h6>{{nannyP.title}}</h6>\n          <h3 style="color:#ac64b0">{{nannyP.nickname}}</h3>\n        </div>\n      </ion-col>\n    </ion-row>\n    <!-- <ion-rating [readOnly]="false" [value]="0" (ionClick)="calificar($event)">\n\n    </ion-rating> -->\n    <ion-grid class="rating-container">\n      <ion-row>\n        <ion-col *ngFor="let star of stars; let i = index " tappable (click)="starClicked(i)">\n          <ion-icon [name]="star"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-row>\n      <ion-col col-12>\n        <div text-center>\n          <ion-item>\n            <ion-label stacked color="dark" text-center><h2>¿Algún comentario?</h2></ion-label>\n            <ion-textarea name="note"\n            [(ngModel)]="data.note" name="note" autocomplete="on" autocorrect="on" rows="5" cols="50"></ion-textarea>\n          </ion-item>\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col col-12>\n        <div text-center>\n          <button ion-button block type="button" color="dark" (click)="guardar(valor)">Calificar</button>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/rating/rating.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], RatingPage);
    return RatingPage;
}());

//# sourceMappingURL=rating.js.map

/***/ }),

/***/ 189:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NanysfPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__nanny_nanny__ = __webpack_require__(101);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NanysfPage = /** @class */ (function () {
    function NanysfPage(navCtrl, navParams, afDB, afAuth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.category = {};
        this.category = this.navParams.get("category");
        this.uid = this.afAuth.auth.currentUser.uid;
        this.items = afDB.list('products_meta').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.itemsImage = afDB.list('products_images').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.usersFa = afDB.list('users/' + this.uid + '/favorite').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
    }
    NanysfPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NanysfPage');
    };
    NanysfPage.prototype.nanny = function (nanyId) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__nanny_nanny__["a" /* NannyPage */], { 'nanyId': nanyId, 'category': this.category });
    };
    NanysfPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-nanysf',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/nanysf/nanysf.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Favoritas</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding class="card-background-page">\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6 *ngFor="let userFa of usersFa | async">\n        <div *ngFor="let item of items | async">\n          <div *ngIf="userFa.key == item.key && userFa.marked == true">\n            <ion-card>\n              <div *ngFor="let itemImage of itemsImage | async">\n                <div *ngIf="item.key == itemImage.key">\n                  <ion-card class="type">\n                    <img src="{{itemImage.screenshot1}}" (click)="nanny(item.key)"/>\n                    <div class="card-title">{{item.tarifa}}</div>\n                  </ion-card>\n                </div>\n              </div>\n              <ion-card-content>\n                <ion-card-title>\n                  <div text-center>\n                    <h2 style="color:#ac64b0"><b>{{item.nickname}}</b></h2>\n                  </div>\n                </ion-card-title>\n              </ion-card-content>\n            </ion-card>\n          </div>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/nanysf/nanysf.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */]])
    ], NanysfPage);
    return NanysfPage;
}());

//# sourceMappingURL=nanysf.js.map

/***/ }),

/***/ 190:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PoliticasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PoliticasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PoliticasPage = /** @class */ (function () {
    function PoliticasPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    PoliticasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PoliticasPage');
    };
    PoliticasPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    PoliticasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-politicas',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/politicas/politicas.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>políticas</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-grid>\n      <ion-row>\n        <ion-col col-12>\n          <h5 text-center>POLÍTICAS DE PRIVACIDAD</h5>\n        </ion-col>\n        <ion-col col-12>\n          <p text-justify>\n            Mediante su acceso y uso de los Servicios usted acuerda vincularse jurídicamente por estas Condiciones, que establecen una relación contractual entre usted y Nannapp. Si usted no acepta estas Condiciones, no podrá acceder o usar los Servicios. Estas Condiciones sustituyen expresamente los acuerdos o compromisos previos con usted. Nannapp podrá poner fin de inmediato a estas Condiciones o cualquiera de los Servicios respecto de usted o, en general, dejar de ofrecer o denegar el acceso a los Servicios o cualquier parte de ellos, en cualquier momento y por cualquier motivo.\n          </p>\n          <p text-justify>\n          Se podrán aplicar condiciones suplementarias a determinados Servicios, como políticas para un evento, una actividad o una promoción particular, y dichas condiciones suplementarias se le comunicarán en relación con los Servicios aplicables. Las condiciones suplementarias se establecen además de las Condiciones, y se considerarán una parte de estas, para los fines de los Servicios aplicables. Las condiciones suplementarias prevalecerán sobre las Condiciones en el caso de conflicto con respecto a los Servicios aplicables.\n          </p>\n          <p text-justify>\n          Nannapp podrá modificar las Condiciones relativas a los Servicios cuando lo considere oportuno. Las modificaciones serán efectivas después de la publicación por parte de Nannapp de dichas Condiciones actualizadas en esta ubicación o las políticas modificadas o condiciones suplementarias sobre el Servicio aplicable. Su acceso o uso continuado de los Servicios después de dicha publicación constituye su consentimiento a vincularse por las Condiciones y sus modificaciones.\n          </p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/politicas/politicas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
    ], PoliticasPage);
    return PoliticasPage;
}());

//# sourceMappingURL=politicas.js.map

/***/ }),

/***/ 191:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__informacion_informacion__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_onesignal__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { PushnotificationProvider } from '../../providers/pushnotification/pushnotification';

//import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';




var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, navParams, afDB, afAuth, modalCtrl, oneSignal, geolocation, 
        //public barcodeScanner: BarcodeScanner,
        toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.modalCtrl = modalCtrl;
        this.oneSignal = oneSignal;
        this.geolocation = geolocation;
        this.toastCtrl = toastCtrl;
        this.usuario = {};
        this.uid = this.afAuth.auth.currentUser.uid;
        this.mn = '{ \"referencia\":\"MN_Nannapp\"}';
        this.afDB.object('users/' + this.uid + '/' + 'meta').valueChanges().subscribe(function (data) {
            _this.usuario = data;
            console.log(_this.usuario);
        });
        this.afDB.object('users/' + this.uid + '/' + 'idPlayer').valueChanges().subscribe(function (data) {
            _this.idPlayer = data;
            console.log(_this.idPlayer);
        });
        /*this.afDB.object('users/'+this.uid+'/'+'codigo/').valueChanges().subscribe(data =>{
             this.CodigoPromo = data;
             this.CodPromo = this.CodigoPromo.text;
             console.log(this.CodPromo);
             if(! this.CodigoPromo){
               this.CodigoPromo = false;
             }
            console.log(this.CodigoPromo);
          });*/
        // this.oneSignal.init_notification();
    }
    PerfilPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PerfilPage');
        console.log(this.mapRef);
        this.iniciarUser();
        this.iniciarGeolocalizacion();
        this.PlayerIDNanny();
        // this.displayMap();
    };
    PerfilPage.prototype.startN = function () {
        var _this = this;
        this.oneSignal.getIds().then(function (id) {
            console.log('MI ID de la suerte' + id.userId);
            var idPlayer = id.userId;
            _this.afDB.database.ref('users' + '/' + _this.uid + '/').update({ idPlayer: idPlayer });
            var noti = {
                app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                include_player_ids: [id.userId],
                data: { "foo": "bar" },
                contents: { en: "Notificaciones activadas." },
                headings: { en: "Aviso" }
            };
            window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                console.log("Notification Post Success:", successResponse);
            }, function (failedResponse) {
                console.log("Notification Post Failed: ", failedResponse);
            });
        });
    };
    PerfilPage.prototype.PlayerIDNanny = function () {
        var _this = this;
        this.afDB.object('users/' + this.uid).valueChanges().subscribe(function (data) {
            _this.idPlayer = data;
            if (_this.idPlayer.meta.user == 1) {
                console.log(_this.idPlayer.idPlayer);
                var idPlayer = _this.idPlayer.idPlayer;
                _this.afDB.database.ref('products_meta' + '/' + _this.idPlayer.meta.product + '/').update({ idPlayer: idPlayer });
            }
            else {
                console.log('No es Nanny');
            }
        });
    };
    PerfilPage.prototype.goInfo = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__informacion_informacion__["a" /* InformacionPage */], { 'userId': this.uid });
        modal.present();
    };
    PerfilPage.prototype.iniciarUser = function () {
        this.uid = this.afAuth.auth.currentUser.uid;
        this.user = this.afDB.object('users/' + this.uid + '/geo/');
    };
    PerfilPage.prototype.iniciarGeolocalizacion = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            // resp.coords.latitude
            // resp.coords.longitude
            console.log(resp.coords);
            var watch = _this.geolocation.watchPosition();
            watch.subscribe(function (data) {
                // data can be a set of coordinates, or an error (if an error occurred).
                // data.coords.latitude
                // data.coords.longitude
                console.log('watch: ', data.coords);
                _this.user.update({ lat: data.coords.latitude, lng: data.coords.longitude });
            });
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    };
    PerfilPage.prototype.displayMap = function () {
        var location = new google.maps.LatLng('42.598726', '-5.567096');
        var options = {
            center: location,
            zoom: 10
        };
        this.map = new google.maps.Map(this.mapRef.nativeElement, options);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], PerfilPage.prototype, "mapRef", void 0);
    PerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-perfil',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/perfil/perfil.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>perfil</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12>\n        <div text-center>\n          <img src="{{usuario.foto}}?type=large&width=720&height=720" style="width:50%;border-radius:50%;">\n        </div>\n      </ion-col>\n    </ion-row>\n    <br><br>\n    <ion-row>\n      <ion-col col-6>\n        <h6><b>Nombre</b></h6>\n      </ion-col>\n      <ion-col col-6>\n        <div text-right>\n          <h6 style="font-weight:normal">{{usuario.displayName}}</h6>\n        </div>\n      </ion-col>\n    </ion-row>\n    <br><br>\n    <ion-row>\n      <ion-col col-8>\n        <h6><b>Información</b></h6>\n      </ion-col>\n      <ion-col col-4>\n        <div text-center (click)="goInfo()">\n          <ion-icon name="clipboard" md="md-clipboard"></ion-icon>\n        </div>\n      </ion-col>\n    </ion-row>\n    <!--<ion-row>\n      <ion-col col-8>\n        <h6><b>Código QR</b></h6>\n      </ion-col>\n      \n      <ion-col col-4>\n         <div text-center (click)="encode()">\n          <ion-icon name="qr-scanner" md="md-qr-scanner"></ion-icon>\n        </div> \n        <div text-center></div>></div>\n          <ion-icon name="qr-scanner" md="md-qr-scanner"></ion-icon>\n        </div>\n      </ion-col>\n      <ion-col col-12>\n        <div text-center *ngIf="scannedData.text">\n          <ion-input type="text" [(ngModel)]="encodeText"></ion-input>\n          <span>Gracias por utilizar el código: <br> {{scannedData.text}}</span>\n        </div>\n        <div *ngIf="CodigoPromo.text == mn">\n          <div text-center>\n            Cuentas con $280.00 en tu proximo cuidado.\n          </div>\n        </div>\n      </ion-col>\n      \n    </ion-row>-->\n    <br><br>\n    <ion-row>\n      <ion-col col-8>\n        <h6><b>Activar notificaciones</b></h6>\n      </ion-col>\n      <ion-col col-4>\n        <div text-center (click)="startN(idPlayer)">\n          <ion-icon name="notifications" md="md-notifications"></ion-icon>\n        </div>\n      </ion-col>\n    </ion-row>\n\n  </ion-grid>\n  <!-- <div #map id="map"></div> -->\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/perfil/perfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */]])
    ], PerfilPage);
    return PerfilPage;
}());

//# sourceMappingURL=perfil.js.map

/***/ }),

/***/ 192:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreguntasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PreguntasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PreguntasPage = /** @class */ (function () {
    function PreguntasPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.items = [{
                title: '1.	¿Qué servicios ofrece Nannapp?',
                text: 'Nannapp es una plataforma digital que enlaza a familias que requieren a una nanny calificada que cuide de sus hijos en casa de acuerdo a su edad. Cuidamos a niños desde 0 a 12 años. Ofrecemos 3 tarifas dentro del perfil TRADITIONAL, que son: ON DEMAND (nanny por hora), STAND (nanny recurrente, paquete por 50 horas al mes) y DREAM (nanny nocturna, tarifa especial por 12 horas).'
            }, {
                title: '2.	¿Cuál es la filosofía de Nannapp?',
                text: 'Vivimos en una era en la que el ritmo de vida es acelerado y los padres de familia pasan mucho tiempo trabajando para tener una mejor calidad de vida. Es importante que mamá y papá también puedan atender sus compromisos personales sin descuidar a sus hijos en el hogar. Nuestras nannies de la guarda son un ángel en casa; además de tutoras, son profesionales en el cuidado de los niños y ellos estará acompañados como merecen, queremos que los niños sean más felices porque ellos son el futuro de nuestra sociedad.'
            }, {
                title: '3.	¿Cuál es el proceso de selección de la nanny?',
                text: 'Cada nanny cuenta con licenciatura en enfermería, psicología, pedagogía, puericulturismo o alguna disciplina relacionada con la salud, educación y crianza de los niños. Se les aplican pruebas psicométricas, entrevista y un estudio socioeconómico y laboral. Todas son evaluadas con base a su experiencia y competencias, solo así podrán darse de alta en nuestra plataforma. Nannapp también permite a los usuarios ver las evaluaciones que otras familias les dan después de cada cuidado, así como el rango de edad de los niños que están capacitadas para cuidar (0-1.5, 1.5 a 3, 3-6, 6-9 y 9-12 años) y si son o no amigables con las mascotas (pet friendly), para prevenir alguna contingencia en caso de que la nanny sea alérgica y desempeñe bien su trabajo.'
            }, {
                title: '4.	¿Cuál es la ventaja de contratar a una nanny de Nannapp?',
                text: 'La ventaja es que su formación académica y los filtros de selección garantizan su vocación, confiabilidad y capacitación por parte de Nannapp en primeros auxilios, psicopedagogía y atención al cliente. Se presentan uniformadas, están supervisadas constantemente y cumplen con nuestro código de ética.'
            }, {
                title: '5.	¿Son privados los datos de la familia que contrata a la nanny?',
                text: 'Sí, tanto los datos de las familias y las nannies están protegidos. Sugerimos leer nuestros avisos y políticas de privacidad.'
            }, {
                title: '6.	¿Cómo es el método de pago?',
                text: 'Por medio de tarjeta de crédito. Al momento de ser aceptada una reservación, se hace el cargo por el cuidado solicitado. En caso de cancelación se reembolsará el cargo realizado por el mismo medio que se pagó.'
            }, {
                title: '7.	¿En dónde puedo solicitar factura? ',
                text: 'Nuestro sistema de facturación es por medio de la página web nannapp.com.mx y todas nuestras tarifas ya incluyen IVA.'
            }, {
                title: '8. ¿Hay penalización en caso de cancelar el servicio y qué pasa si no se cancela pero tampoco llega la nanny?',
                text: 'La nanny debe enviar una notificación avisando a la familia que va en camino. Si la cancelación, por parte de la familia o la nanny, se hace 3 horas antes del servicio, el reembolso será del 100%. Si la cancelación, por parte de la familia ,se hace dentro de las 3 horas previas al cuidado reservado, se le hará el cargo por las horas de la duración del cuidado; si no cancela y la nanny llega pero nadie la recibe; además se levantará un reporte para la familia. Si la cancelación, por parte de la nanny, se hace dentro de las 3 horas previas al cuidado, Nannapp compensará a la familia con una hora gratis en su siguiente contratación al presentarnos su respectiva aclaración por escrito. Si la nanny no cancela y tampoco llega, la familia debe levantar un reporte y puede solicitar una nanny de emergencia, además de ser compensada por Nannapp con 2 horas gratis en su siguiente cuidado. A los 5 reportes, tanto para la familia como para la nanny, Nannapp considerará darlos de baja de la plataforma.'
            }, {
                title: '9. ¿Cuánto tiempo de tolerancia hay para que la nanny o la familia lleguen puntuales en el horario contratado?',
                text: 'La tolerancia es de 15 minutos. Si la nanny llega al minuto 16, la primera hora del cuidado es gratis. Si la familia llega al minuto 16 de que venció la contratación, se cobra la hora completa.'
            }, {
                title: '10.	¿Qué pasa si una vez que la nanny está cuidando a mis hijo requiero que se quede más tiempo con él?',
                text: 'En el perfil de la nanny existe la opción de agregar más horas.'
            }, {
                title: '11.	¿Con cuantas horas de anticipación debo contratar el servicio?',
                text: 'Por motivos de disponibilidad se pide un mínimo de 3 horas de anticipación. Se recomienda hacer las reservaciones que inicien antes de medio dia, al menos la noche de un día antes, esto para que nuestras nannies confirmen el cuidado, mientras se hagan las reservaciones con el mayor tiempo de anticipación posible para encontrar disponible a la nanny de su preferencia.'
            }, {
                title: '12.	¿La familia o la nanny cuentan con algún tipo de seguro durante la contratación del servicio?',
                text: 'Sí, Nannapp tiene una póliza de seguro contra accidentes para los niños y la nanny mientras los cuidan. La plataforma incluye un botón que marca directo a la compañía del seguro y los pasos a seguir en caso de suceder algún percance.'
            }, {
                title: '13.	¿Puedo contratar a la misma nanny para futuros cuidados?',
                text: 'Sí, búscala en tu historial de cuidados y agrégala a favoritas para que puedas enviarle solicitud de reservación, la cual ella aceptará o declinará según su disponibilidad. También puedes contratar la tarifa STAND que está dentro del perfil TRADITIONAL, ésta permite contratar de manera recurrente a la misma nanny, por un paquete de prepago que incluye 50 horas por $7000 pesos al mes. Lo más recomendable es programar una agenda con anticipación para que ella esté disponible y puedas reservar los días y horas en los que la necesitarás. Si ella no está disponible o deseas cambiar de nanny, debes buscar a otra de tu preferencia que acepte continuar con esa tarifa por las horas que te resten del paquete.'
            }, {
                title: '14.	¿Qué pasa si quiero contratar por fuera a la nanny por un mejor precio?',
                text: 'En ese caso, Nannapp no se hace responsable por retrasos, cancelaciones o rembolsos, ni puede sustituirla; además de que el seguro contra accidentes pierde cobertura. La plataforma cuenta con otras ventajas que protegen a la familia y como el sistema de geolocalización y el botón de emergencia que enlaza directamente a las autoridades en caso de algún incidente. Además, tenemos un paquete de recompensas anual para las familias que sumen más horas contratadas, al igual que un bono para las nannies que coticen más cuidados y estén mejor evaluadas.'
            }, {
                title: '15.	¿Puedo evaluar a la nanny que cuidó a mi hijo?',
                text: 'Sí, al finalizar cada cuidado aparecerán en el sistema 5 patitos que equivalen a la calidad del servicio, además de poder escribir algún comentario que será público para que otras familias puedan leerlo. Esto ayuda a motivar y mejorar el desempeño de la nanny. De igual manera, la nanny puede evaluar a la familia de manera privada, ya que es importante el respeto entre ambas partes para volver a ser contratadas o que quieran regresar a su hogar para cuidar a sus hijos.'
            }, {
                title: '16.	¿Hasta cuántos niños puede cuidar una nanny por la misma tarifa?',
                text: 'El máximo de niños por los que la nanny puede hacerse responsable son 2 (de 0 a 3 años) y 3 (arriba de 6 años), a partir de uno más debe solicitarse otra nanny.'
            }, {
                title: '17.	¿Puedo contratar a una nanny para que cuide varios niños en un evento social? ',
                text: 'En caso de requerirlo, debe enviar un email con los datos del evento para solicitar este servicio y esperar nuestra confirmación para reservar las nannies necesarias, por tranquilidad de los padres y seguridad de los niños.'
            }, {
                title: '18.	¿Pueden cuidar a mi hijo si tiene alguna discapacidad física o mental?',
                text: 'En caso de que su hijo requiera de cuidados especiales debe especificarlo en el registro de familia y enviarnos un correo solicitando el servicio para buscar a la nanny ideal que pueda cuidarlo de acuerdo a sus necesidades.'
            }, {
                title: '19.	¿La nanny también puede hacer el aseo de mi casa o pasear a la mascota?',
                text: 'No, el servicio es exclusivo a las labores que respectan el cuidado de los niños. Esto puede incluir la limpieza del área en donde convivieron o los utensilios de cocina para darle sus alimentos.'
            }, {
                title: '20.	¿Qué incluye la tarifa DREAM dentro del perfil TRADITIONAL?',
                text: 'De 8pm a 12am tiene un costo de $1120, el cual equivale a la tarifa ON DEMAND. Apartir de la siguiente hora se cobran $1400 por un horario de 8pm a 8am, en el que se debe brindar hospedaje digno y un alimento a la nanny; quien se encargará de la alimentación, higiene, descanso e integridad de su hijo.'
            }, {
                title: '21.	¿Cómo puedo saber lo que mi hijo está haciendo en casa mientras se queda con la nanny?',
                text: 'Nuestra plataforma tiene un chat privado en el que la familia puede solicitarle a la nanny fotos o videos, además de tener un sistema de geolocalización que le permite saber que su hijo está con la nanny en casa.'
            }, {
                title: '22.	¿Qué pasa si una nanny no está cómoda con la familia?',
                text: 'La nanny puede suspender el servicio en caso de acoso, violencia o cualquier situación que la ponga en riesgo, así como presionar el botón de emergencia para solicitar ayuda a las autoridades. Se levanta un reporte y Nannapp reembolsa el cargo correspondiente por las horas canceladas.'
            }, {
                title: '23.	¿Cómo activo un código QR promocional?',
                text: 'Los códigos QR proporcionados por Nannapp o nuestros aliados comerciales tienen la finalidad de brindar promociones a nuestros usuarios. Se activan en la pestaña que dice código QR en la sección del registro como familia.'
            }, {
                title: '24.	¿Cómo funciona la tarifa STAND?',
                text: 'La tarifa STAND es el servicio de una nanny recurrente, el paquete incluye 100 horas, las fechas y horarios de cuidados se acordarán con la nanny. En caso que la familia quiera solicitar un cambio de nanny o cancelar el servicio, se tendrá una penalización de la mitad de horas faltantes de cubrir, se recomienda solicitar el servicio STAND una vez que se conozca a la nanny mediante servicios TRADITIONAL.'
            },];
    }
    PreguntasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PreguntasPage');
    };
    PreguntasPage.prototype.toggleItem = function (item) {
        if (this.isItemShown(item)) {
            this.shownItem = null;
        }
        else {
            this.shownItem = item;
        }
    };
    PreguntasPage.prototype.isItemShown = function (item) {
        return this.shownItem === item;
    };
    PreguntasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-preguntas',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/preguntas/preguntas.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle hideBackButton="true">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n      <div text-center text-wrap>\n        preguntas frecuentes\n      </div>\n    </ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-list no-lines>\n    <ion-item *ngFor="let item of items" class="item item-text-wrap">\n      <div [class]="isItemShown(item) ? \'item-expand active\' : \'item-expand inactive\'" text-wrap>\n        <h2>{{ item.title }}</h2>\n        <p text-justify>{{ item.text }}</p>\n      </div>\n      <div (click)="toggleItem(item)" class="item-expand-footer">\n        <i [class]="isItemShown(item) ? \'ion-ios-minus-outline\' : \'ion-ios-plus-outline\'"></i>\n        {{ isItemShown(item) ? \'Ver menos\' : \'Ver más\' }}\n      </div>\n    </ion-item>\n  </ion-list>\n  <p text-justify>Si tienes alguna duda, solicitud o comentario que no esté dentro de este cuestionario, por favor contáctanos para atenderte a la brevedad y brindarte un servicio a la medida</p>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/preguntas/preguntas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], PreguntasPage);
    return PreguntasPage;
}());

//# sourceMappingURL=preguntas.js.map

/***/ }),

/***/ 204:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 204;

/***/ }),

/***/ 246:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/category/category.module": [
		578,
		21
	],
	"../pages/chat/chat.module": [
		579,
		20
	],
	"../pages/comments/comments.module": [
		580,
		19
	],
	"../pages/comprobante/comprobante.module": [
		581,
		18
	],
	"../pages/contact/contact.module": [
		582,
		17
	],
	"../pages/foto/foto.module": [
		583,
		16
	],
	"../pages/historial/historial.module": [
		584,
		15
	],
	"../pages/id/id.module": [
		585,
		14
	],
	"../pages/info/info.module": [
		587,
		13
	],
	"../pages/informacion/informacion.module": [
		586,
		12
	],
	"../pages/login/login.module": [
		588,
		11
	],
	"../pages/modal/modal.module": [
		589,
		10
	],
	"../pages/nanny/nanny.module": [
		590,
		9
	],
	"../pages/nanysf/nanysf.module": [
		591,
		8
	],
	"../pages/perfil/perfil.module": [
		593,
		7
	],
	"../pages/politicas/politicas.module": [
		592,
		6
	],
	"../pages/preguntas/preguntas.module": [
		594,
		5
	],
	"../pages/rating/rating.module": [
		595,
		4
	],
	"../pages/request/request.module": [
		596,
		3
	],
	"../pages/requestform/requestform.module": [
		597,
		2
	],
	"../pages/reserved/reserved.module": [
		598,
		1
	],
	"../pages/startservice/startservice.module": [
		599,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 246;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 368:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PushnotificationProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_onesignal__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PushnotificationProvider = /** @class */ (function () {
    function PushnotificationProvider(oneSignal, platform) {
        this.oneSignal = oneSignal;
        this.platform = platform;
        console.log('Hello PushnotificationProvider Provider');
    }
    PushnotificationProvider.prototype.init_notification = function () {
        if (this.platform.is('cordova')) {
            this.oneSignal.startInit('88f157f1-f2e1-41c4-bfec-b479cb7f4176', '748451395664');
            this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
            this.oneSignal.handleNotificationReceived().subscribe(function () {
                console.log('Notificación Recivida');
            });
            this.oneSignal.handleNotificationOpened().subscribe(function () {
                // do something when a notification is opened
                console.log('Notificación Abierta');
            });
            this.oneSignal.endInit();
        }
        else {
            console.log('OneSignal no funciona en Chrome');
        }
    };
    PushnotificationProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]])
    ], PushnotificationProvider);
    return PushnotificationProvider;
}());

//# sourceMappingURL=pushnotification.js.map

/***/ }),

/***/ 369:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_onesignal__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__startservice_startservice__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__rating_rating__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_firebase_app__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_facebook__ = __webpack_require__(315);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { Observable } from 'rxjs/Observable';

// import { PerfilPage } from '../perfil/perfil';

// import { RatingPage } from '../rating/rating';






var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, afDB, afAuth, fb, platform, oneSignal) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.fb = fb;
        this.platform = platform;
        this.oneSignal = oneSignal;
        this.usuario = {};
        this.orderU = {};
        this.nanyId = {};
        this.key = {};
        this.ordersU = {};
        this.nannyP = {};
        this.nanyImage = {};
        this.ordersN = {};
        this.status = {};
        afAuth.authState.subscribe(function (user) {
            if (!user) {
                _this.displayName = null;
                return;
            }
            else {
                _this.uid = _this.afAuth.auth.currentUser.uid;
                _this.afDB.object('users/' + _this.uid + '/' + 'meta').valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    if (!_this.usuario) {
                        _this.displayName = user.displayName;
                        _this.photo = user.photoURL;
                        _this.codigo = user.uid;
                        var usersFirebase = _this.afDB.object('/users/' + _this.codigo + '/' + 'meta');
                        usersFirebase.set({
                            displayName: _this.displayName,
                            foto: _this.photo,
                            user: 0,
                            status: 0
                        });
                        _this.bienvenida();
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
                    }
                    else if (_this.usuario.status == 2) {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__startservice_startservice__["a" /* StartservicePage */], { 'nanyId': _this.usuario.product, 'userId': _this.uid, 'key': _this.usuario.key, 'user': _this.usuario.user });
                    }
                    else if (_this.usuario.status == 0) {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
                        console.log('hola');
                    }
                    else if (_this.usuario.status == 5) {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__rating_rating__["a" /* RatingPage */], { 'nanyId': _this.usuario.product, 'userId': _this.uid, 'key': _this.usuario.key });
                        console.log('rating');
                    }
                });
            }
        });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.signInWithFacebook = function () {
        if (this.platform.is('cordova')) {
            return this.fb.login(['email', 'public_profile']).then(function (res) {
                var facebookCredential = __WEBPACK_IMPORTED_MODULE_8_firebase_app__["auth"].FacebookAuthProvider.credential(res.authResponse.accessToken);
                return __WEBPACK_IMPORTED_MODULE_8_firebase_app__["auth"]().signInWithCredential(facebookCredential);
            });
        }
        else {
            return this.afAuth.auth
                .signInWithPopup(new __WEBPACK_IMPORTED_MODULE_8_firebase_app__["auth"].FacebookAuthProvider())
                .then(function (res) { return console.log(res); });
        }
    };
    LoginPage.prototype.signOut = function () {
        this.afAuth.auth.signOut();
    };
    LoginPage.prototype.bienvenida = function () {
        var _this = this;
        this.oneSignal.getIds().then(function (id) {
            console.log('MI ID de la suerte' + id.userId);
            var idPlayer = id.userId;
            _this.afDB.database.ref('users' + '/' + _this.uid + '/').update({ idPlayer: idPlayer });
            var noti = {
                app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                include_player_ids: [id.userId],
                data: { "foo": "bar" },
                contents: { en: "Nuestras nannies certificadas cuidarán de los niños." },
                headings: { en: "¡Bienvenido a Nannapp!" }
            };
            window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                console.log("Notification Post Success:", successResponse);
            }, function (failedResponse) {
                console.log("Notification Post Failed: ", failedResponse);
            });
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/login/login.html"*/'<ion-header hidden>\n\n  <ion-navbar>\n    <ion-title>Login</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content class="login-content content" padding>\n  <div class="login-box">\n      <br><br><br><br>\n      <ion-row>\n        <ion-col col-2></ion-col>\n        <ion-col text-center col-8>\n          <img src="./assets/imgs/nannapnegro.png" op-lazy-img>\n        </ion-col>\n      </ion-row>\n      <br>\n      <ion-row>\n        <ion-col class="signup-col">\n          <button ion-button class="submit-btn-fb" full (click)="signInWithFacebook()" icon-left color="#37639e">\n            <ion-icon name="logo-facebook"></ion-icon>\n            Inicia sesión con Facebook </button>\n            <ion-card color="black">\n              <ion-card-content>\n                  <ion-item text-center>\n                    <ion-label class="card-content-ios">\n                      Al iniciar sesión aceptas nuestras <br>\n                      políticas de privacidad disponibles <br>en nuestro sitio web.\n                    </ion-label>\n                    <!--<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>-->\n                  </ion-item>\n                  \n              </ion-card-content>\n            </ion-card>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col text-center>\n\n        </ion-col>\n      </ion-row>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_onesignal__["a" /* OneSignal */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 371:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(390);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_onesignal__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_paypal__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_call_number__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__agm_core__ = __webpack_require__(563);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_email_composer__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_ion_rating_ion_rating__ = __webpack_require__(568);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_camera__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_image_picker__ = __webpack_require__(569);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_file__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__app_component__ = __webpack_require__(570);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_home_home__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_perfil_perfil__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_category_category__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_nanny_nanny__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_request_request__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_requestform_requestform__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_reserved_reserved__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_modal_modal__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_startservice_startservice__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_chat_chat__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_info_info__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_informacion_informacion__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_historial_historial__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_rating_rating__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_nanysf_nanysf__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_contact_contact__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_politicas_politicas__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_id_id__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_comprobante_comprobante__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_preguntas_preguntas__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_comments_comments__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_foto_foto__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39_angularfire2__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ionic_native_facebook__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__providers_auth_auth__ = __webpack_require__(573);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__providers_pushnotification_pushnotification__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__global_components___ = __webpack_require__(574);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__global_directives___ = __webpack_require__(576);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__global_services___ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48_ionic_image_loader__ = __webpack_require__(370);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













//import { BarcodeScanner } from '@ionic-native/barcode-scanner';



//pages























// firebase




//providers


// import { UbicacionProvider } from '../providers/ubicacion/ubicacion';




var firebaseConfig = {
    apiKey: "AIzaSyDdOavcjmsfa-KTXW1EbtuHOalvMCdB8Mk",
    authDomain: "nannaapp-1e22e.firebaseapp.com",
    databaseURL: "https://nannaapp-1e22e.firebaseio.com",
    projectId: "nannaapp-1e22e",
    storageBucket: "nannaapp-1e22e.appspot.com",
    messagingSenderId: "748451395664"
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_16__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_17__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_category_category__["a" /* CategoryPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_nanny_nanny__["a" /* NannyPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_request_request__["a" /* RequestPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_requestform_requestform__["a" /* RequestformPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_reserved_reserved__["a" /* ReservedPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_modal_modal__["a" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_startservice_startservice__["a" /* StartservicePage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_info_info__["a" /* InfoPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_informacion_informacion__["a" /* InformacionPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_historial_historial__["a" /* HistorialPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_rating_rating__["a" /* RatingPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_nanysf_nanysf__["a" /* NanysfPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_politicas_politicas__["a" /* PoliticasPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_id_id__["a" /* IdPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_comprobante_comprobante__["a" /* ComprobantePage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_preguntas_preguntas__["a" /* PreguntasPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_comments_comments__["a" /* CommentsPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_foto_foto__["a" /* FotoPage */],
                __WEBPACK_IMPORTED_MODULE_12__components_ion_rating_ion_rating__["a" /* IonRating */],
                __WEBPACK_IMPORTED_MODULE_45__global_components___["a" /* LazyImgComponent */],
                __WEBPACK_IMPORTED_MODULE_46__global_directives___["a" /* LazyLoadDirective */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_16__app_component__["a" /* MyApp */], {
                    backButtonText: 'Atras'
                }, {
                    links: [
                        { loadChildren: '../pages/category/category.module#CategoryPageModule', name: 'CategoryPage', segment: 'category', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/comments/comments.module#CommentsPageModule', name: 'CommentsPage', segment: 'comments', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/comprobante/comprobante.module#ComprobantePageModule', name: 'ComprobantePage', segment: 'comprobante', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/foto/foto.module#FotoPageModule', name: 'FotoPage', segment: 'foto', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/historial/historial.module#HistorialPageModule', name: 'HistorialPage', segment: 'historial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/id/id.module#IdPageModule', name: 'IdPage', segment: 'id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/informacion/informacion.module#InformacionPageModule', name: 'InformacionPage', segment: 'informacion', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/info/info.module#InfoPageModule', name: 'InfoPage', segment: 'info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modal/modal.module#ModalPageModule', name: 'ModalPage', segment: 'modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/nanny/nanny.module#NannyPageModule', name: 'NannyPage', segment: 'nanny', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/nanysf/nanysf.module#NanysfPageModule', name: 'NanysfPage', segment: 'nanysf', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/politicas/politicas.module#PoliticasPageModule', name: 'PoliticasPage', segment: 'politicas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/perfil/perfil.module#PerfilPageModule', name: 'PerfilPage', segment: 'perfil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/preguntas/preguntas.module#PreguntasPageModule', name: 'PreguntasPage', segment: 'preguntas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/rating/rating.module#RatingPageModule', name: 'RatingPage', segment: 'rating', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/request/request.module#RequestPageModule', name: 'RequestPage', segment: 'request', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/requestform/requestform.module#RequestformPageModule', name: 'RequestformPage', segment: 'requestform', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reserved/reserved.module#ReservedPageModule', name: 'ReservedPage', segment: 'reserved', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/startservice/startservice.module#StartservicePageModule', name: 'StartservicePage', segment: 'startservice', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_39_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_40_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_41_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_10__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyBTAwxynPEvzqFj7RAVwXxYNLUPgx5xwcg'
                }),
                __WEBPACK_IMPORTED_MODULE_48_ionic_image_loader__["a" /* IonicImageLoader */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_16__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_17__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_category_category__["a" /* CategoryPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_nanny_nanny__["a" /* NannyPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_request_request__["a" /* RequestPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_requestform_requestform__["a" /* RequestformPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_reserved_reserved__["a" /* ReservedPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_modal_modal__["a" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_startservice_startservice__["a" /* StartservicePage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_info_info__["a" /* InfoPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_informacion_informacion__["a" /* InformacionPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_historial_historial__["a" /* HistorialPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_rating_rating__["a" /* RatingPage */],
                __WEBPACK_IMPORTED_MODULE_12__components_ion_rating_ion_rating__["a" /* IonRating */],
                __WEBPACK_IMPORTED_MODULE_31__pages_nanysf_nanysf__["a" /* NanysfPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_politicas_politicas__["a" /* PoliticasPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_id_id__["a" /* IdPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_comprobante_comprobante__["a" /* ComprobantePage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_preguntas_preguntas__["a" /* PreguntasPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_foto_foto__["a" /* FotoPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_comments_comments__["a" /* CommentsPage */],
                __WEBPACK_IMPORTED_MODULE_45__global_components___["a" /* LazyImgComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_40_angularfire2_database__["a" /* AngularFireDatabase */],
                __WEBPACK_IMPORTED_MODULE_42__ionic_native_facebook__["a" /* Facebook */],
                __WEBPACK_IMPORTED_MODULE_43__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_onesignal__["a" /* OneSignal */],
                __WEBPACK_IMPORTED_MODULE_44__providers_pushnotification_pushnotification__["a" /* PushnotificationProvider */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_paypal__["a" /* PayPal */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                // UbicacionProvider,
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_email_composer__["a" /* EmailComposer */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                //BarcodeScanner,
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_image_picker__["a" /* ImagePicker */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_47__global_services___["a" /* ImgcacheService */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__category_category__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__reserved_reserved__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__historial_historial__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__requestform_requestform__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_onesignal__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, afAuth, alertCtrl, oneSignal, afDB, plt) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.afAuth = afAuth;
        this.alertCtrl = alertCtrl;
        this.oneSignal = oneSignal;
        this.afDB = afDB;
        this.plt = plt;
        this.usuario = {};
        this.tarifas = {};
        this.nannyP = {};
        this.bannerImage = {};
        this.uid = this.afAuth.auth.currentUser.uid;
        var route = 'products_meta/P201804250046482';
        this.newID = localStorage["newID"];
        console.log("*******************Local storage value = " + this.newID);
        this.afDB.object('tarifas').valueChanges().subscribe(function (data) {
            _this.tarifas = data;
            console.log(_this.tarifas);
        });
        this.afDB.object('users/' + this.uid + '/meta/standNanny').valueChanges().subscribe(function (data) {
            _this.standNanny = data;
            console.log(_this.standNanny);
        });
        /*
                        this.afDB.object('users/'+this.uid+'/newID').valueChanges().subscribe(data =>{
                          this.newID = data;
                          console.log(this.newID);
                        });
        */
        this.afDB.object('banner_images/').valueChanges().subscribe(function (data) {
            _this.bannerImage = data;
        });
        this.afDB.object('users/' + this.uid + '/' + 'meta').valueChanges().subscribe(function (data) {
            _this.usuario = data;
            route = 'products_meta/' + _this.standNanny;
            console.log('ruta ' + route);
            _this.afDB.object(route).valueChanges().subscribe(function (data) {
                _this.nannyP = data;
                //console.log("this nannyP stand "+this.nannyP.title);
            });
        });
        if ((plt.is('ios') || plt.is('android')) && this.newID == undefined) {
            console.log('iniciando la notificación, new ID= ' + this.newID);
            this.promptNotification();
        }
        else if ((plt.is('ios') || plt.is('android'))) {
            this.startN();
        }
        this.afDB.object('users/' + this.uid + '/idPlayer').valueChanges().subscribe(function (data) {
            _this.idPlayerUser = data;
            console.log(_this.idPlayerUser);
        });
    }
    HomePage.prototype.goToCategory = function (category) {
        console.log(category);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__category_category__["a" /* CategoryPage */], { 'category': category });
    };
    HomePage.prototype.goToReserved = function (nanyId, val) {
        console.log(nanyId, val);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__reserved_reserved__["a" /* ReservedPage */], { 'nanyId': nanyId, 'userId': this.uid, 'home': val });
    };
    HomePage.prototype.goToHistorial = function (nanyId, val) {
        console.log(nanyId, val);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__historial_historial__["a" /* HistorialPage */], { 'nanyId': nanyId, 'userId': this.uid, 'home': val });
    };
    HomePage.prototype.goRequestForm = function (nanyId, idPlayer, idPlayerUser) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: '<h4>PROGRAMA TU CUIDADO</h4>',
            message: '<h6>Numero de niños que requieren Nanny.</h6> <br>',
            inputs: [
                {
                    type: 'radio',
                    label: '1',
                    value: '1'
                },
                {
                    type: 'radio',
                    label: '2',
                    value: '2'
                },
                {
                    type: 'radio',
                    label: '3',
                    value: '3'
                }
            ],
            buttons: [
                {
                    text: "Cancelar",
                    handler: function (childrens) {
                        console.log(childrens);
                    }
                },
                {
                    text: "Aceptar",
                    handler: function (childrens) {
                        console.log(childrens);
                        console.log("***************id Player User" + idPlayerUser);
                        var price;
                        if (_this.nannyP.tarifa == 'BASIC') {
                            price = _this.tarifas.traditionalBasic;
                            console.log("*****la tarifa traditional basic es " + price);
                        }
                        else if (_this.nannyP.tarifa == 'LUXE') {
                            price = _this.tarifas.traditionalLuxe;
                            console.log("*****la tarifa traditional luxe es " + price);
                        }
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__requestform_requestform__["a" /* RequestformPage */], { 'nanyId': nanyId, 'childrens': childrens, 'price': price, 'category': 'stand100', 'idPlayer': idPlayer, 'idPlayerUser': idPlayerUser });
                    }
                }
            ]
        });
        prompt.present();
    };
    HomePage.prototype.startN = function () {
        var _this = this;
        this.oneSignal.getIds().then(function (id) {
            console.log('MI ID de la suerte' + id.userId);
            var idPlayer = id.userId;
            var newID = true;
            _this.afDB.database.ref('users' + '/' + _this.uid + '/').update({ idPlayer: idPlayer });
            _this.afDB.database.ref('users' + '/' + _this.uid + '/').update({ newID: newID });
            var noti = {
                app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                include_player_ids: [id.userId],
                data: { "foo": "bar" },
                contents: { en: "Ahora, déjanos ser tu nanny de la guarda." },
                headings: { en: "Notificaciones activadas" }
            };
            window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                console.log("Notification Post Success:", successResponse);
            }, function (failedResponse) {
                console.log("Notification Post Failed: ", failedResponse);
            });
        });
    };
    HomePage.prototype.promptNotification = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: '<h4>Activa notificaciones.</h4>',
            message: '<h6>Para el óptimo funcionamiento de la app requerimos que actives las notificaciones.</h6> <br>',
            buttons: [
                {
                    text: "Aceptar",
                    handler: function (childrens) {
                        _this.startN();
                        localStorage['newID'] = true;
                    }
                }
            ]
        });
        prompt.present();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/home/home.html"*/'<ion-header style="background-color: rgb(255, 255, 255);">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n        <img src="./assets/imgs/nannapnegro.png" width="50" height="50">\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <div *ngIf="this.usuario.user == 0">\n    <ion-grid>\n\n<!--stand menu-->\n<div *ngIf="this.usuario.standHours > 0">\n        <br>\n        <ion-row justify-content-center align-items-center style="height: 100%">\n          <ion-col col-6>\n            <div class="products card" style="background: transparent !important;" text-center>\n              <div class="products products-clear item item-stable item-image" style="background: transparent !important;">\n                <img style="max-width:100px !important;" src="http://caudillom.com/stand2.png"(click)="goRequestForm(this.usuario.standNanny,nannyP.idPlayer,idPlayerUser)" />\n              </div>\n              <div class="products products-clear item item-stable item-text-wrap" style="background: transparent !important;" text-center>\n                <span id="current-price" style="color:#505050;font-size: 20px;">\n                  Solicitar cuidado.\n                </span>\n              </div>\n            </div>\n          </ion-col>\n          <ion-col col-6>\n            <div text-center>\n              \n              <p style="color:#ff006f">\n                Paquete contratado.\n              </p>\n              <p>Horas restantes: {{usuario.standHours}}</p>\n              <p>Nanny: {{nannyP.nickname}}</p>\n              <!--<p>Vigencia: 20/05/19</p>\n              \n              <p>Nanny contratada: </p><p>{{nannyP.title}}</p>-->\n              \n            </div>\n          </ion-col>\n        </ion-row>\n</div>\n\n      <ion-row>\n        <ion-col col-12>\n          <div no-padding>\n            <ion-slides autoplay="1000" speed="500" pager="true">\n                \n              <div *ngIf="bannerImage.banner01!=\'.\'">\n                \n              <ion-slide>\n                    <img src="{{bannerImage.banner01}}">\n                </ion-slide>\n                </div>\n                <div *ngIf="bannerImage.banner02!=\'.\'">\n                \n                <ion-slide>\n                  <img src="{{bannerImage.banner02}}">\n                </ion-slide>\n                </div>\n                <div *ngIf="bannerImage.banner03!=\'.\'">\n                \n                <ion-slide>\n                 <img src="{{bannerImage.banner03}}">\n                </ion-slide>\n                </div>\n                <div *ngIf="bannerImage.banner04!=\'.\'">\n                \n                <ion-slide>\n                  <img src="{{bannerImage.banner04}}">\n                </ion-slide>\n                </div>\n                <div *ngIf="bannerImage.banner05!=\'.\'">\n                \n                <ion-slide>\n                  <img src="{{bannerImage.banner05}}">\n                </ion-slide>\n                </div>\n                <div *ngIf="bannerImage.banner06!=\'.\'">\n                \n                <ion-slide>\n                  <img src="{{bannerImage.banner06}}">\n                </ion-slide>\n                </div>\n                <div *ngIf="bannerImage.banner07!=\'.\'">\n                \n                <ion-slide>\n                  <img src="{{bannerImage.banner07}}">\n                </ion-slide>\n                </div>\n                <div *ngIf="bannerImage.banner08!=\'.\'">\n                \n                <ion-slide>\n                  <img src="{{bannerImage.banner08}}">\n                </ion-slide>\n                </div>\n                <div *ngIf="bannerImage.banner09!=\'.\'">\n                <ion-slide>\n                 <img src="{{bannerImage.banner09}}">\n                 </ion-slide>\n                </div>\n                 <div *ngIf="bannerImage.banner10!=\'.\'">\n                 <ion-slide>\n                   <img src="{{bannerImage.banner10}}">\n                 </ion-slide>\n                </div>\n            </ion-slides>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <br>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-6>\n          <div class="products card" style="background: transparent !important;" text-center>\n            <div class="products products-clear item item-stable item-image" style="background: transparent !important;">\n              <img style="max-width:100px !important;" src="http://caudillom.com/traditional3.png" (click)="goToCategory(\'traditional\')"/>\n            </div>\n            <div class="products products-clear item item-stable item-text-wrap" style="background: transparent !important;">\n              <span id="current-price" style="color:#505050;font-size: 20px;">\n                Traditional\n              </span>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <p style="color:#e2c81f">Tarifa por hora.</p>\n            <p style="font-size: 12px;"><!--BASIC &nbsp;--> <strong>{{tarifas.traditionalBasic | currency : "MXN" }}</strong> </p>\n            <!--<p style="font-size: 12px;">LUXE  &nbsp;&nbsp;&nbsp; <strong>{{tarifas.traditionalLuxe | currency : "MXN" }}</strong> </p>\n          -->\n            </div>\n        </ion-col>\n      </ion-row>\n\n      <!--\n      <br>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-6>\n          <div class="products card" style="background: transparent !important;" text-center>\n            <div class="products products-clear item item-stable item-image" style="background: transparent !important;">\n              <lazy-img style="max-width:100px !important;" inputSrc="./assets/imgs/party.png"(click)="goToCategory(\'day\')"></lazy-img>\n            </div>\n            <div class="products products-clear item item-stable item-text-wrap" style="background: transparent !important;" text-center>\n              <span id="current-price" style="color:#505050;font-size: 20px;">\n                Day\n              </span>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <p>Paquete 1 día</p>\n            <p style="color:#ff9100">\n              Paquete de hasta 12 horas <br>a partir de las 7 horas.\n            </p>\n            <p style="font-size: 12px;">\n              BASIC &nbsp; <strong>${{tarifas.standBasic}} MXN</strong>\n            </p>\n            <p style="color:#737373;font-size:10px;">\n              Costo regular: <strong>${{tarifas.traditionalBasic*12}} MXN</strong><br>\n              Ahorro: ${{(tarifas.traditionalBasic*12)-tarifas.standBasic}} MXN\n            </p>\n            <p style="font-size: 12px;">\n              LUXE &nbsp; <strong>${{tarifas.standLuxe}} MXN</strong>\n            </p>\n            <p style="color:#737373;font-size: 10px;">\n              Costo regular: <strong>${{(tarifas.traditionalLuxe*12)}} MXN</strong><br>\n              Ahorro: ${{(tarifas.traditionalLuxe*12)-tarifas.standLuxe}} MXN\n            </p>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <br>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-6>\n          <div class="products card" style="background: transparent !important;" text-center>\n            <div class="products products-clear item item-stable item-image" style="background: transparent !important;">\n              <lazy-img style="max-width:100px !important;" inputSrc="./assets/imgs/stand2.png"(click)="goToCategory(\'week\')"></lazy-img>\n            </div>\n            <div class="products products-clear item item-stable item-text-wrap" style="background: transparent !important;" text-center>\n              <span id="current-price" style="color:#505050;font-size: 20px;">\n                Week\n              </span>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <p>Nanny recurrente <br> Paquete 1 semana</p>\n            <p style="color:#ff006f">\n              Paquete de hasta 50 horas<br> en 1 semana.\n            </p>\n            <p style="font-size: 12px;">\n              BASIC &nbsp; <strong>${{tarifas.semanaBasic}} MXN</strong>\n            </p>\n            <p style="color:#737373;font-size:10px;">\n              Costo regular: <strong>${{tarifas.traditionalBasic*50}} MXN</strong><br>\n              Ahorro: ${{(tarifas.traditionalBasic*50)-tarifas.semanaBasic}} MXN\n            </p>\n            <p style="font-size: 12px;">\n              LUXE &nbsp; <strong>${{tarifas.semanaLuxe}} MXN</strong>\n            </p>\n            <p style="color:#737373;font-size: 10px;">\n              Costo regular: <strong>${{tarifas.traditionalLuxe*50}} MXN</strong><br>\n              Ahorro: ${{(tarifas.traditionalLuxe*50)-tarifas.semanaLuxe}} MXN\n            </p>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <br>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-6>\n          <div class="products card" style="background: transparent !important;" text-center>\n            <div class="products products-clear item item-stable item-image" style="background: transparent !important;">\n              <lazy-img style="max-width:100px !important;" inputSrc="./assets/imgs/dream2.png" (click)="goToCategory(\'dream\')"></lazy-img>\n            </div>\n            <div class="products products-clear item item-stable item-text-wrap" style="background: transparent !important;" text-center>\n              <span id="current-price" style="color:#505050;font-size: 20px;">\n                Dream\n              </span>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <p>Nanny nocturna</p>\n            <p class="doce">Paquete de hasta 12 horas<br>a partir de las <br> 19 horas.</p>\n            <p style="font-size: 12px;">\n              BASIC &nbsp;&nbsp;<strong>${{tarifas.dreamBasic}} MXN</strong>\n              <br><br>\n              LUXE  &nbsp;&nbsp;<strong>${{tarifas.dreamLuxe}} MXN</strong>\n            </p>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <br>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-6>\n          <div class="products card" style="background: transparent !important;" text-center>\n            <div class="products products-clear item item-stable item-image" style="background: transparent !important;">\n              <lazy-img style="max-width:100px !important;" inputSrc="./assets/imgs/trip2.png" (click)="goToCategory(\'trip\')"></lazy-img>\n            </div>\n            <div class="products products-clear item item-stable item-text-wrap" style="background: transparent !important;" text-center>\n              <span id="current-price" style="color:#505050;font-size: 20px;">\n                Trip\n              </span>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <p class="trip_int">Nanny para viajes <br>nacionales e internacionales.</p>\n            <p>Tarifa por día.</p>\n            <p style="font-size: 12px;">\n                <strong>${{tarifas.tripNacional}} MXN nacional</strong>\n              </p>\n              <p style="font-size: 12px;">\n                  <strong>${{tarifas.tripInternacional}} MXN internacional</strong>\n                </p>\n            <p>Tarifa por semana.</p>\n            <p style="font-size: 12px;">\n              <strong>${{tarifas.tripSemanaNacional}} MXN nacional</strong>\n            </p>\n            <p style="font-size: 12px;">\n                <strong>${{tarifas.tripSemanaInternacional}} MXN internacional</strong>\n              </p>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <br>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-6>\n          <div class="products card" style="background: transparent !important;" text-center>\n            <div class="products products-clear item item-stable item-image" style="background: transparent !important;">\n              <lazy-img style="max-width:100px !important;" inputSrc="./assets/imgs/party.png" (click)="goToCategory(\'party\')"></lazy-img>\n            </div>\n            <div class="products products-clear item item-stable item-text-wrap" style="background: transparent !important;" text-center>\n              <span id="current-price" style="color:#505050;font-size: 20px;">\n                Party\n              </span>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <p class="party">Nannies para eventos.</p>\n            <p>Paquete hasta 10 niños por hasta 8 horas.</p>\n            <p style="font-size: 12px;">\n              <strong>${{tarifas.party10}} MXN</strong>\n            </p>\n            <p>Paquete hasta 20 niños por hasta 10 horas.</p>\n            <p style="font-size: 12px;">\n              <strong>${{tarifas.party20}} MXN</strong>\n            </p>\n          </div>\n        </ion-col>\n      </ion-row>\n    -->\n    <div *ngIf="12 > this.usuario.standHours || this.usuario.standHours == undefined">\n      <br>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-6>\n          <div class="products card" style="background: transparent !important;" text-center>\n            <div class="products products-clear item item-stable item-image" style="background: transparent !important;">\n              <img style="max-width:100px !important;" src="http://caudillom.com/stand2.png"(click)="goToCategory(\'stand\')"/>\n            </div>\n            <div class="products products-clear item item-stable item-text-wrap" style="background: transparent !important;" text-center>\n              <span id="current-price" style="color:#505050;font-size: 20px;">\n                Stand\n              </span>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <p>Nanny diurna recurrente<br> hasta las 21 horas</p>\n            <p style="color:#ff006f">\n                50 horas<br> Vigencia: 1 mes.\n              </p>\n              <p style="font-size: 12px;">\n                <!--BASIC &nbsp;--> <strong>{{tarifas.month | currency : "MXN" }} </strong>\n              </p>\n            <p style="color:#ff006f">\n              100 horas<br> Vigencia: 3 meses.\n            </p>\n            <p style="font-size: 12px;">\n              <!--BASIC &nbsp;--> <strong>{{tarifas.standBasic | currency : "MXN" }} </strong>\n            </p>\n            <!--<p style="color:#737373;font-size:10px;">\n              Costo regular: <strong>${{tarifas.traditionalBasic*100}} MXN</strong><br>\n              Ahorro: ${{(tarifas.traditionalBasic*100)-tarifas.standBasic}} MXN\n            </p>\n            <p style="font-size: 12px;">\n              LUXE &nbsp; <strong>{{tarifas.standLuxe | currency : "MXN" }}</strong>\n            </p>\n            <p style="color:#737373;font-size: 10px;">\n              Costo regular: <strong>${{tarifas.traditionalLuxe*100}} MXN</strong><br>\n              Ahorro: ${{(tarifas.traditionalLuxe*100)-tarifas.standLuxe}} MXN\n            </p>-->\n          </div>\n        </ion-col>\n      </ion-row>\n    </div>\n\n\n      <br>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-6>\n          <div class="products card" style="background: transparent !important;" text-center>\n            <div class="products products-clear item item-stable item-image" style="background: transparent !important;">\n              <img style="max-width:100px !important;" src="http://caudillom.com/dream2.png" (click)="goToCategory(\'dream\')"/>\n            </div>\n            <div class="products products-clear item item-stable item-text-wrap" style="background: transparent !important;" text-center>\n              <span id="current-price" style="color:#505050;font-size: 20px;">\n                Dream\n              </span>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <p>Nanny nocturna</p>\n            <p class="doce">Paquete de 12 horas<br>a partir de las <br> 19 horas.</p>\n            <p style="font-size: 12px;">\n              <!--BASIC &nbsp;&nbsp;--><strong>{{tarifas.dreamBasic | currency : "MXN"}}</strong>\n             <!-- <br><br>\n              LUXE  &nbsp;&nbsp;<strong>{{tarifas.dreamLuxe | currency : "MXN"}} </strong>\n            --></p>\n          </div>\n        </ion-col>\n      </ion-row>\n\n\n\n\n      <br>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-6>\n          <div class="products card" style="background: transparent !important;" text-center>\n            <div class="products products-clear item item-stable item-image" style="background: transparent !important;">\n              <img style="max-width:100px !important;" src="http://caudillom.com/special2.png" (click)="goToCategory(\'special\')"/>\n            </div>\n            <div class="products products-clear item item-stable item-text-wrap" style="background: transparent !important;" text-center>\n              <span id="current-price" style="color:#505050;font-size: 20px;">\n                Special\n              </span>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <p class="special">Nanny para niños con capacidades diferentes.</p>\n            <p>Tarifa por hora.</p>\n            <p style="font-size: 12px;">\n              <strong>{{tarifas.special | currency : "MXN"}}</strong>\n            </p>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      \n\n      <br>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-6>\n          <div class="products card" style="background: transparent !important;" text-center>\n            <div class="products products-clear item item-stable item-image" style="background: transparent !important;">\n              <img style="max-width:100px !important;" src="http://caudillom.com/party.png" (click)="goToCategory(\'grand\')"/>\n            </div>\n            <div class="products products-clear item item-stable item-text-wrap" style="background: transparent !important;" text-center>\n              <span id="current-price" style="color:#505050;font-size: 20px;">\n                Grand\n              </span>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <p class="party">Nanny para adultos mayores.</p>\n            <p>Tarifa por hora.</p>\n            <p style="font-size: 12px;">\n              <strong>{{tarifas.grand | currency : "MXN"}} </strong>\n            </p>\n          </div>\n        </ion-col>\n      </ion-row>\n      \n      \n    </ion-grid>\n  </div>\n  <div *ngIf="usuario.user == 1">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-12>\n          <div no-padding>\n              <ion-slides autoplay="5000" loop="true" speed="500">\n                <div *ngIf="bannerImage.bannerN01!=\'.\'">\n                \n                  <ion-slide>\n                        <img src="{{bannerImage.bannerN01}}">\n                    </ion-slide>\n                    </div>\n                    <div *ngIf="bannerImage.bannerN02!=\'.\'">\n                    \n                    <ion-slide>\n                      <img src="{{bannerImage.bannerN02}}">\n                    </ion-slide>\n                    </div>\n                    <div *ngIf="bannerImage.bannerN03!=\'.\'">\n                    \n                    <ion-slide>\n                     <img src="{{bannerImage.bannerN03}}">\n                    </ion-slide>\n                    </div>\n                    <div *ngIf="bannerImage.bannerN04!=\'.\'">\n                    \n                    <ion-slide>\n                      <img src="{{bannerImage.bannerN04}}">\n                    </ion-slide>\n                    </div>\n                    <div *ngIf="bannerImage.bannerN05!=\'.\'">\n                    \n                    <ion-slide>\n                      <img src="{{bannerImage.bannerN05}}">\n                    </ion-slide>\n                    </div>\n                    <div *ngIf="bannerImage.bannerN06!=\'.\'">\n                    \n                    <ion-slide>\n                      <img src="{{bannerImage.bannerN06}}">\n                    </ion-slide>\n                    </div>\n                    <div *ngIf="bannerImage.bannerN07!=\'.\'">\n                    \n                    <ion-slide>\n                      <img src="{{bannerImage.bannerN07}}">\n                    </ion-slide>\n                    </div>\n                    <div *ngIf="bannerImage.bannerN08!=\'.\'">\n                    \n                    <ion-slide>\n                      <img src="{{bannerImage.bannerN08}}">\n                    </ion-slide>\n                    </div>\n                    <div *ngIf="bannerImage.bannerN09!=\'.\'">\n                    <ion-slide>\n                     <img src="{{bannerImage.bannerN09}}">\n                     </ion-slide>\n                    </div>\n                     <div *ngIf="bannerImage.bannerN10!=\'.\'">\n                     <ion-slide>\n                       <img src="{{bannerImage.bannerN10}}">\n                     </ion-slide>\n                    </div>\n                </ion-slides>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-12>\n          <button ion-button block color="dark" (click)="goToReserved(usuario.product, \'home\')">Solicitudes de Cuidado</button>\n        </ion-col>\n      </ion-row>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n        <ion-col col-12>\n          <button ion-button block color="dark" (click)="goToHistorial(usuario.product, \'home\')">Historial</button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReservedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal_modal__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__startservice_startservice__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__request_request__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__ = __webpack_require__(15);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ReservedPage = /** @class */ (function () {
    function ReservedPage(navCtrl, navParams, afDB, afAuth, modalCtrl, viewCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.usuario = {};
        this.nanyId = {};
        this.nannyP = {};
        this.ordersN = {};
        this.ordersU = {};
        this.usua = {};
        this.home = {};
        this.data = {};
        this.total = this.navParams.get("total");
        console.log("total pasado a request " + this.total);
        this.nanyId = this.navParams.get("nanyId");
        console.log('El id de la nany', this.nanyId);
        this.userId = this.afAuth.auth.currentUser.uid;
        console.log(this.userId);
        this.home = this.navParams.get("home");
        console.log(this.home);
        this.uid = this.afAuth.auth.currentUser.uid;
        this.afDB.object('products_meta/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nannyP = data;
            console.log(_this.nannyP);
        });
        this.afDB.object('users/' + this.userId + '/' + 'meta').valueChanges().subscribe(function (data) {
            _this.usuario = data;
            console.log(_this.usuario);
        });
        this.usua = afDB.list('users/').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.ordersN = afDB.list('orders_n/' + this.nanyId).snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.ordersU = afDB.list('orders/' + this.userId).snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.items = afDB.list('products_meta/').snapshotChanges().map(function (data) {
            return data.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        this.afDB.object('users/' + this.uid + '/meta/standHours').valueChanges().subscribe(function (data) {
            _this.userHours = data;
        });
    }
    ReservedPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReservedPage');
        this.viewCtrl.showBackButton(false);
    };
    ReservedPage.prototype.modal_detalles = function (key, userId) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__modal_modal__["a" /* ModalPage */], { 'nanyId': this.nanyId, 'userId': userId, 'key': key });
        modal.present();
    };
    ReservedPage.prototype.modal_detallesU = function (key, nanyId) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__modal_modal__["a" /* ModalPage */], { 'nanyId': nanyId, 'userId': this.userId, 'key': key });
        modal.present();
    };
    ReservedPage.prototype.aceptService = function (key, userId, idPlayerUser, pago) {
        var status = 1;
        this.afDB.database.ref('orders' + '/' + userId + '/' + key + '/data/').update({ status: status });
        this.afDB.database.ref('orders_n' + '/' + this.nanyId + '/' + key + '/data/').update({ status: status });
        this.aceptServiceNoti(idPlayerUser, pago);
    };
    ReservedPage.prototype.aceptServiceNoti = function (idPlayerUser, pago) {
        var _this = this;
        console.log('Este es el Plyer id' + idPlayerUser);
        this.afDB.object('products_meta/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.data = data;
            if (!_this.data) {
                _this.data = {};
            }
            var noti = {
                app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                include_player_ids: [idPlayerUser],
                data: { "foo": "bar" },
                contents: { en: "Tu nanny " + _this.data.title + " ha aceptado el cuidado." }
            };
            window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                console.log("Notification Post Success:", successResponse);
            }, function (failedResponse) {
                console.log("Notification Post Failed: ", failedResponse);
                alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
            });
        });
        if (pago == undefined) {
            var noti = {
                app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                include_player_ids: [idPlayerUser],
                data: { "foo": "bar" },
                contents: { en: "Por favor realiza tu pago para poder ofrecerte el cuidado." }
            };
            window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                console.log("Notification Post Success:", successResponse);
            }, function (failedResponse) {
                console.log("Notification Post Failed: ", failedResponse);
                alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
            });
        }
    };
    ReservedPage.prototype.cancelService = function (key, userId, nannyId, idPlayerNannyUser, user) {
        var _this = this;
        var category;
        this.afDB.object('orders/' + userId + "/" + key + "/data/category").valueChanges().subscribe(function (data) {
            category = data;
            console.log("catego:  " + category);
            if (category == 'stand100') {
                var currentMonth = new Date().getMonth() + 1;
                var currentHour_1 = new Date().getHours();
                var currentDay = new Date().getDate();
                var currentDate_1;
                if (currentDay < 10) {
                    currentDate_1 = new Date().getFullYear() + "-" + currentMonth + "-0" + currentDay;
                }
                else {
                    currentDate_1 = new Date().getFullYear() + "-" + currentMonth + "-" + currentDay;
                }
                console.log("la fecha actual es DDMMHH " + currentDate_1);
                var careDate_1;
                var careTime_1;
                var careHour_1;
                _this.afDB.object('orders/' + userId + "/" + key + "/data/myDate").valueChanges().subscribe(function (data) {
                    careDate_1 = data;
                    console.log("la fecha del cuidado es  " + careDate_1);
                    //if care date is the same as current
                    if (currentDate_1 == careDate_1) {
                        _this.afDB.object('orders/' + userId + "/" + key + "/data/myTime").valueChanges().subscribe(function (data) {
                            careTime_1 = data;
                            careHour_1 = parseInt(careTime_1 + "".split(":")[0]);
                            //if user gets a reembolso
                            if (currentHour_1 + 3 <= careHour_1) {
                                var alert_1 = _this.alertCtrl.create({
                                    title: '¿Estas seguro de cancelar el servicio?',
                                    message: 'tus horas se reestableceran en tu paquete stand.',
                                    buttons: [
                                        {
                                            text: 'Cancelar',
                                            role: 'Cancelar',
                                            handler: function () {
                                                console.log('Cancel clicked');
                                            }
                                        },
                                        {
                                            text: 'Aceptar',
                                            handler: function () {
                                                var careHours;
                                                _this.afDB.object('orders/' + userId + "/" + key + "/data/timeCuidado").valueChanges().subscribe(function (data) {
                                                    careHours = data;
                                                    _this.reembolso(careHours);
                                                });
                                                var status = 4;
                                                var cancel = user;
                                                console.log(key);
                                                console.log(userId);
                                                console.log(nannyId);
                                                _this.afDB.database.ref('orders' + '/' + userId + '/' + key + '/data/').update({ status: status, cancel: cancel });
                                                _this.afDB.database.ref('orders_n' + '/' + nannyId + '/' + key + '/data/').update({ status: status, cancel: cancel });
                                                //this.ServiceCancelNoti(nannyId, idPlayerNannyUser);
                                                console.log('Este es el Plyer id' + idPlayerNannyUser);
                                            }
                                        }
                                    ]
                                });
                                alert_1.present();
                                //if user lost hours
                            }
                            else {
                                var alert_2 = _this.alertCtrl.create({
                                    title: '¿Estas seguro de cancelar el servicio?',
                                    message: 'tus horas se perderan de acuerdo a los terminos del servicio.',
                                    buttons: [
                                        {
                                            text: 'Cancelar',
                                            role: 'Cancelar',
                                            handler: function () {
                                                console.log('Cancel clicked');
                                            }
                                        },
                                        {
                                            text: 'Aceptar',
                                            handler: function () {
                                                var status = 4;
                                                var cancel = user;
                                                console.log(key);
                                                console.log(userId);
                                                console.log(nannyId);
                                                _this.afDB.database.ref('orders' + '/' + userId + '/' + key + '/data/').update({ status: status, cancel: cancel });
                                                _this.afDB.database.ref('orders_n' + '/' + nannyId + '/' + key + '/data/').update({ status: status, cancel: cancel });
                                                //this.ServiceCancelNoti(nannyId, idPlayerNannyUser);
                                                console.log('Este es el Plyer id' + idPlayerNannyUser);
                                            }
                                        }
                                    ]
                                });
                                alert_2.present();
                            }
                        });
                        //if care date is before
                    }
                    else {
                        var alert_3 = _this.alertCtrl.create({
                            title: '¿Estas seguro de cancelar el servicio?',
                            message: 'tus horas se reestableceran en tu paquete stand.',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'Cancelar',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                },
                                {
                                    text: 'Aceptar',
                                    handler: function () {
                                        var careHours;
                                        _this.afDB.object('orders/' + userId + "/" + key + "/data/timeCuidado").valueChanges().subscribe(function (data) {
                                            careHours = data;
                                            _this.reembolso(careHours);
                                        });
                                        var status = 4;
                                        var cancel = user;
                                        console.log(key);
                                        console.log(userId);
                                        console.log(nannyId);
                                        _this.afDB.database.ref('orders' + '/' + userId + '/' + key + '/data/').update({ status: status, cancel: cancel });
                                        _this.afDB.database.ref('orders_n' + '/' + nannyId + '/' + key + '/data/').update({ status: status, cancel: cancel });
                                        //this.ServiceCancelNoti(nannyId, idPlayerNannyUser);
                                        console.log('Este es el Plyer id' + idPlayerNannyUser);
                                    }
                                }
                            ]
                        });
                        alert_3.present();
                    }
                });
            }
            else {
                var alert_4 = _this.alertCtrl.create({
                    title: 'Confirmar cancelación',
                    message: 'Estas seguro de cancelar el servicio',
                    buttons: [
                        {
                            text: 'Cancelar',
                            role: 'Cancelar',
                            handler: function () {
                                console.log('Cancel clicked');
                            }
                        },
                        {
                            text: 'Aceptar',
                            handler: function () {
                                var status = 4;
                                var cancel = user;
                                console.log(key);
                                console.log(userId);
                                console.log(nannyId);
                                _this.afDB.database.ref('orders' + '/' + userId + '/' + key + '/data/').update({ status: status, cancel: cancel });
                                _this.afDB.database.ref('orders_n' + '/' + nannyId + '/' + key + '/data/').update({ status: status, cancel: cancel });
                                //this.ServiceCancelNoti(nannyId, idPlayerNannyUser);
                                console.log('Este es el Plyer id' + idPlayerNannyUser);
                            }
                        }
                    ]
                });
                alert_4.present();
            }
        });
        /*let alert = this.alertCtrl.create({
          title: 'Confirmar cancelación',
          message: 'Estas seguro de cancelar el servicio',
          buttons: [
            {
              text: 'Cancelar',
              role: 'Cancelar',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Aceptar',
              handler: () => {
                let status = 4;
                let cancel = user;
                console.log(key);
                console.log(userId);
                console.log(nannyId);
  
                this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data/').update({status, cancel});
  
                this.afDB.database.ref('orders_n'+'/'+nannyId+'/'+key+'/data/').update({status, cancel});
  
                this.ServiceCancelNoti(nannyId, idPlayerNannyUser);
  
                console.log('Este es el Plyer id'+idPlayerNannyUser);
              }
            }
          ]
        });
        alert.present();*/
    };
    ReservedPage.prototype.reembolso = function (serviceTime) {
        var standHours;
        standHours = parseInt(serviceTime) + parseInt(this.userHours);
        this.afDB.database.ref('users/' + this.userId + '/meta').update({ standHours: standHours });
    };
    ReservedPage.prototype.ServiceCancelNoti = function (nannyId, idPlayerNannyUser) {
        var _this = this;
        console.log(idPlayerNannyUser + 'Este es el Player id');
        this.afDB.object('users/' + this.uid + '/' + 'meta/').valueChanges().subscribe(function (data) {
            _this.data = data;
            if (!_this.data) {
                _this.data = {};
            }
            console.log('Este es el Plyer id' + idPlayerNannyUser);
            if (_this.data.user == 0) {
                var noti = {
                    app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                    include_player_ids: [idPlayerNannyUser],
                    data: { "foo": "bar" },
                    contents: { en: _this.data.displayName + " ha cancelado el cuidado." },
                };
                window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                    console.log("Notification Post Success:", successResponse);
                }, function (failedResponse) {
                    console.log("Notification Post Failed: ", failedResponse);
                    alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
                });
            }
            else {
                _this.afDB.object('products_meta/' + nannyId).valueChanges().subscribe(function (data) {
                    _this.data = data;
                    if (!_this.data) {
                        _this.data = {};
                    }
                    var noti = {
                        app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                        include_player_ids: [idPlayerNannyUser],
                        data: { "foo": "bar" },
                        contents: { en: "Tu nanny " + _this.data.title + " ha cancelado el cuidado." }
                    };
                    window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                        console.log("Notification Post Success:", successResponse);
                    }, function (failedResponse) {
                        console.log("Notification Post Failed: ", failedResponse);
                        alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
                    });
                });
            }
            console.log(_this.data.displayName);
        });
    };
    ReservedPage.prototype.startService = function (key, userId, idPlayerUser, timeCuidado) {
        console.log(timeCuidado);
        var status = 2;
        var product = this.nanyId;
        this.afDB.database.ref('orders' + '/' + userId + '/' + key + '/data' + '/').update({ status: status });
        this.afDB.database.ref('orders_n' + '/' + this.nanyId + '/' + key + '/data/').update({ status: status });
        this.afDB.database.ref('users' + '/' + userId + '/' + 'meta/').update({ status: status, key: key, product: product });
        this.afDB.database.ref('users' + '/' + this.userId + '/' + 'meta/').update({ status: status, key: key, product: product });
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__startservice_startservice__["a" /* StartservicePage */], { 'nanyId': this.nanyId, 'userId': userId, 'key': key, 'timeCuidado': timeCuidado });
        this.StartServiceNoti(product, idPlayerUser);
    };
    ReservedPage.prototype.StartServiceNoti = function (nannyId, idPlayerUser) {
        var _this = this;
        console.log('Este es el Plyer id' + idPlayerUser);
        this.afDB.object('products_meta/' + nannyId).valueChanges().subscribe(function (data) {
            _this.data = data;
            if (!_this.data) {
                _this.data = {};
            }
            var noti = {
                app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                include_player_ids: [idPlayerUser],
                data: { "foo": "bar" },
                contents: { en: "Tu nanny " + _this.data.title + " ha comenzado el cuidado." }
            };
            window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                console.log("Notification Post Success:", successResponse);
            }, function (failedResponse) {
                console.log("Notification Post Failed: ", failedResponse);
                alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
            });
        });
    };
    ReservedPage.prototype.goToHome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    ReservedPage.prototype.goToPaymet = function (nanyId, codigo, key) {
        console.log(nanyId);
        console.log(codigo);
        console.log(key);
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__request_request__["a" /* RequestPage */], { 'nanyId': nanyId, 'userId': codigo, 'key': key });
        modal.present();
    };
    ReservedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-reserved',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/reserved/reserved.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Reservaciones</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div *ngIf="usuario.user == 0">\n    <div *ngFor="let orderU of ordersU | async">\n      <div *ngIf="orderU.data.status == 0 || orderU.data.status == 1">\n        <ion-card>\n          <ion-card-header>\n            <ion-title>\n              <ion-grid>\n                <!-- <ion-row>\n                  <ion-col col-4></ion-col>\n                  <ion-col col-4>\n                      Estatus\n                  </ion-col>\n                  <ion-col col-4></ion-col>\n                </ion-row> -->\n                <ion-row>\n                  <ion-col col-12>\n                    <div *ngIf="orderU.data.pago == undefined">\n                      <h2 ion-text color="danger">Pendiente de pago</h2>\n                    </div>\n                    <div *ngIf="orderU.data.status == 0 && orderU.data.pago != undefined">\n                      <h2 ion-text color="dark">Pendiente</h2>\n                    </div>\n                    <div *ngIf="orderU.data.status == 1">\n                      <h2 ion-text color="secondary">Aceptado</h2>\n                    </div>\n                    <div *ngIf="orderU.data.status == 2">\n                      <button ion-button round color="secondary" (click)="goService(orderU.key, orderU.idNanny, orderU.idUser)">En curso</button>\n                    </div>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n              <ion-grid>\n                <ion-row>\n                  <ion-col>\n                    <div text-wrap *ngFor="let item of items | async">\n                      <div *ngIf="item.key == orderU.data.idNanny">\n                        {{item.title}}\n                      </div>\n                    </div>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-title>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-grid>\n              <ion-row>\n                <ion-col col-12>\n                  <div text-center>\n                    <p style="text-transform: uppercase;" *ngIf="orderU.data.category!=\'stand\'">\n                      Reservado para el día {{ orderU.data.myDate | date: \'dd-MM-yyyy\' }} a las {{ orderU.data.myTime }}.\n                    </p>\n                    <p style="text-transform: uppercase;" *ngIf="orderU.data.category==\'stand\'">\n                        Stand {{orderU.data.horas}} horas.\n                      </p>\n                  </div>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-6>\n                  <button ion-button block color="dark" (click)="modal_detallesU(orderU.key, orderU.data.idNanny)">Detalles</button>\n                </ion-col>\n                <ion-col col-6>\n                  <button ion-button block color="danger" (click)="cancelService(orderU.key,orderU.data.idUser,orderU.data.idNanny,orderU.data.idPlayerNanny,0)">Cancelar</button>\n                </ion-col>\n                <ion-col col-12 *ngIf="orderU.data.pago == undefined">\n                  <button ion-button block color="default" (click)="goToPaymet(orderU.data.idNanny,orderU.data.idUser,orderU.key)">Pagar</button>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-card-content>\n        </ion-card>\n      </div>\n    </div>\n  </div>\n  <div *ngIf="usuario.user == 1">\n    <div *ngFor="let orderN of ordersN | async">\n      <div *ngIf="orderN.data.status == 0 || orderN.data.status == 1">\n        <ion-card>\n          <ion-card-header>\n            <ion-title>\n              <div text-wrap *ngFor="let us of this.usua | async">\n                <div *ngIf="orderN.data.idUser == us.key">\n                  {{us.meta.displayName }}\n                </div>\n              </div>\n            </ion-title>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-grid>\n              <ion-row>\n                <ion-col col-12>\n                  <div *ngIf="orderN.data.status == 2 && orderN.data.pago != undefined">\n                    <button ion-button round color="secondary" (click)="goService(orderN.key, orderN.idNanny, orderU.idUser)">En curso</button>\n                  </div>\n                  <div *ngIf="orderN.data.pago == undefined">\n                    <h2 ion-text color="danger">Pago pendiente</h2>\n                  </div>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>\n                  <div text-left>\n                    <p style="text-transform: uppercase;">\n                      Reservado para el día {{orderN.data.myDate | date: \'dd-MM-yyyy\'}} a las {{orderN.data.myTime}}.\n                    </p>\n                  </div>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-4>\n                  <div text-wrap *ngFor="let us of this.usua | async">\n                    <div *ngIf="orderN.data.idUser == us.key">\n                      <button ion-button block color="dark" (click)="modal_detalles(orderN.key,us.key)">Detalles</button>\n                    </div>\n                  </div>\n                </ion-col>\n                <ion-col col-4>\n                  <div *ngIf="orderN.data.status == 0">\n                    <div text-wrap *ngFor="let us of this.usua | async">\n                      <div *ngIf="orderN.data.idUser == us.key">\n                        <button ion-button block color="secondary" (click)="aceptService(orderN.key,orderN.data.idUser,orderN.data.idPlayerUser,orderN.data.pago)">Aceptar</button>\n                      </div>\n                    </div>\n                  </div>\n                  <div *ngIf="orderN.data.status == 1">\n                    <div text-wrap *ngFor="let us of this.usua | async">\n                      <div *ngIf="orderN.data.idUser == us.key">\n                        <button ion-button block color="dark" (click)="startService(orderN.key,orderN.data.idUser,orderN.data.idPlayerUser,orderN.data.timeCuidado)">Comenzar</button>\n                      </div>\n                    </div>\n                  </div>\n                </ion-col>\n                <ion-col col-4>\n                  <div text-wrap *ngFor="let us of this.usua | async">\n                    <div *ngIf="orderN.data.idUser == us.key">\n                      <button ion-button block color="danger" (click)="cancelService(orderN.key,orderN.data.idUser,orderN.data.idNanny,orderN.data.idPlayerUser,1)">Cancelar</button>\n                    </div>\n                  </div>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-card-content>\n        </ion-card>\n      </div>\n    </div>\n  </div>\n</ion-content>\n<div *ngIf="home == \'home\'">\n  <ion-footer>\n    <ion-toolbar>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-12>\n            <div (click)="goToHome()" text-center>\n              <ion-icon ios="ios-home" md="md-home"></ion-icon>\n              NANNAPP\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-toolbar>\n  </ion-footer>\n</div>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/reserved/reserved.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ReservedPage);
    return ReservedPage;
}());

//# sourceMappingURL=reserved.js.map

/***/ }),

/***/ 568:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IonRating; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IonRating = /** @class */ (function () {
    function IonRating() {
        this.numStars = 5;
        this.value = 2.5;
        this.readOnly = false;
        this.ionClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.stars = [];
    }
    IonRating.prototype.ngAfterViewInit = function () {
        this.calc();
    };
    IonRating.prototype.calc = function () {
        this.stars = [];
        var tmp = this.value;
        for (var i = 0; i < this.numStars; i++, tmp--) {
            if (tmp >= 1)
                this.stars.push("star");
            else if (tmp > 0 && tmp < 1)
                this.stars.push("star-half");
            else
                this.stars.push("star-outline");
        }
    };
    IonRating.prototype.starClicked = function (index) {
        // console.log(index);
        if (!this.readOnly) {
            this.value = index + 1;
            this.ionClick.emit(this.value);
            this.calc();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Number)
    ], IonRating.prototype, "numStars", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Number)
    ], IonRating.prototype, "value", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Boolean)
    ], IonRating.prototype, "readOnly", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], IonRating.prototype, "ionClick", void 0);
    IonRating = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ion-rating',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/components/ion-rating/ion-rating.html"*/'<ion-grid class="rating-container">\n  <ion-row>\n    <ion-col *ngFor="let star of stars; let i = index " tappable (click)="starClicked(i)">\n      <ion-icon [name]="star"></ion-icon>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/components/ion-rating/ion-rating.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], IonRating);
    return IonRating;
}());

//# sourceMappingURL=ion-rating.js.map

/***/ }),

/***/ 570:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_perfil_perfil__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_pushnotification_pushnotification__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_reserved_reserved__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_historial_historial__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_nanysf_nanysf__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_contact_contact__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_politicas_politicas__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_preguntas_preguntas__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__global_services__ = __webpack_require__(181);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










// import { AuthProvider } from '../providers/auth/auth';








var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, menuCtrl, _pushProvider, afAuth, afDB, modalCtrl, oneSignal, imgcacheService) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.menuCtrl = menuCtrl;
        this._pushProvider = _pushProvider;
        this.afAuth = afAuth;
        this.afDB = afDB;
        this.modalCtrl = modalCtrl;
        this.oneSignal = oneSignal;
        this.imgcacheService = imgcacheService;
        this.home = __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */];
        this.perfil = __WEBPACK_IMPORTED_MODULE_8__pages_perfil_perfil__["a" /* PerfilPage */];
        this.reserved = __WEBPACK_IMPORTED_MODULE_10__pages_reserved_reserved__["a" /* ReservedPage */];
        this.login = __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */];
        this.historial = __WEBPACK_IMPORTED_MODULE_12__pages_historial_historial__["a" /* HistorialPage */];
        this.nanysf = __WEBPACK_IMPORTED_MODULE_13__pages_nanysf_nanysf__["a" /* NanysfPage */];
        this.contactanos = __WEBPACK_IMPORTED_MODULE_14__pages_contact_contact__["a" /* ContactPage */];
        this.preguntas = __WEBPACK_IMPORTED_MODULE_16__pages_preguntas_preguntas__["a" /* PreguntasPage */];
        this.usuario = {};
        platform.ready().then(function () {
            _this.rootPage = 'LoginPage';
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            _this._pushProvider.init_notification();
            // this._authProvider.users();
            // Init ImgCache lib
            imgcacheService.initImgCache().then(function () {
                // this.nav.setRoot(this.rootPage);
            });
        });
        this.initializeApp();
    }
    MyApp.prototype.openPage = function (pagina) {
        this.rootPage = pagina;
        this.menuCtrl.close();
    };
    MyApp.prototype.users = function () {
        var _this = this;
        this.uid = this.afAuth.auth.currentUser.uid;
        console.log(this.uid);
        this.afDB.object('users/' + this.uid + '/' + 'meta').valueChanges().subscribe(function (data) {
            _this.usuario = data;
            console.log(_this.usuario);
        });
    };
    MyApp.prototype.signOut = function (pagina) {
        this.afAuth.auth.signOut();
        this.rootPage = pagina;
        this.menuCtrl.close();
    };
    MyApp.prototype.modal_politicas = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_15__pages_politicas_politicas__["a" /* PoliticasPage */]);
        modal.present();
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            if (_this.platform.is('cordova')) {
                // Optional OneSignal code for iOS to prompt users later
                // Set your iOS Settings
                var iosSettings = {};
                iosSettings["kOSSettingsKeyAutoPrompt"] = false; // will not prompt users when start app 1st time
                iosSettings["kOSSettingsKeyInAppLaunchURL"] = false; // false opens safari with Launch URL
                // OneSignal Code start:
                // Enable to debug issues.
                // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
                var notificationOpenedCallback = function (jsonData) {
                    console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
                    if (jsonData.notification.payload.additionalData != null) {
                        console.log("Here we access addtional data");
                        if (jsonData.notification.payload.additionalData.openURL != null) {
                            console.log("Here we access the openURL sent in the notification data");
                        }
                    }
                };
                window["plugins"].OneSignal
                    .startInit('2036276e-adf8-46b2-8040-de0783b2956e', '748451395664')
                    .iOSSettings(iosSettings) // only needed if added Optional OneSignal code for iOS above
                    .inFocusDisplaying(window["plugins"].OneSignal.OSInFocusDisplayOption.Notification)
                    .handleNotificationOpened(notificationOpenedCallback)
                    .endInit();
            }
            else {
                console.log('OneSignal no funciona en Chrome');
            }
        });
    };
    MyApp.prototype.init_notification = function () {
        if (this.platform.is('cordova')) {
            this.oneSignal.startInit('2036276e-adf8-46b2-8040-de0783b2956e', '748451395664');
            this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
            this.oneSignal.handleNotificationReceived().subscribe(function () {
                console.log('Notificación Recibida');
            });
            this.oneSignal.handleNotificationOpened().subscribe(function () {
                // do something when a notification is opened
                console.log('Notificación Abierta');
            });
            this.oneSignal.endInit();
        }
        else {
            console.log('OneSignal no funciona en Chrome');
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('nav'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/app/app.html"*/'<ion-menu [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n  <ion-content class="dark">\n\n    <ion-list no-lines>\n\n      <ion-grid>\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <button ion-item (click)="openPage(home)" class="dark">\n\n              <ion-col col-4>\n\n                <ion-icon name="md-home" md="md-home"></ion-icon>\n\n              </ion-col>\n\n              <ion-col col-8>\n\n                Inicio\n\n              </ion-col>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <button ion-item (click)="openPage(perfil)" class="dark">\n\n              <ion-col col-4>\n\n                <ion-icon name="md-person" md="md-person"></ion-icon>\n\n              </ion-col>\n\n              <ion-col col-8>\n\n                Mi Perfil\n\n              </ion-col>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <button ion-item (click)="openPage(nanysf)" class="dark">\n\n              <ion-col col-4>\n\n                <ion-icon name="heart" md="md-heart"></ion-icon>\n\n              </ion-col>\n\n              <ion-col col-8>\n\n                Mis Nannies\n\n              </ion-col>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <button ion-item (click)="openPage(reserved)" class="dark">\n\n              <ion-col col-4>\n\n                <ion-icon name="md-document" md="md-document"></ion-icon>\n\n              </ion-col>\n\n              <ion-col col-8>\n\n                  Mis Reservaciones\n\n              </ion-col>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <button ion-item (click)="openPage(historial)" class="dark">\n\n              <ion-col col-4>\n\n                <ion-icon name="md-filing" md="md-filing"></ion-icon>\n\n              </ion-col>\n\n              <ion-col col-8>\n\n                Mi Historial\n\n              </ion-col>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <button ion-item (click)="openPage(contactanos)" class="dark">\n\n              <ion-col col-4>\n\n                <ion-icon name="mail" md="md-mail"></ion-icon>\n\n              </ion-col>\n\n              <ion-col col-8>\n\n                Contáctanos\n\n              </ion-col>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <button ion-item (click)="openPage(preguntas)" class="dark">\n\n              <ion-col col-4>\n\n              <ion-icon ios="ios-book" md="md-book"></ion-icon>\n\n              </ion-col>\n\n              <ion-col col-8>\n\n                Preguntas Frecuentes\n\n              </ion-col>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <button ion-item (click)="signOut(login)" class="dark">\n\n              <ion-col col-4>\n\n                <ion-icon name="md-log-out" md="md-log-out"></ion-icon>\n\n              </ion-col>\n\n              <ion-col col-8>\n\n                Cerrar sesión\n\n              </ion-col>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n    </ion-list>\n\n    <ion-footer>\n\n      <ion-toolbar>\n\n        <ion-grid>\n\n          <ion-row>\n\n            <ion-col col-1></ion-col>\n\n            <ion-col col-11>\n\n              <button ion-item (click)="modal_politicas()" class="dark">\n\n                  Políticas de privacidad\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-toolbar>\n\n    </ion-footer>\n\n  </ion-content>\n\n</ion-menu>\n\n\n\n\n\n<ion-nav [root]="rootPage" #content></ion-nav>\n\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_pushnotification_pushnotification__["a" /* PushnotificationProvider */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_17__global_services__["a" /* ImgcacheService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 571:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImgcacheService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_imgcache_js__ = __webpack_require__(572);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_imgcache_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_imgcache_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * This service is charged of provide the methods to cache the images
 */
var ImgcacheService = /** @class */ (function () {
    function ImgcacheService(platform) {
        this.imgQueue = [];
        __WEBPACK_IMPORTED_MODULE_2_imgcache_js___default.a.options.debug = true;
    }
    /**
     * Init imgCache library
     * @return {Promise}
     */
    ImgcacheService.prototype.initImgCache = function () {
        return new Promise(function (resolve, reject) {
            if (__WEBPACK_IMPORTED_MODULE_2_imgcache_js___default.a.ready) {
                resolve();
            }
            else {
                __WEBPACK_IMPORTED_MODULE_2_imgcache_js___default.a.init(function () { return resolve(); }, function () { return reject(); });
            }
        });
    };
    /**
     * Cache images
     * @param src {string} - img source
     */
    ImgcacheService.prototype.cacheImg = function (src) {
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_imgcache_js___default.a.isCached(src, function (path, success) {
                // if not, it will be cached
                if (success) {
                    __WEBPACK_IMPORTED_MODULE_2_imgcache_js___default.a.getCachedFileURL(src, function (originalUrl, cacheUrl) {
                        resolve(cacheUrl);
                    }, function (e) {
                        reject(e);
                    });
                }
                else {
                    // cache img
                    __WEBPACK_IMPORTED_MODULE_2_imgcache_js___default.a.cacheFile(src);
                    // return original img URL
                    resolve(src);
                }
            });
        });
    };
    ImgcacheService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]])
    ], ImgcacheService);
    return ImgcacheService;
}());

//# sourceMappingURL=cache-img.service.js.map

/***/ }),

/***/ 573:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthProvider = /** @class */ (function () {
    function AuthProvider(afAuth, afDB) {
        this.afAuth = afAuth;
        this.afDB = afDB;
        this.uid = {};
        this.usuario = {};
        console.log('Hello AuthProvider Provider');
    }
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 574:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__lazy_img_lazy_img_component__ = __webpack_require__(575);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__lazy_img_lazy_img_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 575:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LazyImgComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Component in charge of lazy load images and cache them
 */
var LazyImgComponent = /** @class */ (function () {
    function LazyImgComponent() {
        this.placeholderActive = true;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], LazyImgComponent.prototype, "inputSrc", void 0);
    LazyImgComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'lazy-img',
            template: "\n  <div text-center [ngClass]=\"{ 'placeholder': placeholderActive }\">\n    <img [inputSrc]=\"inputSrc\" lazy-load (loaded)=\"placeholderActive = false\"/>\n  </div>\n  "
        })
    ], LazyImgComponent);
    return LazyImgComponent;
}());

//# sourceMappingURL=lazy-img.component.js.map

/***/ }),

/***/ 576:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__lazy_load_directive__ = __webpack_require__(577);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__lazy_load_directive__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 577:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LazyLoadDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services___ = __webpack_require__(181);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * This directive is charge of cache the images and emit a loaded event
 */
var LazyLoadDirective = /** @class */ (function () {
    function LazyLoadDirective(el, imgCacheService, renderer) {
        this.el = el;
        this.imgCacheService = imgCacheService;
        this.renderer = renderer;
        this.src = '';
        this.loaded = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    LazyLoadDirective.prototype.ngOnInit = function () {
        var _this = this;
        // get img element
        var nativeElement = this.el.nativeElement;
        var render = this.renderer;
        // add load listener
        this.loadEvent = render.listen(nativeElement, 'load', function () {
            render.addClass(nativeElement, 'loaded');
            _this.loaded.emit();
        });
        this.errorEvent = render.listen(nativeElement, 'error', function () {
            nativeElement.remove();
        });
        // cache img and set the src to the img
        this.imgCacheService.cacheImg(this.src).then(function (value) {
            render.setAttribute(nativeElement, 'src', value);
        });
    };
    LazyLoadDirective.prototype.ngOnDestroy = function () {
        // remove listeners
        this.loadEvent();
        this.errorEvent();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('inputSrc'),
        __metadata("design:type", Object)
    ], LazyLoadDirective.prototype, "src", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", Object)
    ], LazyLoadDirective.prototype, "loaded", void 0);
    LazyLoadDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Directive */])({
            selector: '[lazy-load]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_1__services___["a" /* ImgcacheService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["X" /* Renderer2 */]])
    ], LazyLoadDirective);
    return LazyLoadDirective;
}());

//# sourceMappingURL=lazy-load.directive.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestformPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__request_request__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__reserved_reserved__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { Geolocation } from '@ionic-native/geolocation';


//import { AngularFireObject } from 'angularfire2/database/interfaces';


var RequestformPage = /** @class */ (function () {
    //UserTop:any={};
    function RequestformPage(navCtrl, navParams, afDB, afAuth, fb, loadingCtrl, alertCtrl, 
        // private geolocation: Geolocation,
        modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.fb = fb;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.childrens = {};
        this.data = {};
        this.key = {};
        this.users = {};
        this.nanyId = {};
        this.obj = {};
        this.userGeo = {};
        this.fechaActual = {};
        this.horaActual = {};
        this.price = {};
        this.idPlayer = '';
        this.idPlayerUser = '';
        this.form = '';
        this.usuario = {};
        this.category = {};
        //promocodes
        this.PromoCodes = {};
        this.promoText = "";
        this.total = 0;
        this.nanyId = this.navParams.get("nanyId");
        this.childrens = this.navParams.get("childrens");
        this.price = this.navParams.get("price");
        console.log("precio recibido " + this.price);
        this.category = this.navParams.get("category");
        this.idPlayer = this.navParams.get("idPlayer");
        this.idPlayerUser = this.navParams.get("idPlayerUser");
        console.log(this.idPlayerUser);
        console.log(this.price);
        console.log(this.category);
        console.log(this.idPlayer + 'Nanny');
        console.log(this.idPlayerUser + 'user');
        this.fechaActual = new Date().toJSON().split('T')[0];
        this.horaActual = new Date().getHours();
        this.uid = this.afAuth.auth.currentUser.uid;
        afAuth.authState.subscribe(function (user) {
            if (!user) {
                _this.displayName = null;
            }
            _this.codigo = user.uid;
            console.log(_this.codigo);
        });
        this.discount = 1;
        this.afDB.object('users/' + this.uid + '/' + 'geo').valueChanges().subscribe(function (data) {
            _this.userGeo = data;
            console.log(_this.userGeo);
        });
        /*this.afDB.object('users/'+this.uid).valueChanges().subscribe(data =>{
          this.UserTop = data;
          console.log(this.userGeo);
        });*/
        this.afDB.object('users/' + this.uid + '/meta').valueChanges().subscribe(function (data) {
            _this.usuario = data;
            console.log("numero de horas stand " + _this.usuario.standHours);
        });
        this.afDB.object('users/' + this.uid + '/' + 'orden/').valueChanges().subscribe(function (data) {
            _this.data = data;
            _this.data.cpr = "";
            _this.data.note = "";
            if (!_this.data) {
                _this.data = {};
                _this.data.ciudad = '';
            }
            if (_this.category == 'stand' || _this.category == 'dream') {
                _this.data.timeCuidado = "";
            }
            console.log(_this.data);
        });
        this.data.ciudad = '';
        if (this.childrens == 1 && this.category == 'traditional') {
            this.myForm = this.fb.group({
                myDate: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                myTime: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                timeCuidado: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                nombre1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidop1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidom1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                edad1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                fechan1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                nombre2: ['', []],
                apellidop2: ['', []],
                apellidom2: ['', []],
                edad2: ['', []],
                fechan2: ['', []],
                nombre3: ['', []],
                apellidop3: ['', []],
                apellidom3: ['', []],
                edad3: ['', []],
                fechan3: ['', []],
                note: ['', []],
                ciudad: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                delegacion: ['', []],
                colonia: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                calle: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numEx: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numIn: ['', []],
                cp: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                movil: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                cpr: ['', []]
            });
            console.log('hola1');
        }
        else if (this.childrens == 1) {
            this.myForm = this.fb.group({
                myDate: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                myTime: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                timeCuidado: ['', []],
                nombre1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidop1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidom1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                edad1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                fechan1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                nombre2: ['', []],
                apellidop2: ['', []],
                apellidom2: ['', []],
                edad2: ['', []],
                fechan2: ['', []],
                nombre3: ['', []],
                apellidop3: ['', []],
                apellidom3: ['', []],
                edad3: ['', []],
                fechan3: ['', []],
                note: ['', []],
                ciudad: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                delegacion: ['', []],
                colonia: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                calle: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numEx: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numIn: ['', []],
                cp: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                movil: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                cpr: ['', []]
            });
            console.log('hola12');
        }
        if (this.childrens == 2 && this.category == 'traditional') {
            this.myForm = this.fb.group({
                myDate: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                myTime: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                timeCuidado: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                nombre1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidop1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidom1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                edad1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                fechan1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                nombre2: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidop2: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidom2: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                edad2: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                fechan2: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                nombre3: ['', []],
                apellidop3: ['', []],
                apellidom3: ['', []],
                edad3: ['', []],
                fechan3: ['', []],
                note: ['', []],
                ciudad: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                delegacion: ['', []],
                colonia: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                calle: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numEx: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numIn: ['', []],
                cp: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                movil: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                cpr: ['', []]
            });
            console.log('hola2');
        }
        else if (this.childrens == 2) {
            this.myForm = this.fb.group({
                myDate: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                myTime: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                timeCuidado: ['', []],
                nombre1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidop1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidom1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                edad1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                fechan1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                nombre2: ['', []],
                apellidop2: ['', []],
                apellidom2: ['', []],
                edad2: ['', []],
                fechan2: ['', []],
                nombre3: ['', []],
                apellidop3: ['', []],
                apellidom3: ['', []],
                edad3: ['', []],
                fechan3: ['', []],
                note: ['', []],
                ciudad: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                delegacion: ['', []],
                colonia: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                calle: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numEx: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numIn: ['', []],
                cp: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                movil: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                cpr: ['', []]
            });
            console.log('hola22');
        }
        if (this.childrens == 3 && this.category == 'traditional') {
            this.myForm = this.fb.group({
                myDate: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                myTime: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                timeCuidado: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                nombre1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidop1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidom1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                edad1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                fechan1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                nombre2: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidop2: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidom2: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                edad2: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                fechan2: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                nombre3: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidop3: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidom3: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                edad3: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                fechan3: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                note: ['', []],
                ciudad: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                delegacion: ['', []],
                colonia: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                calle: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numEx: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numIn: ['', []],
                cp: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                movil: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                cpr: ['', []]
            });
            console.log('hola3');
        }
        else if (this.childrens == 3) {
            this.myForm = this.fb.group({
                myDate: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                myTime: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                timeCuidado: ['', []],
                nombre1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidop1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                apellidom1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                edad1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                fechan1: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                nombre2: ['', []],
                apellidop2: ['', []],
                apellidom2: ['', []],
                edad2: ['', []],
                fechan2: ['', []],
                nombre3: ['', []],
                apellidop3: ['', []],
                apellidom3: ['', []],
                edad3: ['', []],
                fechan3: ['', []],
                note: ['', []],
                ciudad: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                delegacion: ['', []],
                colonia: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                calle: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numEx: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                numIn: ['', []],
                cp: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                movil: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
                cpr: ['', []]
            });
            console.log('hola33');
        }
    }
    RequestformPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RequestformPage');
        // this.iniciarUser();
        // this.iniciarGeolocalizacion();
    };
    // iniciarUser(){
    //
    //   this.uid = this.afAuth.auth.currentUser.uid;
    //   this.usuario = this.afDB.object( 'users/' + this.uid + '/geo/' );
    //
    // }
    // iniciarGeolocalizacion(){
    //
    //   this.geolocation.getCurrentPosition().then((resp) => {
    //
    //     // resp.coords.latitude
    //     // resp.coords.longitude
    //     console.log(resp.coords);
    //
    //
    //
    //     let watch = this.geolocation.watchPosition();
    //
    //     watch.subscribe((data) => {
    //       // data can be a set of coordinates, or an error (if an error occurred).
    //       // data.coords.latitude
    //       // data.coords.longitude
    //       console.log('watch: ', data.coords );
    //
    //       this.usuario.update({lat: data.coords.latitude, lng: data.coords.longitude});
    //
    //     });
    //
    //   }).catch((error) => {
    //
    //     console.log('Error getting location', error);
    //
    //   });
    //
    //
    // }
    // count(ev: any){
    //   this.childrens = ev;
    //   console.log(this.childrens);
    // }
    RequestformPage.prototype.showAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Tu Nanny debe ser solicitada mínimo con 3 horas de anticipación',
            buttons: ['Aceptar']
        });
        alert.present();
    };
    RequestformPage.prototype.showAlertC = function () {
        /*let alert = this.alertCtrl.create({
          title: 'Por el momento solo tenemos servicio en: <br> Ciudad de México <br> y <br> Estado de México',
          buttons: ['Aceptar']
        });
        alert.present();*/
    };
    RequestformPage.prototype.onChoseLocation = function (event) {
        console.log(event);
        this.latitud = event.coords.lat;
        this.longitude = event.coords.lng;
    };
    //method to check if promocode is valid
    RequestformPage.prototype.checkPromo = function (children) {
        var _this = this;
        var revised = false;
        this.afDB.object('promoCodes/' + this.data.cpr).valueChanges().subscribe(function (data) {
            _this.PromoCodes = data;
            //if the promo discount does not exist
            if (!_this.PromoCodes) {
                _this.discount = 1;
                _this.promoText = "Tu codigo de promoción es incorrecto, el código es sensible a mayúsculas.";
                //console.log(this.PromoCodes + "  check promo discount " + this.discount);
                //if promo is empty
            }
            else if (_this.data.cpr == "") {
                _this.discount = 1;
                _this.promoText = " ";
                //if promo discount exists
            }
            else {
                //if is nanny recommendation
                if (_this.PromoCodes.discount == 0) {
                    _this.discount = 1;
                    _this.promoText = "Tu codigo se ha aplicado. " + _this.PromoCodes.text;
                    //console.log("the discount is  "+this.discount);
                    //if it really has a discount
                }
                else {
                    //console.log(this.PromoCodes);
                    //if the code is unlimited apply it
                    if (_this.PromoCodes.availability == "unlimited") {
                        _this.discount = (100 - _this.PromoCodes.discount) / 100;
                        _this.promoText = "Tu código se ha aplicado. " + _this.PromoCodes.text;
                        //console.log("/////////Unlimited Promo Code");
                    }
                    else if (_this.PromoCodes.availability == "limited") {
                        //if is limited promoCode and the limit is not 0
                        if (revised == false) {
                            if (_this.PromoCodes.limit > 1) {
                                _this.discount = (100 - _this.PromoCodes.discount) / 100;
                                _this.promoText = "Tu código se ha aplicado. " + _this.PromoCodes.text;
                                //get limit 
                                var limit = _this.PromoCodes.limit;
                                //update limit variable
                                limit--;
                                //update limit 
                                _this.afDB.database.ref('promoCodes/' + _this.data.cpr + '/').update({ limit: limit });
                                revised = true;
                            }
                            else {
                                _this.discount = (100 - _this.PromoCodes.discount) / 100;
                                _this.promoText = "Tu código se ha aplicado. " + _this.PromoCodes.text;
                                //erase promoCode
                                _this.afDB.database.ref('promoCodes/' + _this.data.cpr + '/').remove();
                                revised = true;
                            }
                        }
                        //if the code is once it is needed to check if it was redeemed before
                    }
                    else if (_this.PromoCodes.availability == "once") {
                        //var promosUsed=this.UserTop.PromoCodesUsed;
                        var invalid;
                        /*console.log("length "+promosUsed.length);
                        for (i=0;i<promosUsed.length;i++){
                          //you used the code already
                          if(promosUsed[i]==this.data.cpr){
                            console.log(promosUsed[i]);
                            console.log("el codigo ya fue utilizado");
                            //then this discount has already redeamed and the promoCode.
                            this.discount=1;
                            //Text needs to change to "este código ya ha sido usado anteriormente"
                            this.promoText="Este código ha sido utilizado anteriormente.";
                            invalid=true;
                          }*/
                        console.log("availability " + _this.PromoCodes.availability + "code " + _this.PromoCodes.code);
                        var codeText = _this.PromoCodes.code;
                        _this.afDB.database.ref('users/').child(_this.uid).child("PromoCodesUsed").child(codeText).once("value", function (snapshot) {
                            console.log("snapshot exists? " + snapshot.exists());
                            //console.log("ciclo para ver si existe "+ snapshot.val());
                            if (snapshot.exists()) {
                                invalid = true;
                                console.log("invalid= " + invalid);
                                //console.log("el codigo ya fue utilizado");
                                //then this discount has already redeamed and the promoCode.
                                _this.discount = 1;
                                //Text needs to change to "este código ya ha sido usado anteriormente"
                                _this.promoText = "Este código ha sido utilizado anteriormente.";
                            }
                            else {
                                invalid = false;
                                console.log("invalid= " + invalid + "discount " + _this.PromoCodes.discount);
                                _this.discount = (100 - _this.PromoCodes.discount) / 100;
                                _this.promoText = "Tu código se ha aplicado. " + _this.PromoCodes.text;
                                var codeData = _this.PromoCodes;
                                //register to the user profile
                                _this.afDB.database.ref('users' + '/' + _this.codigo + '/PromoCodesUsed/' + _this.data.cpr + '/').update({ codeData: codeData });
                                //console.log("**************"+ this.UserTop.PromoCodesUsed);  
                            }
                        });
                    }
                }
                /*
                  }else{
                    this.afDB.object('users/'+this.codigo+"/PromoObject").valueChanges().subscribe(data =>{
                      this.CodesUsed = data;
                      
                      //if this code is in user profile used code list
                      if(this.CodesUsed){
                        if(this.CodesUsed.availability=="used" && this.CodesUsed.code==this.data.cpr){
                          console.log("el codigo ya fue utilizado");
                          //then this discount has already redeamed and the promoCode.
                          this.discount=1;
                          //Text needs to change to "este código ya ha sido usado anteriormente"
                          this.promoText="Este código ha sido utilizado anteriormente.";
                        //else the discount is applied and it is registered as used in user profile
                        }else{
                          this.discount=(100-this.PromoCodes.discount)/100;
                          this.promoText="Tu código se ha aplicado. "+this.PromoCodes.text;
                          var data=this.PromoCodes;
                          //var availability = "used";
                          //register to the user profile
                          this.afDB.database.ref('users'+'/'+this.codigo+'/PromoCodesUsed/'+this.data.cpr+'/').update({PromoObject});
                          //this.afDB.database.ref('users'+'/'+this.codigo+'/PromoObject').update({availability});
                        
                        }
                      }else{
                        
                        this.discount=(100-this.PromoCodes.discount)/100;
                        this.promoText="Tu código se ha aplicado. "+this.PromoCodes.text;
                        var PromoObject=this.PromoCodes;
                        var availability = "used";
                        //register to the user profile
                        this.afDB.database.ref('users'+'/'+this.codigo+'/').update({PromoObject});
                        this.afDB.database.ref('users'+'/'+this.codigo+'/PromoObject').update({availability});
                        
                      }
                    });
                  }
                  */
            }
            console.log("descuento" + _this.discount);
            if (_this.category == 'dream' || _this.category == 'stand') {
                _this.total = _this.price * _this.discount;
            }
            else if (_this.category == 'traditional' || _this.category == 'special' /*&& this.data.timeCuidado != '24'*/ || _this.category == 'grand') {
                _this.total = _this.price * _this.data.timeCuidado * _this.discount;
                console.log(_this.total + " precio " + _this.price + "*tiempo de cuidado " + _this.data.timeCuidado + "*descuento " + _this.discount);
            }
            else if (_this.category == 'stand100') {
                //si aun tiene horas su paquete
                if (_this.usuario.standHours - _this.data.timeCuidado < 0) {
                    var horasFaltantes = _this.data.timeCuidado - _this.usuario.standHours;
                    _this.total = _this.price * horasFaltantes * _this.discount;
                }
                else {
                    _this.total = 0;
                }
            }
            console.log("total con descuento= " + _this.total);
            var flag = false;
            var i = 0;
            while (flag == false) {
                console.log(i++ + "total: " + _this.total);
                //if(this.total != 0){
                if (children == 1) {
                    console.log("entra a ejecutar el método");
                    flag = true;
                    _this.addSer();
                }
                else if (children == 2) {
                    console.log("entra a ejecutar el método");
                    flag = true;
                    _this.addServ();
                }
                else if (children == 3) {
                    console.log("entra a ejecutar el método");
                    flag = true;
                    _this.addServi();
                }
                //}
            }
        });
    };
    RequestformPage.prototype.addSer = function () {
        var pagado = false;
        var key = this.afDB.createPushId();
        //console.log(this.idPlayer+'Nanny');
        //console.log(this.idPlayerUser+'User');
        var data;
        var orden;
        if (this.data.genero2 == null) {
            //if (this.category == 'dream' || this.category == 'stand'){
            //  this.total = this.price*this.discount;
            //}else if(this.category == 'traditional' || this.category == 'special' /*&& this.data.timeCuidado != '24'*/ || this.category == 'grand'){
            //this.total = this.price * this.data.timeCuidado*this.discount;
            //          console.log(this.total+" costo ");
            //      }else 
            if (this.category == 'stand100') {
                //si aun tiene horas su paquete 
                if (this.usuario.standHours - this.data.timeCuidado >= 0) {
                    pagado = true;
                    var standHours = this.usuario.standHours - this.data.timeCuidado;
                    this.afDB.database.ref('users/' + this.codigo + '/meta').update({ standHours: standHours });
                    this.total = 0;
                    data = {
                        idUser: this.codigo,
                        idNanny: this.nanyId,
                        idPlayerUser: this.idPlayerUser,
                        idPlayerNanny: this.idPlayer,
                        numChil: this.childrens,
                        tarifa: this.price,
                        category: this.category,
                        costo: this.total,
                        myDate: this.data.myDate,
                        myTime: this.data.myTime,
                        timeCuidado: this.data.timeCuidado,
                        nombre1: this.data.nombre1,
                        apellidop1: this.data.apellidop1,
                        apellidom1: this.data.apellidom1,
                        edad1: this.data.edad1,
                        fechan1: this.data.fechan1,
                        note: this.data.note,
                        ciudad: this.data.ciudad,
                        delegacion: this.data.delegacion,
                        colonia: this.data.colonia,
                        calle: this.data.calle,
                        numEx: this.data.numEx,
                        numIn: this.data.numIn,
                        cp: this.data.cp,
                        movil: this.data.movil,
                        cpr: this.data.cpr,
                        status: 0,
                        pago: 'payment'
                    };
                    orden = {
                        idUser: this.codigo,
                        idNanny: this.nanyId,
                        numChil: this.childrens,
                        costo: this.total,
                        nombre1: this.data.nombre1,
                        apellidop1: this.data.apellidop1,
                        apellidom1: this.data.apellidom1,
                        edad1: this.data.edad1,
                        fechan1: this.data.fechan1,
                        ciudad: this.data.ciudad,
                        delegacion: this.data.delegacion,
                        colonia: this.data.colonia,
                        calle: this.data.calle,
                        numEx: this.data.numEx,
                        numIn: this.data.numIn,
                        cp: this.data.cp,
                        movil: this.data.movil,
                        status: 0
                    };
                }
            }
            //else{
            //          var horasFaltantes=this.data.timeCuidado-this.usuario.standHours;
            //        this.total = this.price * horasFaltantes*this.discount;
            //    }
            //}
            /*else if(this.category == 'special' && this.data.timeCuidado == '24'){
            this.operacion = this.price * this.data.timeCuidado;
            this.total = this.operacion - 2780;
            }else if(this.category == 'grand' && this.data.timeCuidado != '12'){
            this.total = this.price * this.data.timeCuidado;
            }else if(this.category == 'grand' && this.data.timeCuidado == '12'){
            this.total = this.data.timeCuidado * 100;
            }*/
            if (this.category != 'stand100' || this.total > 0) {
                data = {
                    idUser: this.codigo,
                    idNanny: this.nanyId,
                    idPlayerUser: this.idPlayerUser,
                    idPlayerNanny: this.idPlayer,
                    numChil: this.childrens,
                    tarifa: this.price,
                    category: this.category,
                    costo: this.total,
                    myDate: this.data.myDate,
                    myTime: this.data.myTime,
                    timeCuidado: this.data.timeCuidado,
                    nombre1: this.data.nombre1,
                    apellidop1: this.data.apellidop1,
                    apellidom1: this.data.apellidom1,
                    edad1: this.data.edad1,
                    fechan1: this.data.fechan1,
                    note: this.data.note,
                    ciudad: this.data.ciudad,
                    delegacion: this.data.delegacion,
                    colonia: this.data.colonia,
                    calle: this.data.calle,
                    numEx: this.data.numEx,
                    numIn: this.data.numIn,
                    cp: this.data.cp,
                    movil: this.data.movil,
                    cpr: this.data.cpr,
                    status: 0
                };
                orden = {
                    idUser: this.codigo,
                    idNanny: this.nanyId,
                    numChil: this.childrens,
                    costo: this.total,
                    nombre1: this.data.nombre1,
                    apellidop1: this.data.apellidop1,
                    apellidom1: this.data.apellidom1,
                    edad1: this.data.edad1,
                    fechan1: this.data.fechan1,
                    ciudad: this.data.ciudad,
                    delegacion: this.data.delegacion,
                    colonia: this.data.colonia,
                    calle: this.data.calle,
                    numEx: this.data.numEx,
                    numIn: this.data.numIn,
                    cp: this.data.cp,
                    movil: this.data.movil,
                    status: 0
                };
            }
            this.afDB.database.ref('orders' + '/' + this.codigo + '/' + key).set({ data: data });
            this.afDB.database.ref('orders_n' + '/' + this.nanyId + '/' + key).set({ data: data });
            this.afDB.database.ref('users' + '/' + this.codigo + '/').update({ orden: orden });
            this.ServiceSoli();
            if (!pagado) {
                var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__request_request__["a" /* RequestPage */], { 'nanyId': this.nanyId, 'userId': this.codigo, 'key': key, 'standHours': 0, 'cpr': this.data.cpr, 'total': this.total, 'promotext': this.promoText });
                modal.present();
            }
            else {
                var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__reserved_reserved__["a" /* ReservedPage */], { 'nanyId': this.nanyId, 'userId': this.codigo, 'total': this.total, 'promotext': this.promoText });
                modal.present();
            }
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__reserved_reserved__["a" /* ReservedPage */], { 'nanyId': this.nanyId, 'userId': this.codigo, 'total': this.total, 'promotext': this.promoText });
        }
    };
    RequestformPage.prototype.addServ = function () {
        var pagado = false;
        var key = this.afDB.createPushId();
        var data;
        var orden;
        if (this.data.genero3 == null) {
            if (this.category == 'stand100') {
                //si aun tiene horas su paquete
                if (this.usuario.standHours - this.data.timeCuidado >= 0) {
                    pagado = true;
                    var standHours = this.usuario.standHours - this.data.timeCuidado;
                    this.afDB.database.ref('users/' + this.codigo + '/meta').update({ standHours: standHours });
                    this.total = 0;
                    data = {
                        idUser: this.codigo,
                        idNanny: this.nanyId,
                        idPlayerUser: this.idPlayerUser,
                        idPlayerNanny: this.idPlayer,
                        numChil: this.childrens,
                        tarifa: this.price,
                        category: this.category,
                        costo: this.total,
                        myDate: this.data.myDate,
                        myTime: this.data.myTime,
                        timeCuidado: this.data.timeCuidado,
                        nombre1: this.data.nombre1,
                        apellidop1: this.data.apellidop1,
                        apellidom1: this.data.apellidom1,
                        edad1: this.data.edad1,
                        fechan1: this.data.fechan1,
                        note: this.data.note,
                        ciudad: this.data.ciudad,
                        delegacion: this.data.delegacion,
                        colonia: this.data.colonia,
                        calle: this.data.calle,
                        numEx: this.data.numEx,
                        numIn: this.data.numIn,
                        cp: this.data.cp,
                        movil: this.data.movil,
                        cpr: this.data.cpr,
                        status: 0,
                        pago: 'payment'
                    };
                    orden = {
                        idUser: this.codigo,
                        idNanny: this.nanyId,
                        numChil: this.childrens,
                        costo: this.total,
                        nombre1: this.data.nombre1,
                        apellidop1: this.data.apellidop1,
                        apellidom1: this.data.apellidom1,
                        edad1: this.data.edad1,
                        fechan1: this.data.fechan1,
                        ciudad: this.data.ciudad,
                        delegacion: this.data.delegacion,
                        colonia: this.data.colonia,
                        calle: this.data.calle,
                        numEx: this.data.numEx,
                        numIn: this.data.numIn,
                        cp: this.data.cp,
                        movil: this.data.movil,
                        status: 0
                    };
                }
            }
            if (this.category != 'stand100' || this.total > 0) {
                data = {
                    idUser: this.codigo,
                    idNanny: this.nanyId,
                    idPlayerUser: this.idPlayerUser,
                    idPlayerNanny: this.idPlayer,
                    numChil: this.childrens,
                    tarifa: this.price,
                    category: this.category,
                    costo: this.total,
                    myDate: this.data.myDate,
                    myTime: this.data.myTime,
                    timeCuidado: this.data.timeCuidado,
                    nombre1: this.data.nombre1,
                    apellidop1: this.data.apellidop1,
                    apellidom1: this.data.apellidom1,
                    edad1: this.data.edad1,
                    fechan1: this.data.fechan1,
                    nombre2: this.data.nombre2,
                    apellidop2: this.data.apellidop2,
                    apellidom2: this.data.apellidom2,
                    edad2: this.data.edad2,
                    fechan2: this.data.fechan2,
                    note: this.data.note,
                    ciudad: this.data.ciudad,
                    delegacion: this.data.delegacion,
                    colonia: this.data.colonia,
                    calle: this.data.calle,
                    numEx: this.data.numEx,
                    numIn: this.data.numIn,
                    cp: this.data.cp,
                    movil: this.data.movil,
                    cpr: this.data.cpr,
                    status: 0
                };
                orden = {
                    idUser: this.codigo,
                    idNanny: this.nanyId,
                    numChil: this.childrens,
                    costo: this.total,
                    nombre1: this.data.nombre1,
                    apellidop1: this.data.apellidop1,
                    apellidom1: this.data.apellidom1,
                    edad1: this.data.edad1,
                    fechan1: this.data.fechan1,
                    nombre2: this.data.nombre2,
                    apellidop2: this.data.apellidop2,
                    apellidom2: this.data.apellidom2,
                    edad2: this.data.edad2,
                    fechan2: this.data.fechan2,
                    ciudad: this.data.ciudad,
                    delegacion: this.data.delegacion,
                    colonia: this.data.colonia,
                    calle: this.data.calle,
                    numEx: this.data.numEx,
                    numIn: this.data.numIn,
                    cp: this.data.cp,
                    movil: this.data.movil,
                    status: 0
                };
            }
            this.afDB.database.ref('orders' + '/' + this.codigo + '/' + key).set({ data: data });
            this.afDB.database.ref('orders_n' + '/' + this.nanyId + '/' + key).set({ data: data });
            this.afDB.database.ref('users' + '/' + this.codigo + '/').update({ orden: orden });
            this.ServiceSoli();
            if (!pagado) {
                var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__request_request__["a" /* RequestPage */], { 'nanyId': this.nanyId, 'userId': this.codigo, 'key': key, 'standHours': 0, 'cpr': this.data.cpr, 'total': this.total, 'promotext': this.promoText });
                modal.present();
            }
            else {
                var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__reserved_reserved__["a" /* ReservedPage */], { 'nanyId': this.nanyId, 'userId': this.codigo, 'total': this.total, 'promotext': this.promoText });
                modal.present();
            }
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__reserved_reserved__["a" /* ReservedPage */], { 'nanyId': this.nanyId, 'userId': this.codigo, 'total': this.total, 'promotext': this.promoText });
        }
    };
    RequestformPage.prototype.addServi = function () {
        var pagado = false;
        var key = this.afDB.createPushId();
        var data;
        var orden;
        if (this.data.genero4 == null) {
            if (this.category == 'stand100') {
                //si aun tiene horas su paquete
                if (this.usuario.standHours - this.data.timeCuidado >= 0) {
                    pagado = true;
                    var standHours = this.usuario.standHours - this.data.timeCuidado;
                    this.afDB.database.ref('users/' + this.codigo + '/meta').update({ standHours: standHours });
                    this.total = 0;
                    data = {
                        idUser: this.codigo,
                        idNanny: this.nanyId,
                        idPlayerUser: this.idPlayerUser,
                        idPlayerNanny: this.idPlayer,
                        numChil: this.childrens,
                        tarifa: this.price,
                        category: this.category,
                        costo: this.total,
                        myDate: this.data.myDate,
                        myTime: this.data.myTime,
                        timeCuidado: this.data.timeCuidado,
                        nombre1: this.data.nombre1,
                        apellidop1: this.data.apellidop1,
                        apellidom1: this.data.apellidom1,
                        edad1: this.data.edad1,
                        fechan1: this.data.fechan1,
                        note: this.data.note,
                        ciudad: this.data.ciudad,
                        delegacion: this.data.delegacion,
                        colonia: this.data.colonia,
                        calle: this.data.calle,
                        numEx: this.data.numEx,
                        numIn: this.data.numIn,
                        cp: this.data.cp,
                        movil: this.data.movil,
                        cpr: this.data.cpr,
                        status: 0,
                        pago: 'payment'
                    };
                    orden = {
                        idUser: this.codigo,
                        idNanny: this.nanyId,
                        numChil: this.childrens,
                        costo: this.total,
                        nombre1: this.data.nombre1,
                        apellidop1: this.data.apellidop1,
                        apellidom1: this.data.apellidom1,
                        edad1: this.data.edad1,
                        fechan1: this.data.fechan1,
                        ciudad: this.data.ciudad,
                        delegacion: this.data.delegacion,
                        colonia: this.data.colonia,
                        calle: this.data.calle,
                        numEx: this.data.numEx,
                        numIn: this.data.numIn,
                        cp: this.data.cp,
                        movil: this.data.movil,
                        status: 0
                    };
                }
            }
            if (this.category != 'stand100' || this.total > 0) {
                data = {
                    idUser: this.codigo,
                    idNanny: this.nanyId,
                    idPlayerUser: this.idPlayerUser,
                    idPlayerNanny: this.idPlayer,
                    numChil: this.childrens,
                    tarifa: this.price,
                    category: this.category,
                    costo: this.total,
                    myDate: this.data.myDate,
                    myTime: this.data.myTime,
                    timeCuidado: this.data.timeCuidado,
                    nombre1: this.data.nombre1,
                    apellidop1: this.data.apellidop1,
                    apellidom1: this.data.apellidom1,
                    edad1: this.data.edad1,
                    fechan1: this.data.fechan1,
                    nombre2: this.data.nombre2,
                    apellidop2: this.data.apellidop2,
                    apellidom2: this.data.apellidom2,
                    edad2: this.data.edad2,
                    fechan2: this.data.fechan2,
                    nombre3: this.data.nombre3,
                    apellidop3: this.data.apellidop3,
                    apellidom3: this.data.apellidom3,
                    edad3: this.data.edad3,
                    fechan3: this.data.fechan3,
                    note: this.data.note,
                    ciudad: this.data.ciudad,
                    delegacion: this.data.delegacion,
                    colonia: this.data.colonia,
                    calle: this.data.calle,
                    numEx: this.data.numEx,
                    numIn: this.data.numIn,
                    cp: this.data.cp,
                    movil: this.data.movil,
                    cpr: this.data.cpr,
                    status: 0
                };
                orden = {
                    idUser: this.codigo,
                    idNanny: this.nanyId,
                    numChil: this.childrens,
                    costo: this.total,
                    nombre1: this.data.nombre1,
                    apellidop1: this.data.apellidop1,
                    apellidom1: this.data.apellidom1,
                    edad1: this.data.edad1,
                    fechan1: this.data.fechan1,
                    nombre2: this.data.nombre2,
                    apellidop2: this.data.apellidop2,
                    apellidom2: this.data.apellidom2,
                    edad2: this.data.edad2,
                    fechan2: this.data.fechan2,
                    nombre3: this.data.nombre3,
                    apellidop3: this.data.apellidop3,
                    apellidom3: this.data.apellidom3,
                    edad3: this.data.edad3,
                    fechan3: this.data.fechan3,
                    ciudad: this.data.ciudad,
                    delegacion: this.data.delegacion,
                    colonia: this.data.colonia,
                    calle: this.data.calle,
                    numEx: this.data.numEx,
                    numIn: this.data.numIn,
                    cp: this.data.cp,
                    movil: this.data.movil,
                    status: 0
                };
            }
            this.afDB.database.ref('orders' + '/' + this.codigo + '/' + key).set({ data: data });
            this.afDB.database.ref('orders_n' + '/' + this.nanyId + '/' + key).set({ data: data });
            this.afDB.database.ref('users' + '/' + this.codigo + '/').update({ orden: orden });
            this.ServiceSoli();
            if (!pagado) {
                var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__request_request__["a" /* RequestPage */], { 'nanyId': this.nanyId, 'userId': this.codigo, 'key': key, 'standHours': 0, 'cpr': this.data.cpr, 'total': this.total, 'promotext': this.promoText });
                modal.present();
            }
            else {
                var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__reserved_reserved__["a" /* ReservedPage */], { 'nanyId': this.nanyId, 'userId': this.codigo, 'total': this.total, 'promotext': this.promoText });
                modal.present();
            }
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__reserved_reserved__["a" /* ReservedPage */], { 'nanyId': this.nanyId, 'userId': this.codigo, 'total': this.total, 'promotext': this.promoText });
        }
    };
    RequestformPage.prototype.ServiceSoli = function () {
        var _this = this;
        this.afDB.object('users/' + this.uid + '/' + 'meta/').valueChanges().subscribe(function (data) {
            _this.data = data;
            if (!_this.data) {
                _this.data = {};
            }
            var noti = {
                app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                include_player_ids: [_this.idPlayer],
                data: { "foo": "bar" },
                contents: { en: "Nueva solicitud de cuidado de " + _this.data.displayName },
            };
            window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                console.log("Notification Post Success:", successResponse);
            }, function (failedResponse) {
                console.log("Notification Post Failed: ", failedResponse);
                alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
            });
            console.log(_this.data.displayName);
        });
    };
    RequestformPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-requestform',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/requestform/requestform.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>mi cuidado</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12>\n        <div text-uppercase text-center>\n          <b>Completa el siguiente formulario</b>\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col col-12>\n        <ion-list>\n          <form [formGroup]="myForm" novalidate>\n            <ion-item>\n              <ion-label floating color="dark">Fecha del cuidado<span class="color">*</span></ion-label>\n              <ion-datetime displayFormat="DD-MMMM-YYYY" pickerFormat="DD/MMMM/YYYY" [(ngModel)]="data.myDate" name="myDate" formControlName="myDate"\n              monthNames="Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre"\n              monthShortNames="Ene, Feb, Mar, Abr, May, Jun, Jul, Ago, Sep, Oct, Nov, Dic"\n              dayNames="domingo, Lunes, Martes, Miercoles, Juves, Viernes, Sabado, Domingo"\n              dayShortNames="Dom, Lum, Mar, Mie, Jue, Vie, Sab"\n              min={{fechaActual}}\n              max=2020-12-31\n              yearValues="2019,2020"\n              doneText=Aceptar\n              cancelText=Cancelar>\n            </ion-datetime>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Horario<span class="color">*</span></ion-label>\n            \n            \n              <ion-datetime name="myTime" formControlName="myTime"\n              displayFormat="HH:mm"\n              [(ngModel)]="data.myTime"\n              hourValues="7,8,9,10,11,12,13,14,15,16,17,18,19"\n              minuteValues="0,5,10,15,20,25,30,35,40,45,50,55"\n              doneText=Aceptar\n              cancelText=Cancelar\n              (click)="showAlert()"\n              *ngIf="this.category == \'traditional\' || this.category == \'special\' || this.category == \'grand\' || this.category == \'stand100\'"></ion-datetime>\n            \n\n            \n                <ion-datetime name="myTime" formControlName="myTime"\n                displayFormat="HH:mm"\n                [(ngModel)]="data.myTime"\n                hourValues="19,20,21,22,23"\n                minuteValues="0,5,10,15,20,25,30,35,40,45,50,55"\n                doneText=Aceptar\n                cancelText=Cancelar\n                (click)="showAlert()"\n                *ngIf="this.category==\'dream\'"></ion-datetime>\n            \n          \n      </ion-item>\n\n\n\n            <div *ngIf="this.category == \'traditional\' || this.category == \'special\' || this.category == \'grand\' || this.category == \'stand100\'">\n              <ion-item>\n                <ion-label floating color="dark">Duración de tu cuidado. (Horas)<span class="color">*</span></ion-label>\n                <ion-select name="timeCuidado" formControlName="timeCuidado"\n                  [(ngModel)]="data.timeCuidado"\n                  okText=Aceptar\n                  cancelText=Cancelar>\n                  <ion-option value="2">2</ion-option>\n                  <ion-option value="3">3</ion-option>\n                  <ion-option value="4">4</ion-option>\n                  <ion-option value="5">5</ion-option>\n                  <ion-option value="6">6</ion-option>\n                  <ion-option value="7">7</ion-option>\n                  <ion-option value="8">8</ion-option>\n                  <ion-option value="9">9</ion-option>\n                  <ion-option value="10">10</ion-option>\n                  <ion-option value="11">11</ion-option>\n                  <ion-option value="12">12</ion-option>\n                  <!--<div *ngIf="this.category == \'special\'">\n                    <ion-option value="24">24</ion-option>\n                  </div>-->\n                </ion-select>\n              </ion-item>\n            </div>\n            <br><br>\n          <div *ngIf="childrens == 1">\n            <div text-center text-uppercase *ngIf="category!=\'grand\'">\n              <strong>Datos del niño (1)</strong>\n            </div>\n            <div text-center text-uppercase *ngIf="category==\'grand\'">\n              <strong>Datos de la persona (1)</strong>\n            </div>\n            <br>\n            <ion-item>\n              <ion-label floating color="dark">Nombre<span class="color">*</span></ion-label>\n              <ion-input type="text" name="nombre1" formControlName="nombre1"\n              [(ngModel)]="data.nombre1"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'nombre1\').errors && myForm.get(\'nombre1\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'nombre1\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido paterno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidop1" formControlName="apellidop1"\n              [(ngModel)]="data.apellidop1"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidop1\').errors && myForm.get(\'apellidop1\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidop1\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido materno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidom1" formControlName="apellidom1"\n              [(ngModel)]="data.apellidom1"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidom1\').errors && myForm.get(\'apellidom1\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidom1\').hasError(\'required\')"></p>\n            </ion-item>\n            \n              \n              <div *ngIf="category!=\'grand\'">\n                <ion-item>\n                  <ion-label floating color="dark">Edad<span class="color">*</span></ion-label>\n              <ion-select name="edad1" formControlName="edad1"\n                [(ngModel)]="data.edad1"\n                okText=Aceptar\n                cancelText=Cancelar>\n                \n                <ion-option value="1">0-1 año</ion-option>\n                <ion-option value="2">2 años</ion-option>\n                <ion-option value="3">3 años</ion-option>\n                <ion-option value="4">4 años</ion-option>\n                <ion-option value="5">5 años</ion-option>\n                <ion-option value="6">6 años</ion-option>\n                <ion-option value="7">7 años</ion-option>\n                <ion-option value="8">8 años</ion-option>\n                <ion-option value="9">9 años</ion-option>\n                <ion-option value="10">10 años</ion-option>\n                <ion-option value="11">11 años</ion-option>\n                <ion-option value="12">12 años</ion-option>\n                \n              </ion-select>\n            </ion-item>\n            </div>\n          <div *ngIf="category==\'grand\'">\n            <ion-item>\n              <ion-label floating color="dark">Edad<span class="color">*</span></ion-label>\n            <ion-input type="text"  name="edad1" formControlName="edad1"\n            [(ngModel)]="data.edad1" okText=Aceptar cancelText=Cancelar></ion-input>\n          </ion-item>\n          </div>\n\n            <div *ngIf="category!=\'grand\'">\n            <ion-item>\n              <ion-label floating color="dark">Fecha de nacimiento<span class="color">*</span></ion-label>\n              <ion-datetime displayFormat="DD-MMMM-YYYY" pickerFormat="DD/MMMM/YYYY" [(ngModel)]="data.fechan1" name="fechan1" formControlName="fechan1"\n              monthNames="Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre"\n              monthShortNames="Ene, Feb, Mar, Abr, May, Jun, Jul, Ago, Sep, Oct, Nov, Dic"\n              dayNames="domingo, Lunes, Martes, Miercoles, Juves, Viernes, Sabado, Domingo"\n              dayShortNames="Dom, Lum, Mar, Mie, Jue, Vie, Sab"\n              min=2006-01-01\n              doneText=Aceptar\n              cancelText=Cancelar >\n            </ion-datetime>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'fechan1\').errors && myForm.get(\'fechan1\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'fechan1\').hasError(\'required\')"></p>\n            </ion-item>\n            </div>\n            <div *ngIf="category==\'grand\'">\n                <ion-item>\n                  <ion-label floating color="dark">Fecha de nacimiento<span class="color">*</span></ion-label>\n                  <ion-datetime displayFormat="DD-MMMM-YYYY" pickerFormat="DD/MMMM/YYYY" [(ngModel)]="data.fechan1" name="fechan1" formControlName="fechan1"\n                  monthNames="Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre"\n                  monthShortNames="Ene, Feb, Mar, Abr, May, Jun, Jul, Ago, Sep, Oct, Nov, Dic"\n                  dayNames="domingo, Lunes, Martes, Miercoles, Juves, Viernes, Sabado, Domingo"\n                  dayShortNames="Dom, Lum, Mar, Mie, Jue, Vie, Sab"\n                  doneText=Aceptar\n                  cancelText=Cancelar >\n                </ion-datetime>\n                </ion-item>\n                <ion-item *ngIf="myForm.get(\'fechan1\').errors && myForm.get(\'fechan1\').dirty">\n                  <p color="danger" ion-text *ngIf="myForm.get(\'fechan1\').hasError(\'required\')"></p>\n                </ion-item>\n                </div>\n          </div>\n\n          <div *ngIf="childrens == 2">\n            <br><br>\n            <div text-center text-uppercase *ngIf="category!=\'grand\'">\n              <strong>Datos del niño (1)</strong>\n            </div>\n            <div text-center text-uppercase *ngIf="category==\'grand\'">\n              <strong>Datos de la persona (1)</strong>\n            </div>\n            <br>\n            <ion-item>\n              <ion-label floating color="dark">Nombre<span class="color">*</span></ion-label>\n              <ion-input type="text" name="nombre1" formControlName="nombre1"\n              [(ngModel)]="data.nombre1"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'nombre1\').errors && myForm.get(\'nombre1\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'nombre1\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido paterno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidop1" formControlName="apellidop1"\n              [(ngModel)]="data.apellidop1"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidop1\').errors && myForm.get(\'apellidop1\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidop1\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido materno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidom1" formControlName="apellidom1"\n              [(ngModel)]="data.apellidom1"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidom1\').errors && myForm.get(\'apellidom1\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidom2\').hasError(\'required\')"></p>\n            </ion-item>\n            <div *ngIf="category!=\'grand\'">\n                <ion-item>\n                  <ion-label floating color="dark">Edad<span class="color">*</span></ion-label>\n              <ion-select name="edad1" formControlName="edad1"\n                [(ngModel)]="data.edad1"\n                okText=Aceptar\n                cancelText=Cancelar>\n                \n                <ion-option value="1">0-1 año</ion-option>\n                <ion-option value="2">2 años</ion-option>\n                <ion-option value="3">3 años</ion-option>\n                <ion-option value="4">4 años</ion-option>\n                <ion-option value="5">5 años</ion-option>\n                <ion-option value="6">6 años</ion-option>\n                <ion-option value="7">7 años</ion-option>\n                <ion-option value="8">8 años</ion-option>\n                <ion-option value="9">9 años</ion-option>\n                <ion-option value="10">10 años</ion-option>\n                <ion-option value="11">11 años</ion-option>\n                <ion-option value="12">12 años</ion-option>\n                \n              </ion-select>\n            </ion-item>\n            </div>\n          <div *ngIf="category==\'grand\'">\n            <ion-item>\n              <ion-label floating color="dark">Edad<span class="color">*</span></ion-label>\n            <ion-input type="text"  name="edad1" formControlName="edad1"\n            [(ngModel)]="data.edad1" okText=Aceptar cancelText=Cancelar></ion-input>\n          </ion-item>\n          </div>\n          <div *ngIf="category!=\'grand\'">\n              <ion-item>\n                <ion-label floating color="dark">Fecha de nacimiento<span class="color">*</span></ion-label>\n                <ion-datetime displayFormat="DD-MMMM-YYYY" pickerFormat="DD/MMMM/YYYY" [(ngModel)]="data.fechan1" name="fechan1" formControlName="fechan1"\n                monthNames="Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre"\n                monthShortNames="Ene, Feb, Mar, Abr, May, Jun, Jul, Ago, Sep, Oct, Nov, Dic"\n                dayNames="domingo, Lunes, Martes, Miercoles, Juves, Viernes, Sabado, Domingo"\n                dayShortNames="Dom, Lum, Mar, Mie, Jue, Vie, Sab"\n                min=2006-01-01\n                doneText=Aceptar\n                cancelText=Cancelar >\n              </ion-datetime>\n              </ion-item>\n              <ion-item *ngIf="myForm.get(\'fechan1\').errors && myForm.get(\'fechan1\').dirty">\n                <p color="danger" ion-text *ngIf="myForm.get(\'fechan1\').hasError(\'required\')"></p>\n              </ion-item>\n              </div>\n              <div *ngIf="category==\'grand\'">\n                  <ion-item>\n                    <ion-label floating color="dark">Fecha de nacimiento<span class="color">*</span></ion-label>\n                    <ion-datetime displayFormat="DD-MMMM-YYYY" pickerFormat="DD/MMMM/YYYY" [(ngModel)]="data.fechan1" name="fechan1" formControlName="fechan1"\n                    monthNames="Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre"\n                    monthShortNames="Ene, Feb, Mar, Abr, May, Jun, Jul, Ago, Sep, Oct, Nov, Dic"\n                    dayNames="domingo, Lunes, Martes, Miercoles, Juves, Viernes, Sabado, Domingo"\n                    dayShortNames="Dom, Lum, Mar, Mie, Jue, Vie, Sab"\n                    doneText=Aceptar\n                    cancelText=Cancelar >\n                  </ion-datetime>\n                  </ion-item>\n                  <ion-item *ngIf="myForm.get(\'fechan1\').errors && myForm.get(\'fechan1\').dirty">\n                    <p color="danger" ion-text *ngIf="myForm.get(\'fechan1\').hasError(\'required\')"></p>\n                  </ion-item>\n                  </div>\n            <br><br>\n            <div text-center text-uppercase *ngIf="category!=\'grand\'">\n              <strong>Datos del niño (2)</strong>\n            </div>\n            <div text-center text-uppercase *ngIf="category==\'grand\'">\n              <strong>Datos de la persona (2)</strong>\n            </div>\n            <br>\n            <ion-item>\n              <ion-label floating color="dark">Nombre<span class="color">*</span></ion-label>\n              <ion-input type="text" name="nombre2" formControlName="nombre2"\n              [(ngModel)]="data.nombre2"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'nombre2\').errors && myForm.get(\'nombre2\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'nombre2\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido paterno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidop2" formControlName="apellidop2"\n              [(ngModel)]="data.apellidop2"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidop2\').errors && myForm.get(\'apellidop2\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidop2\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido materno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidom2" formControlName="apellidom2"\n              [(ngModel)]="data.apellidom2"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidom2\').errors && myForm.get(\'apellidom2\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidom2\').hasError(\'required\')"></p>\n            </ion-item>\n            <div *ngIf="category!=\'grand\'">\n                <ion-item>\n                  <ion-label floating color="dark">Edad<span class="color">*</span></ion-label>\n              <ion-select name="edad2" formControlName="edad2"\n                [(ngModel)]="data.edad2"\n                okText=Aceptar\n                cancelText=Cancelar>\n                \n                <ion-option value="1">0-1 año</ion-option>\n                <ion-option value="2">2 años</ion-option>\n                <ion-option value="3">3 años</ion-option>\n                <ion-option value="4">4 años</ion-option>\n                <ion-option value="5">5 años</ion-option>\n                <ion-option value="6">6 años</ion-option>\n                <ion-option value="7">7 años</ion-option>\n                <ion-option value="8">8 años</ion-option>\n                <ion-option value="9">9 años</ion-option>\n                <ion-option value="10">10 años</ion-option>\n                <ion-option value="11">11 años</ion-option>\n                <ion-option value="12">12 años</ion-option>\n                \n              </ion-select>\n            </ion-item>\n            </div>\n          <div *ngIf="category==\'grand\'">\n            <ion-item>\n              <ion-label floating color="dark">Edad<span class="color">*</span></ion-label>\n            <ion-input type="text"  name="edad2" formControlName="edad2"\n            [(ngModel)]="data.edad1" okText=Aceptar cancelText=Cancelar></ion-input>\n          </ion-item>\n          </div>\n          <div *ngIf="category!=\'grand\'">\n              <ion-item>\n                <ion-label floating color="dark">Fecha de nacimiento<span class="color">*</span></ion-label>\n                <ion-datetime displayFormat="DD-MMMM-YYYY" pickerFormat="DD/MMMM/YYYY" [(ngModel)]="data.fechan2" name="fechan2" formControlName="fechan2"\n                monthNames="Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre"\n                monthShortNames="Ene, Feb, Mar, Abr, May, Jun, Jul, Ago, Sep, Oct, Nov, Dic"\n                dayNames="domingo, Lunes, Martes, Miercoles, Juves, Viernes, Sabado, Domingo"\n                dayShortNames="Dom, Lum, Mar, Mie, Jue, Vie, Sab"\n                min=2006-01-01\n                doneText=Aceptar\n                cancelText=Cancelar >\n              </ion-datetime>\n              </ion-item>\n              <ion-item *ngIf="myForm.get(\'fechan2\').errors && myForm.get(\'fechan2\').dirty">\n                <p color="danger" ion-text *ngIf="myForm.get(\'fechan2\').hasError(\'required\')"></p>\n              </ion-item>\n              </div>\n              <div *ngIf="category==\'grand\'">\n                  <ion-item>\n                    <ion-label floating color="dark">Fecha de nacimiento<span class="color">*</span></ion-label>\n                    <ion-datetime displayFormat="DD-MMMM-YYYY" pickerFormat="DD/MMMM/YYYY" [(ngModel)]="data.fechan2" name="fechan2" formControlName="fechan2"\n                    monthNames="Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre"\n                    monthShortNames="Ene, Feb, Mar, Abr, May, Jun, Jul, Ago, Sep, Oct, Nov, Dic"\n                    dayNames="domingo, Lunes, Martes, Miercoles, Juves, Viernes, Sabado, Domingo"\n                    dayShortNames="Dom, Lum, Mar, Mie, Jue, Vie, Sab"\n                    doneText=Aceptar\n                    cancelText=Cancelar >\n                  </ion-datetime>\n                  </ion-item>\n                  <ion-item *ngIf="myForm.get(\'fechan2\').errors && myForm.get(\'fechan2\').dirty">\n                    <p color="danger" ion-text *ngIf="myForm.get(\'fechan2\').hasError(\'required\')"></p>\n                  </ion-item>\n                  </div>\n          </div>\n          <div *ngIf="childrens == 3">\n            <br><br>\n            <div text-center text-uppercase>\n              <strong>Datos del niño (1)</strong>\n            </div>\n            <br>\n            <ion-item>\n              <ion-label floating color="dark">Nombre<span class="color">*</span></ion-label>\n              <ion-input type="text" name="nombre1" formControlName="nombre1"\n              [(ngModel)]="data.nombre1"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'nombre1\').errors && myForm.get(\'nombre1\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'nombre1\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido paterno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidop1" formControlName="apellidop1"\n              [(ngModel)]="data.apellidop1"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidop1\').errors && myForm.get(\'apellidop1\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidop1\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido materno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidom1" formControlName="apellidom1"\n              [(ngModel)]="data.apellidom1"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidom1\').errors && myForm.get(\'apellidom1\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidom1\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Edad<span class="color">*</span></ion-label>\n              <ion-select name="edad1" formControlName="edad1"\n                [(ngModel)]="data.edad1"\n                okText=Aceptar\n                cancelText=Cancelar>\n                <ion-option value="1">0-1 año</ion-option>\n                <ion-option value="2">2 años</ion-option>\n                <ion-option value="3">3 años</ion-option>\n                <ion-option value="4">4 años</ion-option>\n                <ion-option value="5">5 años</ion-option>\n                <ion-option value="6">6 años</ion-option>\n                <ion-option value="7">7 años</ion-option>\n                <ion-option value="8">8 años</ion-option>\n                <ion-option value="9">9 años</ion-option>\n                <ion-option value="10">10 años</ion-option>\n                <ion-option value="11">11 años</ion-option>\n                <ion-option value="12">12 años</ion-option>\n              </ion-select>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Fecha de nacimiento<span class="color">*</span></ion-label>\n              <ion-datetime displayFormat="DD-MMMM-YYYY" pickerFormat="DD/MMMM/YYYY" [(ngModel)]="data.fechan1" name="fechan1" formControlName="fechan1"\n              monthNames="Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre"\n              monthShortNames="Ene, Feb, Mar, Abr, May, Jun, Jul, Ago, Sep, Oct, Nov, Dic"\n              dayNames="domingo, Lunes, Martes, Miercoles, Juves, Viernes, Sabado, Domingo"\n              dayShortNames="Dom, Lum, Mar, Mie, Jue, Vie, Sab"\n              min=2006-01-01\n              doneText=Aceptar\n              cancelText=Cancelar >\n            </ion-datetime>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'fechan1\').errors && myForm.get(\'fechan1\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'fechan1\').hasError(\'required\')"></p>\n            </ion-item>\n            <br><br>\n            <div text-center text-uppercase>\n              <strong>Datos del niño (2)</strong>\n            </div>\n            <br>\n            <ion-item>\n              <ion-label floating color="dark">Nombre<span class="color">*</span></ion-label>\n              <ion-input type="text" name="nombre2" formControlName="nombre2"\n              [(ngModel)]="data.nombre2"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'nombre2\').errors && myForm.get(\'nombre2\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'nombre2\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido paterno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidop2" formControlName="apellidop2"\n              [(ngModel)]="data.apellidop2"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidop2\').errors && myForm.get(\'apellidop2\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidop2\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido materno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidom2" formControlName="apellidom2"\n              [(ngModel)]="data.apellidom2"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidom2\').errors && myForm.get(\'apellidom2\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidom2\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Edad<span class="color">*</span></ion-label>\n              <ion-select name="edad2" formControlName="edad2"\n                [(ngModel)]="data.edad2"\n                okText=Aceptar\n                cancelText=Cancelar>\n                <ion-option value="1">0-1 año</ion-option>\n                <ion-option value="2">2 años</ion-option>\n                <ion-option value="3">3 años</ion-option>\n                <ion-option value="4">4 años</ion-option>\n                <ion-option value="5">5 años</ion-option>\n                <ion-option value="6">6 años</ion-option>\n                <ion-option value="7">7 años</ion-option>\n                <ion-option value="8">8 años</ion-option>\n                <ion-option value="9">9 años</ion-option>\n                <ion-option value="10">10 años</ion-option>\n                <ion-option value="11">11 años</ion-option>\n                <ion-option value="12">12 años</ion-option>\n              </ion-select>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Fecha de nacimiento<span class="color">*</span></ion-label>\n              <ion-datetime displayFormat="DD-MMMM-YYYY" pickerFormat="DD/MMMM/YYYY" [(ngModel)]="data.fechan2" name="fechan2" formControlName="fechan2"\n              monthNames="Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre"\n              monthShortNames="Ene, Feb, Mar, Abr, May, Jun, Jul, Ago, Sep, Oct, Nov, Dic"\n              dayNames="domingo, Lunes, Martes, Miercoles, Juves, Viernes, Sabado, Domingo"\n              dayShortNames="Dom, Lum, Mar, Mie, Jue, Vie, Sab"\n              min=2006-01-01\n              doneText=Aceptar\n              cancelText=Cancelar >\n            </ion-datetime>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'fechan2\').errors && myForm.get(\'fechan2\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'fechan2\').hasError(\'required\')"></p>\n            </ion-item>\n            <br><br>\n            <div text-center text-uppercase>\n              <strong>Datos del niño (3)</strong>\n            </div>\n            <br>\n            <ion-item>\n              <ion-label floating color="dark">Nombre<span class="color">*</span></ion-label>\n              <ion-input type="text" name="nombre3" formControlName="nombre3"\n              [(ngModel)]="data.nombre3"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'nombre3\').errors && myForm.get(\'nombre3\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'nombre3\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido paterno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidop3" formControlName="apellidop3"\n              [(ngModel)]="data.apellidop3"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidop3\').errors && myForm.get(\'apellidop3\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidop3\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Apellido materno<span class="color">*</span></ion-label>\n              <ion-input type="text" name="apellidom3" formControlName="apellidom3"\n              [(ngModel)]="data.apellidom3"></ion-input>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'apellidom3\').errors && myForm.get(\'apellidom3\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'apellidom3\').hasError(\'required\')"></p>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Edad<span class="color">*</span></ion-label>\n              <ion-select name="edad3" formControlName="edad3"\n                [(ngModel)]="data.edad3"\n                okText=Aceptar\n                cancelText=Cancelar>\n                <ion-option value="1">0-1 año</ion-option>\n                <ion-option value="2">2 años</ion-option>\n                <ion-option value="3">3 años</ion-option>\n                <ion-option value="4">4 años</ion-option>\n                <ion-option value="5">5 años</ion-option>\n                <ion-option value="6">6 años</ion-option>\n                <ion-option value="7">7 años</ion-option>\n                <ion-option value="8">8 años</ion-option>\n                <ion-option value="9">9 años</ion-option>\n                <ion-option value="10">10 años</ion-option>\n                <ion-option value="11">11 años</ion-option>\n                <ion-option value="12">12 años</ion-option>\n              </ion-select>\n            </ion-item>\n            <ion-item>\n              <ion-label floating color="dark">Fecha de nacimiento<span class="color">*</span></ion-label>\n              <ion-datetime displayFormat="DD-MMMM-YYYY" pickerFormat="DD/MMMM/YYYY" [(ngModel)]="data.fechan3" name="fechan3" formControlName="fechan3"\n              monthNames="Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre"\n              monthShortNames="Ene, Feb, Mar, Abr, May, Jun, Jul, Ago, Sep, Oct, Nov, Dic"\n              dayNames="domingo, Lunes, Martes, Miercoles, Juves, Viernes, Sabado, Domingo"\n              dayShortNames="Dom, Lum, Mar, Mie, Jue, Vie, Sab"\n              min=2006-01-01\n              doneText=Aceptar\n              cancelText=Cancelar >\n            </ion-datetime>\n            </ion-item>\n            <ion-item *ngIf="myForm.get(\'fechan3\').errors && myForm.get(\'fechan3\').dirty">\n              <p color="danger" ion-text *ngIf="myForm.get(\'fechan3\').hasError(\'required\')"></p>\n            </ion-item>\n          </div>\n          <ion-item>\n            <ion-label floating color="dark">Información adicional</ion-label>\n            <ion-textarea name="note" formControlName="note"\n            [(ngModel)]="data.note" name="note" autocomplete="on" autocorrect="on" rows="5" cols="50"></ion-textarea>\n          </ion-item>\n          <br><br>\n          <div text-center text-uppercase>\n            <strong>Domicilio del cuidado</strong>\n          </div>\n          <br>\n          <ion-item>\n            <ion-label floating color="dark">Ciudad<span class="color">*</span></ion-label>\n            <ion-select name="ciudad" formControlName="ciudad"\n              [(ngModel)]="data.ciudad"\n              okText=Aceptar\n              cancelText=Cancelar\n              (click)="showAlertC()">\n              <ion-option value="Ciudad de México">Ciudad de México</ion-option>\n              <ion-option value="Estado de México">Estado de México</ion-option>\n            </ion-select>\n          </ion-item>\n          <ion-item>\n            <ion-label floating color="dark">Delegación</ion-label>\n            <ion-input type="text" name="delegacion" formControlName="delegacion"\n            [(ngModel)]="data.delegacion"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating color="dark">Colonia<span class="color">*</span></ion-label>\n            <ion-input type="text" name="colonia" formControlName="colonia"\n            [(ngModel)]="data.colonia"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating color="dark">Calle<span class="color">*</span></ion-label>\n            <ion-input type="text" name="calle" formControlName="calle"\n            [(ngModel)]="data.calle"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating color="dark">Número exterior<span class="color">*</span></ion-label>\n            <ion-input type="text" name="numEx" formControlName="numEx"\n            [(ngModel)]="data.numEx"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating color="dark">Número interior</ion-label>\n            <ion-input type="text" name="numIn" formControlName="numIn"\n            [(ngModel)]="data.numIn"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating color="dark">Código postal<span class="color">*</span></ion-label>\n            <ion-input type="number" name="cp" formControlName="cp"\n            [(ngModel)]="data.cp"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating color="dark">Telefono celular<span class="color">*</span></ion-label>\n            <ion-input type="number" name="movil" formControlName="movil"\n            [(ngModel)]="data.movil"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating color="dark">Código Promo</ion-label>\n            <ion-input type="text" name="cpr" formControlName="cpr"\n            [(ngModel)]="data.cpr"></ion-input>\n          </ion-item>\n\n          <!-- Inicia Mapa -->\n            <!-- <ion-item>\n              <ion-label floating color="dark">Dirección donde sera el cuidado<span class="color">*</span></ion-label>\n              <ion-input type="text" [(ngModel)]="data.dire"  name="dire" formControlName="dire"></ion-input>\n            </ion-item>\n            <br>\n            <div text-center>\n              <strong>Tu ubicacion actual</strong>\n            </div>\n            <agm-map [latitude]="userGeo.lat" [longitude]="userGeo.lng" [zoom]="16" (mapClick)="onChoseLocation($event)">\n              <agm-marker [latitude]="userGeo.lat" [longitude]="userGeo.lng"></agm-marker>\n            </agm-map> -->\n            <!-- Termina Mapa -->\n\n            <div padding  *ngIf="childrens == 1">\n              <button ion-button block type="button" color="dark" (click)="checkPromo(1)" [disabled]="!myForm.valid">Guardar</button>\n            </div>\n            <div padding  *ngIf="childrens == 2">\n              <button ion-button block type="button" color="dark" (click)="checkPromo(2)" [disabled]="!myForm.valid">Guardar</button>\n            </div>\n            <div padding  *ngIf="childrens == 3">\n              <button ion-button block type="button" color="dark" (click)="checkPromo(3)" [disabled]="!myForm.valid">Guardar</button>\n            </div>\n            <div padding  *ngIf="childrens == 4">\n              <button ion-button block type="button" color="dark" (click)="checkPromo(4)" [disabled]="!myForm.valid">Guardar</button>\n            </div>\n          </form>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/requestform/requestform.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], RequestformPage);
    return RequestformPage;
}());

//# sourceMappingURL=requestform.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_paypal__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__reserved_reserved__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { RequestformPage } from '../requestform/requestform';




var RequestPage = /** @class */ (function () {
    function RequestPage(navCtrl, navParams, afDB, afAuth, alertCtrl, loadingCtrl, payPal, iab, viewCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.payPal = payPal;
        this.iab = iab;
        this.viewCtrl = viewCtrl;
        this.nannyP = {};
        this.nanyId = {};
        this.userId = {};
        this.ordersU = {};
        this.key = {};
        this.CodigoPromo = {};
        this.standHours = {};
        this.PromoCodes = {};
        this.promoText = this.navParams.get("promotext");
        this.cpr = this.navParams.get("cpr");
        this.standHours = this.navParams.get("standHours");
        this.nanyId = this.navParams.get("nanyId");
        this.userId = this.navParams.get("userId");
        console.log(this.userId);
        this.key = this.navParams.get("key");
        console.log(this.key);
        this.dosBasic = '2 Hrs Gratis BASIC'; //360
        this.dosHrsBasic = '2 horas BASIC'; //360
        this.dosLuxe = '2 horas LUXE'; //560
        this.tresLuxe = '3 horas LUXE'; //840
        this.tresBasic = '3 horas BASIC'; //540
        this.unaLuxe = '1 hora LUXE'; //280
        this.unaBasic = '1 hora BASIC'; //180
        this.afDB.object('products_meta/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nannyP = data;
            console.log(_this.nannyP);
        });
        this.afDB.object('promoCodes/' + this.cpr).valueChanges().subscribe(function (data) {
            _this.PromoCodes = data;
            if (_this.PromoCodes != null || _this.PromoCodes != undefined) {
                console.log("////////////////////////////////////////" + _this.PromoCodes.discount + " ha entrado");
            }
            else {
                console.log("/*********/******/*****" + _this.PromoCodes);
            }
        });
        this.afDB.object('orders/' + this.userId + '/' + this.key + '/data').valueChanges().subscribe(function (data) {
            _this.ordersU = data;
            console.log(_this.ordersU);
        });
        this.afDB.object('users/' + this.userId + '/' + 'codigo/').valueChanges().subscribe(function (data) {
            _this.CodigoPromo = data;
            if (!_this.CodigoPromo) {
                _this.CodigoPromo = {};
            }
            console.log(_this.CodigoPromo);
        });
    }
    RequestPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RequestPage');
    };
    RequestPage.prototype.close = function (home) {
        this.viewCtrl.dismiss();
    };
    RequestPage.prototype.pago = function (forma_pago, costo, home, descuento) {
        console.log(forma_pago);
        console.log(home);
        console.log(costo.toString());
        /*let discount=(100-this.PromoCodes.discount)/100;
          let discountedPrice=costo*discount;
          console.log("precio con descuento "+ discount + " = "+discountedPrice);
        if(this.PromoCodes != null || this.PromoCodes != undefined){
          
          this.tarifa=discountedPrice;
          //update cost
        }*/
        //if(this.unaLuxe == descuento){
        //  this.tarifa = costo - 280;
        //  console.log(this.tarifa);
        //}else if(this.unaBasic == descuento){
        //  this.tarifa = costo - 180;
        //  console.log(this.tarifa);
        //}else if(this.dosLuxe == descuento){
        //this.tarifa = costo - 560;
        //console.log(this.tarifa);
        //}else if(this.dosBasic == descuento|| this.dosHrsBasic == descuento){
        //  this.tarifa = costo - 360;
        //  console.log(this.tarifa);
        //}else if(this.tresLuxe == descuento){
        //  this.tarifa = costo - 840;
        //  console.log(this.tarifa);
        //}else if(this.tresBasic == descuento){
        //  this.tarifa = costo - 540;
        //  console.log(this.tarifa);
        //}
        //else{
        this.tarifa = costo;
        console.log(this.tarifa);
        //}
    };
    RequestPage.prototype.pay = function (forma_pago, costo, home, descuento) {
        var _this = this;
        console.log(forma_pago);
        console.log(home);
        console.log(costo.toString());
        /*
        let discount=(100-this.PromoCodes.discount)/100;
        let discountedPrice=costo*discount;
        console.log("precio con descuento "+ discount + " = "+discountedPrice);
      if(this.PromoCodes != null || this.PromoCodes != undefined){
        
        this.tarifa=discountedPrice;
      }else{
    
          this.tarifa = costo;
          console.log(this.tarifa);
    
        }*/
        this.tarifa = costo;
        console.log(this.tarifa);
        this.payPal.init({
            PayPalEnvironmentProduction: 'AVhw-GRoXY-7xXsyNXbwLj9PDrSfnISAQp7JgwAx7EYjv-LXzUYxsMjLdLvwUbwKNYpjrkhUv0BLxpl0',
            PayPalEnvironmentSandbox: 'ATdR1VentY1ZMhRaDvBXVRdVyU8jQf-YLusfzO1oNzgFuAZcGRsdJKij1Al-CU8l_E7-GGVg1mIaLvw4'
        }).then(function () {
            // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
            _this.payPal.prepareToRender('PayPalEnvironmentProduction', new __WEBPACK_IMPORTED_MODULE_1__ionic_native_paypal__["b" /* PayPalConfiguration */]({})).then(function () {
                var payment = new __WEBPACK_IMPORTED_MODULE_1__ionic_native_paypal__["c" /* PayPalPayment */](_this.tarifa.toString(), 'MXN', "Nanny: " + _this.nannyP.nickname, 'sale');
                payment.shortDescription = "Nanny: " + _this.nannyP.nickname + " fecha: " + _this.ordersU.myDate + " hora: " + _this.ordersU.myTime;
                _this.payPal.renderSinglePaymentUI(payment).then(function (data) {
                    // Successfully paid
                    console.log(JSON.stringify(data));
                    // Example sandbox response
                    //
                    // {
                    //   "client": {
                    //     "environment": "sandbox",
                    //     "product_name": "PayPal iOS SDK",
                    //     "paypal_sdk_version": "2.16.0",
                    //     "platform": "iOS"
                    //   },
                    //   "response_type": "payment",
                    //   "response": {
                    //     "id": "PAY-1AB23456CD789012EF34GHIJ",
                    //     "state": "approved",
                    //     "create_time": "2016-10-03T13:33:33Z",
                    //     "intent": "sale"
                    //   }
                    // }
                    //let status=3;
                    var standNanny = _this.nanyId;
                    var standHours = _this.standHours;
                    _this.afDB.database.ref('users/' + _this.userId + '/meta').update({ standNanny: standNanny });
                    _this.afDB.database.ref('users/' + _this.userId + '/meta').update({ standHours: standHours });
                    //this.afDB.database.ref('orders'+'/'+this.userId+'/'+this.key+'/data').update({status});
                    var pago = 'payment';
                    var codigo = false;
                    _this.afDB.database.ref('orders' + '/' + _this.userId + '/' + _this.key + '/data/').update({ pago: pago });
                    _this.afDB.database.ref('orders_n' + '/' + _this.nanyId + '/' + _this.key + '/data/').update({ pago: pago });
                    _this.afDB.database.ref('users' + '/' + _this.userId + '/codigo/').update({ text: codigo });
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__reserved_reserved__["a" /* ReservedPage */], { 'nanyId': _this.nanyId, 'userId': _this.userId, 'home': home });
                }, function () {
                    // Error or render dialog closed without being successful
                });
            }, function () {
                // Error in configuration
            });
        }, function () {
            // Error in initialization, maybe PayPal isn't supported or something else
        });
    };
    RequestPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-request',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/request/request.html"*/'<ion-header>\n\n  <ion-navbar hideBackButton>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Pago</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card>\n    <ion-card-header>\n        <ion-title>\n          <div text-wrap>\n            {{nannyP.title}}\n          </div>\n        </ion-title>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-4></ion-col>\n          <ion-col col-8>\n            <div text-right>\n              <h5><b style="text-transform: uppercase;">Tarifa: $ {{ordersU.costo}}</b></h5>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12>\n        <button ion-button full color="dark" (click)="pay(\'paypal\', ordersU.costo, \'home\', CodigoPromo.text)">\n          Pagar\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <div>\n    <div text-center>\n      {{this.promoText}}\n\n    </div>\n  </div>\n  \n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/request/request.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_paypal__["a" /* PayPal */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* ViewController */]])
    ], RequestPage);
    return RequestPage;
}());

//# sourceMappingURL=request.js.map

/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__id_id__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__comprobante_comprobante__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ModalPage = /** @class */ (function () {
    function ModalPage(navCtrl, navParams, viewCtrl, afDB, afAuth, modalCtrl, callNumber) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.modalCtrl = modalCtrl;
        this.callNumber = callNumber;
        this.usuario = {};
        this.nanyId = {};
        this.key = {};
        this.ordersU = {};
        this.nannyP = {};
        this.nanyImage = {};
        this.ordersN = {};
        this.user = {};
        this.info = {};
        this.nanyId = this.navParams.get("nanyId");
        console.log(this.nanyId);
        this.userId = this.navParams.get("userId");
        console.log(this.userId);
        this.key = this.navParams.get("key");
        console.log(this.key);
        this.uid = this.afAuth.auth.currentUser.uid;
        this.afDB.object('orders/' + this.userId + '/' + this.key + '/data').valueChanges().subscribe(function (data) {
            _this.ordersU = data;
            console.log(_this.ordersU);
        });
        this.afDB.object('orders_n/' + this.nanyId + '/' + this.key + '/data').valueChanges().subscribe(function (data) {
            _this.ordersN = data;
            console.log(_this.ordersN.numChil);
            _this.afDB.object('users/' + _this.ordersN.idUser + '/' + 'meta').valueChanges().subscribe(function (data) {
                _this.user = data;
                console.log(_this.user);
            });
        });
        this.afDB.object('products_meta/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nannyP = data;
            console.log(_this.nannyP);
        });
        this.afDB.object('products_images/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nanyImage = data;
            console.log(_this.nanyImage);
        });
        this.afDB.object('users/' + this.uid + '/' + 'meta').valueChanges().subscribe(function (data) {
            _this.usuario = data;
            console.log(_this.usuario);
        });
        this.afDB.object('users/' + this.uid + '/' + 'info').valueChanges().subscribe(function (data) {
            _this.info = data;
            console.log(_this.usuario);
        });
    }
    ModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModalPage');
    };
    ModalPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    ModalPage.prototype.modalId = function (userId) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__id_id__["a" /* IdPage */], { 'userId': userId });
        modal.present();
    };
    ModalPage.prototype.modalComprobante = function (userId) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__comprobante_comprobante__["a" /* ComprobantePage */], { 'userId': userId });
        modal.present();
    };
    ModalPage.prototype.call = function (telephoneNumber) {
        this.callNumber.callNumber(telephoneNumber, true);
        this.callNumber.isCallSupported()
            .then(function (response) {
            if (response == true) {
                // do something
            }
            else {
                // do something else
            }
        });
    };
    ModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-modal',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/modal/modal.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>detalles</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div *ngIf="usuario.user == 0">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-6>\n          <img src="{{nanyImage.screenshot1}}" style="width:90%;border-radius:50%;">\n        </ion-col>\n        <ion-col col-6>\n          <div text-center>\n            <br>\n            <h6>{{nannyP.title}}</h6>\n            <h3 style="color:#ac64b0">{{nannyP.nickname}}</h3>\n            <h6> <small>{{nannyP.certificado}}</small> </h6>\n          </div>\n        </ion-col>\n      </ion-row>\n      <br><br>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Fecha del cuidado:</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal"> {{ ordersU.myDate | date: \'dd-MM-yyyy\' }}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Horario:</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersU.myTime}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Duración:</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersU.timeCuidado}} horas</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Niños a cuidar:</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersU.numChil}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-12>\n          <div text-center text-uppercase>\n            <h3>Datos de los niños</h3>\n          </div>\n        </ion-col>\n      </ion-row>\n      <div *ngIf="ordersU.numChil == 1">\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.nombre1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidop1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidom1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.edad1}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.fechan1 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n      </div>\n      <div *ngIf="ordersU.numChil == 2">\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.nombre1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidop1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidom1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.edad1}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.fechan1 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.nombre2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidop2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidom2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.edad2}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.fechan2 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n      </div>\n\n      <div *ngIf="ordersU.numChil == 3">\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.nombre1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidop1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidom1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.edad1}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.fechan1 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.nombre2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidop2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidom2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.edad2}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.fechan2 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.nombre3}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidop3}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.apellidom3}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.edad3}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersU.fechan3 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n      </div>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Información adicional</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersU.note}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-12>\n          <div text-center text-uppercase>\n            <h3>Domicilio del cuidado</h3>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Ciudad</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersU.ciudad}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Delegación</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersU.delegacion}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Colonia</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersU.colonia}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Calle</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersU.calle}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Número exterior</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersU.numEx}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Número interior</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersU.numIn}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Código postal</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersU.cp}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Total</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6><b>${{ordersU.costo}}</b></h6>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div *ngIf="usuario.user == 1">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-2></ion-col>\n        <ion-col col-8>\n          <div text-center>\n            <img src="{{user.foto}}?type=large&width=720&height=720" style="width:70%;border-radius:50%;">\n          </div>\n        </ion-col>\n        <ion-col col-2></ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col col-12>\n            <div text-center>\n              <h6>{{user.displayName}}</h6>\n            </div>\n          </ion-col>\n      </ion-row>\n      <br><br>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Fecha del cuidado:</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal"> {{ ordersN.myDate | date: \'dd-MM-yyyy\' }}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Horario:</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersN.myTime}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Duración:</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersN.timeCuidado}} horas</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Niños a cuidar:</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersN.numChil}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-12>\n          <div text-center text-uppercase>\n            <h3>Datos de los niños</h3>\n          </div>\n        </ion-col>\n      </ion-row>\n      <div *ngIf="ordersN.numChil == 1">\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.nombre1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidop1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidom1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.edad1}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.fechan1 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n      </div>\n      <div *ngIf="ordersN.numChil == 2">\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.nombre1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidop1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidom1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.edad1}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.fechan1 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.nombre2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidop2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidom2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.edad2}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.fechan2 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n      </div>\n\n      <div *ngIf="ordersN.numChil == 3">\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.nombre1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidop1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidom1}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.edad1}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.fechan1 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.nombre2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidop2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidom2}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.edad2}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.fechan2 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-6>\n            <h6>Nombre</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.nombre3}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido paterno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidop3}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Apellido materno</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.apellidom3}}</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Edad</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.edad3}} años</h6>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <h6>Fecha de nacimiento</h6>\n          </ion-col>\n          <ion-col col-6>\n            <h6 style="font-weight:normal">{{ordersN.fechan3 | date: \'dd-MM-yyyy\' }}</h6>\n          </ion-col>\n        </ion-row>\n      </div>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Información adicional</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersN.note}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-12>\n          <div text-center text-uppercase>\n            <h3>Domicilio del cuidado</h3>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Ciudad</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersN.ciudad}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Delegación</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersN.delegacion}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Colonia</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersN.colonia}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Calle</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersN.calle}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Número exterior</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersN.numEx}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Número interior</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersN.numIn}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Código postal</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6 style="font-weight:normal">{{ordersN.cp}}</h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 class="movil">\n          <h6>Teléfono celular</h6>\n        </ion-col>\n        <ion-col col-6>\n          <button ion-button small color="dark" (click)="call(ordersN.movil)">Contactar</button>\n        </ion-col>\n      </ion-row>\n      <br>\n      <ion-row>\n        <ion-col col-6>\n          <h6>Total</h6>\n        </ion-col>\n        <ion-col col-6>\n          <h6><b>${{ordersN.costo}}</b></h6>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n          <button ion-button block color="dark" (click)="modalId(userId)">\n            <ion-icon class="ion-icon" name="contact" md="md-contact"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-6>\n          <button ion-button block color="dark" (click)="modalComprobante(userId)">\n            <ion-icon class="ion-icon" name="paper" md="md-paper"></ion-icon>\n          </button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/modal/modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number__["a" /* CallNumber */]])
    ], ModalPage);
    return ModalPage;
}());

//# sourceMappingURL=modal.js.map

/***/ }),

/***/ 76:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartservicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__chat_chat__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modal_modal__ = __webpack_require__(75);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { HomePage } from '../home/home';
// import { RatingPage } from '../rating/rating';

var StartservicePage = /** @class */ (function () {
    function StartservicePage(navCtrl, navParams, afDB, afAuth, callNumber, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.callNumber = callNumber;
        this.modalCtrl = modalCtrl;
        this.usuario = {};
        this.nanyId = {};
        this.key = {};
        this.ordersU = {};
        this.nannyP = {};
        this.nanyImage = {};
        this.ordersN = {};
        this.data = {};
        this.nanyId = this.navParams.get("nanyId");
        console.log(this.nanyId);
        this.userId = this.navParams.get("userId");
        console.log(this.userId);
        this.key = this.navParams.get("key");
        console.log(this.key);
        this.user = this.navParams.get("user");
        console.log(this.user);
        // this.timeCuidado = this.navParams.get("timeCuidado");
        // console.log(this.timeCuidado+'holsa');
        this.afDB.object('orders/' + this.userId + '/' + this.key + '/data/').valueChanges().subscribe(function (data) {
            _this.ordersU = data;
            console.log(_this.ordersU);
        });
        this.afDB.object('orders_n/' + this.nanyId + '/' + this.key + '/data/').valueChanges().subscribe(function (data) {
            _this.ordersN = data;
            console.log(_this.ordersN.movil);
            _this.afDB.object('users/' + _this.ordersN.idUser + '/' + 'meta/').valueChanges().subscribe(function (data) {
                _this.usuario = data;
                console.log(_this.usuario);
            });
        });
        this.afDB.object('users/' + this.userId + '/' + 'meta').valueChanges().subscribe(function (data) {
            _this.usuarioType = data;
            _this.type = _this.usuarioType.user;
            console.log(_this.usuarioType.user);
        });
        this.afDB.object('products_meta/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nannyP = data;
            console.log(_this.nannyP);
        });
        this.afDB.object('products_images/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.nanyImage = data;
            console.log(_this.nanyImage);
        });
    }
    StartservicePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad StartservicePage');
    };
    StartservicePage.prototype.call = function (telephoneNumber) {
        this.callNumber.callNumber(telephoneNumber, true);
        this.callNumber.isCallSupported()
            .then(function (response) {
            if (response == true) {
                // do something
            }
            else {
                // do something else
            }
        });
    };
    StartservicePage.prototype.goToChat = function (name) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__chat_chat__["a" /* ChatPage */], { 'key': this.key, 'name': name, 'nanyId': this.nanyId, 'user': this.user });
    };
    StartservicePage.prototype.modal_detallesU = function (nanyId, userId, key) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__modal_modal__["a" /* ModalPage */], { 'nanyId': nanyId, 'userId': userId, 'key': key });
        modal.present();
    };
    StartservicePage.prototype.endService = function (key, userId, idPlayerUser) {
        var status = 3;
        this.afDB.database.ref('orders' + '/' + userId + '/' + key + '/data/').update({ status: status });
        this.afDB.database.ref('orders_n' + '/' + this.nanyId + '/' + key + '/data/').update({ status: status });
        this.afDB.database.ref('users' + '/' + userId + '/' + 'meta/').update({ status: 5 });
        this.afDB.database.ref('users' + '/' + this.userId + '/' + 'meta/').update({ status: 0 });
        this.endServiceNoti(idPlayerUser);
    };
    StartservicePage.prototype.endServiceNoti = function (idPlayerUser) {
        var _this = this;
        console.log('Este es el Plyer id' + idPlayerUser);
        this.afDB.object('products_meta/' + this.nanyId).valueChanges().subscribe(function (data) {
            _this.data = data;
            if (!_this.data) {
                _this.data = {};
            }
            var noti = {
                app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                include_player_ids: [idPlayerUser],
                data: { "foo": "bar" },
                contents: { en: "Califica tu cuidado con " + _this.data.title },
                headings: { en: "¡Gracias!" }
            };
            window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                console.log("Notification Post Success:", successResponse);
            }, function (failedResponse) {
                console.log("Notification Post Failed: ", failedResponse);
                alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
            });
        });
    };
    StartservicePage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    StartservicePage.prototype.initTimer = function () {
        var _this = this;
        this.afDB.object('orders_n/' + this.nanyId + '/' + this.key + '/data/').valueChanges().subscribe(function (data) {
            _this.ordersN = data;
            _this.timeCuidado = _this.ordersN.timeCuidado;
            console.log(_this.ordersN.numChil);
            var tiempo = _this.timeCuidado * 3600;
            if (!_this.timeInSeconds) {
                _this.timeInSeconds = tiempo;
            }
            _this.timer = {
                time: _this.timeInSeconds,
                runTimer: false,
                hasStarted: false,
                hasFinished: false,
                timeRemaining: _this.timeInSeconds
            };
            _this.timer.displayTime = _this.getSecondsAsDigitalClock(_this.timer.timeRemaining);
        });
        this.timer = {
            time: this.timeInSeconds,
            runTimer: false,
            hasStarted: false,
            hasFinished: false,
            timeRemaining: this.timeInSeconds
        };
    };
    StartservicePage.prototype.startTimer = function () {
        console.log('hola');
        this.timer.hasStarted = true;
        this.timer.runTimer = true;
        this.timerTick();
    };
    // hasFinished() {
    //   return this.timer.hasFinished;
    // }
    // pauseTimer() {
    //   this.timer.runTimer = false;
    // }
    StartservicePage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    StartservicePage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.timer.runTimer) {
                return;
            }
            _this.timer.timeRemaining--;
            console.log(_this.timer.timeRemaining);
            _this.timer.displayTime = _this.getSecondsAsDigitalClock(_this.timer.timeRemaining);
            if (_this.timer.timeRemaining > 0) {
                _this.timerTick();
            }
            else {
                _this.timer.hasFinished = true;
            }
        }, 1000);
    };
    StartservicePage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    StartservicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-startservice',template:/*ion-inline-start:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/startservice/startservice.html"*/'<ion-header>\n\n  <ion-navbar hideBackButton>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>cuidado en curso</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div *ngIf="type == 0">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-6>\n          <img src="{{nanyImage.screenshot1}}" style="width:90%;border-radius:50%;">\n        </ion-col>\n        <!-- <ion-col col-4>\n          <div text-center>\n            <ion-icon ios="ios-camera" style="color:#505050;font-size:60px;"></ion-icon>\n          </div>\n        </ion-col> -->\n        <ion-col col-6>\n          <div text-center (click)="call(\'911\')">\n            <img src="https://firebasestorage.googleapis.com/v0/b/nannaapp-1e22e.appspot.com/o/alarm.png?alt=media&token=f910dd5f-e7c9-46b2-8bfa-28a65a9faa09" style="width:90%;border-radius:50%;">\n          </div>\n        </ion-col>\n      </ion-row>\n      <br><br>\n      <ion-row>\n        <ion-col col-12>\n          <div  text-center>\n            <h4>{{nannyP.nickname}}  es tu nanny de la guarda.</h4>\n          </div>\n        </ion-col>\n      </ion-row>\n      <br><br>\n      <ion-row>\n        <ion-col col-6>\n            <button ion-button block style="background-color: #696969"  (click)="goToChat(usuario.displayName)">Enviar mensaje</button>\n        </ion-col>\n        <ion-col col-6>\n            <button ion-button block style="background-color: #696969" (click)="call()">Llamar nanny</button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-12>\n          <button ion-button block style="background-color: #696969" (click)="modal_detallesU(this.nanyId, this.userId, this.key)">Detalles</button>\n        </ion-col>\n      </ion-row>\n      <br><br>\n      <!-- <ion-row>\n        <ion-col col-6>\n          <button ion-button block style="background-color: #696969">Solicitar foto</button>\n        </ion-col>\n        <ion-col col-6>\n          <button ion-button block style="background-color: #696969">Solicitar video</button>\n        </ion-col>\n      </ion-row>\n      <br><br>\n      <ion-row>\n        <ion-col col-6>\n          <button ion-button block style="background-color: #696969">Agregar horas</button>\n        </ion-col>\n        <ion-col col-6>\n          <button ion-button block style="background-color: #696969">Geolocalización</button>\n        </ion-col>\n      </ion-row>\n      <br><br>\n      <ion-row>\n        <ion-col col-12>\n          <button ion-button block style="background-color: #696969">Finalizar cuidado</button>\n        </ion-col>\n      </ion-row> -->\n      <div>\n        <ion-item class="no-bottom-border item">\n          <h1 text-center color="primary" class="timer-button timer-text">{{timer.displayTime}}</h1>\n        </ion-item>\n        <!-- <ion-fab bottom left *ngIf="timer.runTimer && timer.hasStarted && !timer.hasFinished">\n          <button ion-fab color="primary" (click)="pauseTimer()">\n        <ion-icon name="pause"></ion-icon>\n        </button>\n        </ion-fab> -->\n        <!-- <ion-fab bottom left *ngIf="!timer.runTimer && timer.hasStarted && !timer.hasFinished">\n          <button ion-fab color="primary" (click)="resumeTimer()">\n      <ion-icon name="play"></ion-icon>\n      </button>\n        </ion-fab> -->\n        <ion-fab bottom left *ngIf="!timer.hasStarted">\n          <button ion-fab color="primary" (click)="startTimer()" item-right>\n      <ion-icon name="play"></ion-icon>\n      </button>\n        </ion-fab>\n\n        <!-- <ion-fab bottom right *ngIf="!timer.runTimer && (timer.hasStarted || timer.hasFinished) || timer.hasFinished">\n          <button ion-fab color="danger" (click)="initTimer()" item-left>\n            <ion-icon name="refresh"></ion-icon>\n          </button>\n        </ion-fab> -->\n      </div>\n    </ion-grid>\n  </div>\n\n  <div *ngIf="type == 1">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-6>\n          <img src="{{usuario.foto}}?type=large&width=720&height=720" style="width:90%;border-radius:50%;">\n        </ion-col>\n        <!-- <ion-col col-4>\n          <div text-center>\n            <ion-icon ios="ios-camera" style="color:#505050;font-size:60px;"></ion-icon>\n          </div>\n        </ion-col> -->\n        <ion-col col-6>\n          <div text-center (click)="call(\'911\')">\n            <img src="https://firebasestorage.googleapis.com/v0/b/nannaapp-1e22e.appspot.com/o/alarm.png?alt=media&token=c235bd66-3fad-42e0-993a-21334ea6412a" style="width:90%;border-radius:50%;">\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col col-12>\n            <div text-center>\n              <h6>{{usuario.name}}</h6>\n            </div>\n          </ion-col>\n      </ion-row>\n      <br><br>\n      <ion-row>\n        <ion-col col-6>\n            <button ion-button block style="background-color: #696969" (click)="goToChat(usuario.displayName)">Enviar mensaje</button>\n        </ion-col>\n        <ion-col col-6>\n            <button ion-button block style="background-color: #696969" (click)="call(ordersN.movil)">Llamar a usuario</button>\n        </ion-col>\n      </ion-row>\n      <br><br>\n      <!-- <ion-row>\n        <ion-col col-6>\n          <button ion-button block style="background-color: #696969">Solicitar foto</button>\n        </ion-col>\n        <ion-col col-6>\n          <button ion-button block style="background-color: #696969">Solicitar video</button>\n        </ion-col>\n      </ion-row>\n      <br><br>\n      <ion-row>\n        <ion-col col-6>\n          <button ion-button block style="background-color: #696969">Agregar horas</button>\n        </ion-col>\n        <ion-col col-6>\n          <button ion-button block style="background-color: #696969">Geolocalización</button>\n        </ion-col>\n      </ion-row> -->\n      <br><br>\n      <ion-row>\n        <ion-col col-12>\n          <button ion-button block style="background-color: #696969" (click)="endService(key,ordersN.idUser,ordersN.idPlayerUser)">Finalizar cuidado</button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <div>\n      <ion-item class="no-bottom-border item">\n        <h1 text-center color="primary" class="timer-button timer-text">{{timer.displayTime}}</h1>\n      </ion-item>\n\n      <ion-fab bottom left *ngIf="!timer.hasStarted">\n        <button ion-fab color="primary" (click)="startTimer()" item-right>\n          <ion-icon name="play"></ion-icon>\n        </button>\n      </ion-fab>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/marioacaudillomelgoza/mario-caudillo/Nannapp/src/pages/startservice/startservice.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], StartservicePage);
    return StartservicePage;
}());

//# sourceMappingURL=startservice.js.map

/***/ })

},[371]);
//# sourceMappingURL=main.js.map