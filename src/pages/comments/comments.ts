import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';


@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
})
export class CommentsPage {

  nanyId:any;
  nanyR:any;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public afDB: AngularFireDatabase) {

                this.nanyId = this.navParams.get("nanyId");
                console.log(this.nanyId);

                this.nanyR = afDB.list('rating/' + this.nanyId).snapshotChanges().map(data => {
                  return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
                });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentsPage');
  }

  close(){
    this.viewCtrl.dismiss();
  }

}
