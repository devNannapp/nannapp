import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class AuthProvider {

  uid : any = {};
  usuario:any = {};

  constructor(public afAuth : AngularFireAuth, public  afDB: AngularFireDatabase) {
    console.log('Hello AuthProvider Provider');
  }

  // users(){
  //
  //   this.uid = this.afAuth.auth.currentUser.uid;
  //   console.log(this.uid);
  //
  //   this.afDB.object('users/'+this.uid+'/'+'meta').valueChanges().subscribe(data =>{
  //     this.usuario = data;
  //     console.log(this.usuario);
  //   });
  // }

}
