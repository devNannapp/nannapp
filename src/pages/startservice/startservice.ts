import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import { ChatPage } from '../chat/chat';
// import { HomePage } from '../home/home';
// import { RatingPage } from '../rating/rating';
import { ModalPage } from '../modal/modal';
import {PTimer} from "./ptimer";

@IonicPage()
@Component({
  selector: 'page-startservice',
  templateUrl: 'startservice.html',
})
export class StartservicePage {

  userId: string;
  public usuario = {};
  nanyId:any = {};
  key:any = {};
  ordersU:any = {};
  nannyP:any = {};
  nanyImage:any = {};
  ordersN:any = {};
  user:any;
  timeCuidado:any;
  data:any = {};
  usuarioType:any;
  uid:any;
  type:any;

  private timeInSeconds: number;
  public timer: PTimer;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public afDB: AngularFireDatabase,
              public afAuth:AngularFireAuth,
              public callNumber: CallNumber,
              public  modalCtrl: ModalController) {

                this.nanyId = this.navParams.get("nanyId");
                console.log(this.nanyId);
                this.userId = this.navParams.get("userId");
                console.log(this.userId);
                this.key = this.navParams.get("key");
                console.log(this.key);
                this.user = this.navParams.get("user");
                console.log(this.user);
                // this.timeCuidado = this.navParams.get("timeCuidado");
                // console.log(this.timeCuidado+'holsa');

                this.afDB.object('orders/'+this.userId+'/'+this.key+'/data/').valueChanges().subscribe(data =>{
                  this.ordersU = data;
                  console.log(this.ordersU);
                });

                this.afDB.object('orders_n/'+this.nanyId+'/'+this.key+'/data/').valueChanges().subscribe(data =>{
                  this.ordersN = data;
                  console.log(this.ordersN.movil);
                  this.afDB.object('users/'+this.ordersN.idUser+'/'+'meta/').valueChanges().subscribe(data =>{
                    this.usuario = data;
                    console.log(this.usuario);
                  });
                });

                this.afDB.object('users/'+this.userId+'/'+'meta').valueChanges().subscribe(data =>{
                  this.usuarioType = data;
                  this.type = this.usuarioType.user;
                  console.log(this.usuarioType.user);
                });

                this.afDB.object('products_meta/'+this.nanyId).valueChanges().subscribe(data =>{
                  this.nannyP = data;
                  console.log(this.nannyP);
                });

              this.afDB.object('products_images/'+this.nanyId).valueChanges().subscribe(data =>{
                this.nanyImage = data;
                console.log(this.nanyImage);
              });




  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad StartservicePage');

  }

  call(telephoneNumber) {
        this.callNumber.callNumber(telephoneNumber, true);
        this.callNumber.isCallSupported()
        .then(function (response) {
            if (response == true) {
              // do something
            }
            else {
              // do something else
            }
        });
  }

  goToChat(name){
      this.navCtrl.push( ChatPage, { 'key': this.key, 'name': name, 'nanyId': this.nanyId, 'user': this.user} );
  }

  modal_detallesU(nanyId, userId, key){

    let modal = this.modalCtrl.create( ModalPage, { 'nanyId' : nanyId, 'userId': userId, 'key': key } );
    modal.present();

  }

  endService(key, userId, idPlayerUser){

      let status = 3;

      this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data/').update({status});

      this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+key+'/data/').update({status});

      this.afDB.database.ref('users'+'/'+userId+'/'+'meta/').update({status: 5});

      this.afDB.database.ref('users'+'/'+this.userId+'/'+'meta/').update({status: 0});

      this.endServiceNoti(idPlayerUser);

  }

  endServiceNoti(idPlayerUser){

      console.log('Este es el Plyer id'+idPlayerUser);

        this.afDB.object('products_meta/'+this.nanyId).valueChanges().subscribe(data =>{
          this.data = data;
          if(! this.data){
            this.data={}
          }

          let noti =  {
            app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
            include_player_ids: [idPlayerUser],
            data: {"foo": "bar"},
            contents: {en: "Califica tu cuidado con " + this.data.title},
            headings: {en: "¡Gracias!"}

          }

          window["plugins"].OneSignal.postNotification(noti,
            function(successResponse) {
              console.log("Notification Post Success:", successResponse);
            },
            function (failedResponse) {
              console.log("Notification Post Failed: ", failedResponse);
              alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
            }
          );
        });

  }

  ngOnInit() {
    this.initTimer();
  }

  initTimer() {

      this.afDB.object('orders_n/'+this.nanyId+'/'+this.key+'/data/').valueChanges().subscribe(data =>{
        this.ordersN = data;
        this.timeCuidado = this.ordersN.timeCuidado;
        console.log(this.ordersN.numChil);
        var tiempo = this.timeCuidado * 3600;
        if (!this.timeInSeconds) { this.timeInSeconds = tiempo; }

        this.timer = <PTimer>{
          time: this.timeInSeconds,
          runTimer: false,
          hasStarted: false,
          hasFinished: false,
          timeRemaining: this.timeInSeconds
        };
        this.timer.displayTime = this.getSecondsAsDigitalClock(this.timer.timeRemaining);
      });



    this.timer = <PTimer>{
      time: this.timeInSeconds,
      runTimer: false,
      hasStarted: false,
      hasFinished: false,
      timeRemaining: this.timeInSeconds
    };

  }

  startTimer() {
    console.log('hola')
    this.timer.hasStarted = true;
    this.timer.runTimer = true;
    this.timerTick();
  }

  // hasFinished() {
  //   return this.timer.hasFinished;
  // }
  // pauseTimer() {
  //   this.timer.runTimer = false;
  // }

  resumeTimer() {
    this.startTimer();
  }

  timerTick() {
    setTimeout(() => {

      if (!this.timer.runTimer) { return; }
      this.timer.timeRemaining--;
      console.log(this.timer.timeRemaining);
      this.timer.displayTime = this.getSecondsAsDigitalClock(this.timer.timeRemaining);
      if (this.timer.timeRemaining > 0) {
        this.timerTick();
      }
      else {
        this.timer.hasFinished = true;
      }
    }, 1000);
  }

  getSecondsAsDigitalClock(inputSeconds: number) {
    var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);
    var hoursString = '';
    var minutesString = '';
    var secondsString = '';
    hoursString = (hours < 10) ? "0" + hours : hours.toString();
    minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
    secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
    return hoursString + ':' + minutesString + ':' + secondsString;
  }


}
