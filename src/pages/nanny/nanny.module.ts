import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NannyPage } from './nanny';

@NgModule({
  declarations: [
    NannyPage,
  ],
  imports: [
    IonicPageModule.forChild(NannyPage),
  ],
})
export class NannyPageModule {}
