import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { RequestformPage } from '../requestform/requestform';
import { AlertController } from 'ionic-angular';

import { RequestPage } from '../request/request';
import { CommentsPage } from "../comments/comments";


import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { ReservedPage } from '../reserved/reserved';
import { HomePage } from '../home/home';
// import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-nanny',
  templateUrl: 'nanny.html',
})
export class NannyPage {

  nannyP:any = {};
  nanyImage:any = {};
  nanyId:any = {};
  category:any = {};
  price:any = {};
  usersFa:any = {};
  uid:any;
  usuario:any;
  idPlayerUser:any;
  nanyR:any;
  nanyRo:any;
  promedio:any;
  //extra variables
  nanyTrip:any = {};
  tarifas:any = {};
  standHoursOfUser:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public afDB: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public  modalCtrl: ModalController) {

      this.nanyId = this.navParams.get("nanyId");
      this.category = this.navParams.get("category");
      this.uid = this.afAuth.auth.currentUser.uid;
      this.afDB.object('tarifas').valueChanges().subscribe(data =>{
        this.tarifas = data;

        console.log(this.tarifas);
      });
      this.afDB.object('users/'+this.uid+'/meta/standHours').valueChanges().subscribe(data =>{
        this.standHoursOfUser = data;
      });
      if(this.category=='trip'){
        this.afDB.object('products_trip-meta/'+this.nanyId).valueChanges().subscribe(data =>{
          this.nanyTrip = data;
          console.log(this.nanyTrip);
        });
      }
      console.log(this.nanyId);

      this.afDB.object('products_meta/'+this.nanyId).valueChanges().subscribe(data =>{
        this.nannyP = data;

        if(this.category == 'traditional'){
          //if(this.nannyP.tarifa == 'BASIC'){
          //  this.price=this.tarifas.traditionalBasic;
          //  console.log("la tarifa traditional basic es "+ this.tarifas.traditionalBasic);
          //}else if(this.nannyP.tarifa == 'LUXE'){
            this.price=this.tarifas.traditionalLuxe;
            console.log("la tarifa traditional luxe es "+ this.tarifas.traditionalLuxe);
          
          //}
        }else if(this.category == 'stand'){
          //if(this.nannyP.tarifa == 'BASIC'){
          //  this.price=this.tarifas.standBasic;
          //  console.log("la tarifa day basic es "+ this.tarifas.standBasic);
          
          //}else if(this.nannyP.tarifa == 'LUXE'){
            this.price=this.tarifas.standLuxe;
          //  console.log("la tarifa day luxe es "+ this.tarifas.standLuxe);
          //}
        }else if(this.category == 'dream'){
          //if(this.nannyP.tarifa == 'BASIC'){
          //  this.price=this.tarifas.dreamBasic;
          //  console.log("la tarifa dream basic es "+ this.tarifas.dreamLuxe);
      
          //}else if(this.nannyP.tarifa == 'LUXE'){
            this.price=this.tarifas.dreamLuxe;
            //console.log("la tarifa dream luxe es "+ this.tarifas.standLuxe);
          
          //}
        }else if(this.category == 'week'){
          //if(this.nannyP.tarifa == 'BASIC'){
          //  this.price=this.tarifas.semanaBasic;
          //  console.log("la tarifa week basic es "+ this.tarifas.semanaBasic);
          
          //}else if(this.nannyP.tarifa == 'LUXE'){
            this.price=this.tarifas.semanaLuxe;
          //  console.log("la tarifa week luxe es "+ this.tarifas.semanaLuxe);
          
          //}
        }else if(this.category == 'trip'){
          if(this.nanyTrip.tarifa == 'NATIONAL'){
            this.price=this.tarifas.tripNacional;
            console.log("la tarifa trip nacional 1 día es "+ this.tarifas.tripNacional);
          
          }else if(this.nanyTrip.tarifa == 'INTERNATIONAL'){
            this.price=this.tarifas.tripInternacional;
            console.log("la tarifa trip internacional 1 día es "+ this.tarifas.tripInternacional);
          
          }
        }else if(this.category == 'party'){
          if(this.nannyP.tarifa == 'BASIC'){

          }else if(this.nannyP.tarifa == 'LUXE'){

          }
        }else if(this.category == 'special'){
          this.price=this.tarifas.special;
            console.log("la tarifa special es "+ this.tarifas.special);
          
        }else if(this.category == 'grand'){
          this.price=this.tarifas.grand;
            console.log("la tarifa grand es "+ this.tarifas.grand);
          
        }else if(this.category == 'stand100'){
          //if(this.nannyP.tarifa == 'BASIC'){
          //  this.price=this.tarifas.traditionalBasic;
          //  console.log("la tarifa traditional basic es "+ this.tarifas.traditionalBasic);
          //}else if(this.nannyP.tarifa == 'LUXE'){
            this.price=this.tarifas.traditionalLuxe;
          //  console.log("la tarifa traditional luxe es "+ this.tarifas.traditionalLuxe);
          
          //}
        }


        /*if(this.category == 'dream'){
          this.price = this.nannyP.tarifaDre;
          console.log(this.price);
        }else if(this.category == 'traditional'){
          this.price = this.nannyP.tarifaTra;
          console.log(this.price);
        }else if(this.category == 'stand'){
          this.price = this.nannyP.tarifaSta;
          console.log(this.price);
        }else if(this.category == 'special'){
          this.price = this.nannyP.tarifaSG;
          console.log(this.price);
        }else if(this.category == 'grand'){
          this.price = this.nannyP.tarifaSG;
          console.log(this.price);
        }
        console.log(this.nannyP);*/
      });

    this.afDB.object('rating/'+this.nanyId).valueChanges().subscribe(data =>{
      this.nanyRo = data;
      if (!this.nanyRo) {

          this.promedio = '4.90';

      }else{

          let resultado = (Math.round(this.nanyRo.service/this.nanyRo.count*100)/100).toString();
          if((this.nanyRo.service/this.nanyRo.count*100)%100 == 0){
            resultado+='.00';
          }
          this.promedio = resultado;
          console.log(this.nanyRo.service);

      }
    });

      this.nanyR = afDB.list('rating/' + this.nanyId).snapshotChanges().map(data => {
        return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });


    this.afDB.object('products_images/'+this.nanyId).valueChanges().subscribe(data =>{
      this.nanyImage = data;
      console.log(this.nanyImage);
    });

    this.afDB.object('users/'+ this.uid + '/favorite/' + this.nanyId).valueChanges().subscribe(data =>{
      this.usersFa = data;
      if(! this.usersFa){
        this.usersFa = false
      }
      console.log(this.usersFa);
    });

    this.afDB.object('users/'+ this.uid +'/idPlayer').valueChanges().subscribe(data =>{
      this.idPlayerUser = data;
      console.log(this.idPlayerUser);
    });

  //   this.usersFa = afDB.list('users/' + this.uid + '/favorite' + this.nanyId).snapshotChanges().map(data => {
  //     return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
  //   });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NannyPage');
  }

  goRequest(nanyId){
    this.navCtrl.push( RequestPage, { 'nanyId' : nanyId } );
  }

  goRequestForm(nanyId, idPlayer, idPlayerUser){
    if(this.category != 'grand' && this.category != 'stand'){
      let prompt = this.alertCtrl.create({
      title: '<h4>PROGRAMA TU CUIDADO</h4>',
      message: '<h6>Numero de niños que requieren Nanny.</h6> <br>',
      inputs : [
      {
          type:'radio',
          label:'1',
          value:'1'
      },
      {
          type:'radio',
          label:'2',
          value:'2'
      },
      {
          type:'radio',
          label:'3',
          value:'3'
      }],
      buttons : [
      {
          text: "Cancelar",
          handler: childrens => {
          console.log(childrens);
          }
      },
      {
          text: "Aceptar",
          handler: childrens => {
          console.log(childrens);
          console.log(idPlayerUser);
          this.navCtrl.push( RequestformPage, { 'nanyId' : nanyId, 'childrens': childrens, 'price' : this.price, 'category': this.category, 'idPlayer': idPlayer, 'idPlayerUser': idPlayerUser} );
          }
      }]});
      prompt.present()
    }else if(this.category == 'grand'){
      let prompt = this.alertCtrl.create({
        title: '<h4>PROGRAMA TU CUIDADO</h4>',
        message: '<h6>Numero de adultos mayores que requieren Nanny.</h6> <br>',
        inputs : [
        {
            type:'radio',
            label:'1',
            value:'1'
        },
        {
            type:'radio',
            label:'2',
            value:'2'
        }],
        buttons : [
        {
            text: "Cancelar",
            handler: childrens => {
            console.log(childrens);
            }
        },
        {
            text: "Aceptar",
            handler: childrens => {
            console.log(childrens);
            console.log(idPlayerUser);
            console.log("nanny.ts  precio pasado "+this.price);
            this.navCtrl.push( RequestformPage, { 'nanyId' : nanyId, 'childrens': childrens, 'price' : this.price, 'category': this.category, 'idPlayer': idPlayer, 'idPlayerUser': idPlayerUser} );
            }
        }]});
        prompt.present()
    }else{
      let prompt = this.alertCtrl.create({
        title: '<h4>PAGA TU PAQUETE</h4>',
        message: '<h6>Al pagar tu paquete podrás empezar a reservar y usar tus horas de cuidado.</h6> <br>',
        inputs : [
          {
              type:'radio',
              label:'50 horas',
              value:'50'
          },
          {
              type:'radio',
              label:'100 horas',
              value:'100'
          }],
        buttons : [
        {
            text: "Cancelar",
        },
        {
            text: "Aceptar",
            handler: childrens =>{
              if(childrens==50){
                this.price=this.tarifas.month;
              }else{
                this.price=this.tarifas.standLuxe;
              }
              let pagado=false;
              let key = this.afDB.createPushId();
              var data = {
              idUser:       this.uid,
              idNanny:      this.nanyId,
              tarifa:       this.price,
              category:     this.category,
              costo:        this.price,
              //pago:         'payment',
              status:       0,
              horas:        childrens
            };
            this.afDB.database.ref('orders'+'/'+this.uid+'/'+key).set({data});

          //this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+ key).set({data});

          if(!pagado){
            let standHours=this.standHoursOfUser;
            if(data.horas==50){
              standHours=50+parseInt(this.standHoursOfUser);
            }else{
              standHours=100+parseInt(this.standHoursOfUser);
            }
            console.log("******************** horas stand pasadas de la nanny"+standHours);

            let modal = this.modalCtrl.create( RequestPage, { 'nanyId' : this.nanyId, 'userId': this.uid, 'key' : key, 'standHours': standHours });
            modal.present();
          }else{
            
            let modal = this.modalCtrl.create( /*ReservedPage*/HomePage, { 'nanyId': this.nanyId, 'userId' : this.uid } );
            modal.present();
            let status=3;
            let standNanny=this.nanyId;
            let standHours=this.standHoursOfUser;
            if(data.horas==50){
              standHours=50+parseInt(this.standHoursOfUser);
            }else{
              standHours=100+parseInt(this.standHoursOfUser);
            }

            this.afDB.database.ref('users/'+this.uid+'/meta').update({standNanny})
            this.afDB.database.ref('users/'+this.uid+'/meta').update({standHours})
            this.afDB.database.ref('orders'+'/'+this.uid+'/'+key+'/data').update({status});
          }
          this.navCtrl.setRoot( ReservedPage, { 'nanyId' :   this.nanyId, 'userId' : this.uid } );            }
        }]});
        prompt.present()
    }
    
  }

  favorite(valor){

    this.uid = this.afAuth.auth.currentUser.uid;
    this.usuario = this.afDB.object( 'users/' + this.uid + '/favorite/' + this.nanyId );
      this.usuario.update({marked: valor});

  }

  goComments(){

    let modal = this.modalCtrl.create( CommentsPage, { 'nanyId': this.nanyId } );
    modal.present();

  }




}
