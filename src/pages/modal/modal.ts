import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { CallNumber } from '@ionic-native/call-number';


import { IdPage } from "../id/id";
import { ComprobantePage } from "../comprobante/comprobante";

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  userId: string;
  public usuario = {};
  nanyId:any = {};
  key:any = {};
  ordersU:any = {};
  nannyP:any = {};
  nanyImage:any = {};
  ordersN:any = {};
  uid : string;
  user:any = {};
  info:any = {};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public afDB: AngularFireDatabase,
              private afAuth: AngularFireAuth,
              public  modalCtrl: ModalController,
              public callNumber: CallNumber) {

            this.nanyId = this.navParams.get("nanyId");
            console.log(this.nanyId);
            this.userId = this.navParams.get("userId");
            console.log(this.userId);
            this.key = this.navParams.get("key");
            console.log(this.key);

            this.uid = this.afAuth.auth.currentUser.uid;

            this.afDB.object('orders/'+this.userId+'/'+this.key+'/data').valueChanges().subscribe(data =>{
              this.ordersU = data;
              console.log(this.ordersU);
            });

            this.afDB.object('orders_n/'+this.nanyId+'/'+this.key+'/data').valueChanges().subscribe(data =>{
              this.ordersN = data;
              console.log(this.ordersN.numChil);

              this.afDB.object('users/'+this.ordersN.idUser+'/'+'meta').valueChanges().subscribe(data =>{
                this.user = data;
                console.log(this.user);
              });

            });

            this.afDB.object('products_meta/'+this.nanyId).valueChanges().subscribe(data =>{
              this.nannyP = data;
              console.log(this.nannyP);
            });

          this.afDB.object('products_images/'+this.nanyId).valueChanges().subscribe(data =>{
            this.nanyImage = data;
            console.log(this.nanyImage);
          });

          this.afDB.object('users/'+this.uid+'/'+'meta').valueChanges().subscribe(data =>{
            this.usuario = data;
            console.log(this.usuario);
          });

          this.afDB.object('users/'+this.uid+'/'+'info').valueChanges().subscribe(data =>{
            this.info = data;
            console.log(this.usuario);
          });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

  close(){
    this.viewCtrl.dismiss();
  }

  modalId(userId){

    let modal = this.modalCtrl.create( IdPage, {'userId' : userId});
    modal.present();

  }

  modalComprobante(userId){

    let modal = this.modalCtrl.create( ComprobantePage, {'userId' : userId});
    modal.present();

  }

  call(telephoneNumber) {
        this.callNumber.callNumber(telephoneNumber, true);
        this.callNumber.isCallSupported()
        .then(function (response) {
            if (response == true) {
              // do something
            }
            else {
              // do something else
            }
        });
  }

}
