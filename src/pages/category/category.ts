import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireObject } from 'angularfire2/database/interfaces';
import { Observable } from 'rxjs/Observable';

import { NannyPage } from '../nanny/nanny';
import { InformacionPage } from '../informacion/informacion';

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {

  items: Observable<any[]>;
  itemsTraditional: Observable<any[]>;
  itemsSpecial: Observable<any[]>;
  itemsGrand: Observable<any[]>;
  itemsTrip: Observable<any[]>;
  itemsStand: Observable<any[]>;
  itemsStandSemana: Observable<any[]>;
  itemsDream: Observable<any[]>;
  itemsParty: Observable<any[]>;
  
  itemsImage: Observable<any[]>;
  usersFa: Observable<any[]>;
  category:any = {};

  usuario: AngularFireObject<any>;
  uid:any;
  data:any;
  filterNum=1;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private     afDB: AngularFireDatabase,
      private afAuth: AngularFireAuth,
      public  modalCtrl: ModalController) {
        
        this.category = this.navParams.get("category");
        console.log(this.category);
        this.uid = this.afAuth.auth.currentUser.uid;

        //if(this.filterNum==0){
        //this.itemsTraditional = afDB.list('products_traditional_meta',ref => ref.orderByChild('nickname')).snapshotChanges().map(data => {
        //  return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        //});
      //}else{
        //this.itemsTraditional = afDB.list('products_traditional_meta',ref => ref.orderByChild('age')).snapshotChanges().map(data => {
          //return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        //});
      //}

      this.itemsTraditional = afDB.list('products_traditional_meta').snapshotChanges().map(data => {
        return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });

        this.itemsSpecial = afDB.list('products_special_meta').snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsGrand = afDB.list('products_grand_meta').snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsTrip = afDB.list('products_trip-meta').snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsStand = afDB.list('products_stand_meta').snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsStandSemana = afDB.list('products_standSemana_meta').snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsDream = afDB.list('products_dream_meta').snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsParty = afDB.list('products_party_meta').snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.items = afDB.list('products_meta').snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsImage = afDB.list('products_images').snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.usersFa = afDB.list('users/' + this.uid + '/favorite').snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.afDB.object('users/'+this.uid+'/'+'orden/').valueChanges().subscribe(data =>{
          this.data = data;

          if(! this.data){
            this.data= false;
          }
          console.log(this.data);
        });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryPage');
  }

  nanny(nanyId){
    this.navCtrl.push( NannyPage, { 'nanyId' : nanyId, 'category' : this.category } );
  }

  favorite(key){

    this.uid = this.afAuth.auth.currentUser.uid;
    this.usuario = this.afDB.object( 'users/' + this.uid + '/favorite/' + key );
      this.usuario.update({marked: true});

  }

  // goToCategory(category:any){
  //   console.log( category );
  //   this.navCtrl.push( CategoryPage, { 'category' : category } );
  // }

  goToInfo(){

    let modal = this.modalCtrl.create( InformacionPage, { 'userId': this.uid } );
    modal.present();

  }

  filterBy(){
    //0 by nickname 
    //1 by age
    //2 antiguedad
    if(this.filterNum==1){
      this.filterNum=0;
    }else{
      this.filterNum=1;
    }
    if(this.filterNum==0){
        this.itemsTraditional = this.afDB.list('products_traditional_meta',ref => ref.orderByChild('nickname')).snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });
        this.itemsSpecial = this.afDB.list('products_special_meta',ref => ref.orderByChild('nickname')).snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsGrand = this.afDB.list('products_grand_meta',ref => ref.orderByChild('nickname')).snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsTrip = this.afDB.list('products_trip-meta',ref => ref.orderByChild('nickname')).snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsStand = this.afDB.list('products_stand_meta',ref => ref.orderByChild('nickname')).snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsStandSemana = this.afDB.list('products_standSemana_meta',ref => ref.orderByChild('nickname')).snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsDream = this.afDB.list('products_dream_meta',ref => ref.orderByChild('nickname')).snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

        this.itemsParty = this.afDB.list('products_party_meta',ref => ref.orderByChild('nickname')).snapshotChanges().map(data => {
          return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });

    }else if(this.filterNum==1){
      this.itemsTraditional = this.afDB.list('products_traditional_meta').snapshotChanges().map(data => {
        return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });
      this.itemsSpecial = this.afDB.list('products_special_meta').snapshotChanges().map(data => {
        return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });

      this.itemsGrand = this.afDB.list('products_grand_meta').snapshotChanges().map(data => {
        return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });

      this.itemsTrip = this.afDB.list('products_trip-meta').snapshotChanges().map(data => {
        return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });

      this.itemsStand = this.afDB.list('products_stand_meta').snapshotChanges().map(data => {
        return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });

      this.itemsStandSemana = this.afDB.list('products_standSemana_meta').snapshotChanges().map(data => {
        return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });

      this.itemsDream = this.afDB.list('products_dream_meta').snapshotChanges().map(data => {
        return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });

      this.itemsParty = this.afDB.list('products_party_meta').snapshotChanges().map(data => {
        return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });
    }
  }

}
