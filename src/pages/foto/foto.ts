import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { DomSanitizer } from "@angular/platform-browser";


@IonicPage()
@Component({
  selector: 'page-foto',
  templateUrl: 'foto.html',
})
export class FotoPage {

  foto:any;
  image:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public  modalCtrl: ModalController,
              public viewCtrl: ViewController,
              public sanitizer: DomSanitizer) {

                this.foto = this.navParams.get("foto");

                this.image = this.sanitizer.bypassSecurityTrustStyle(`url(${this.foto})`);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FotoPage');
  }

  close(){
    this.viewCtrl.dismiss();
  }

}
