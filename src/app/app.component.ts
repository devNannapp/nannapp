import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, ModalController, Nav} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { OneSignal } from '@ionic-native/onesignal';

import { HomePage } from '../pages/home/home';
import { PerfilPage } from '../pages/perfil/perfil';
import { PushnotificationProvider } from '../providers/pushnotification/pushnotification';
// import { AuthProvider } from '../providers/auth/auth';
import { ReservedPage } from '../pages/reserved/reserved';
import { LoginPage } from '../pages/login/login';
import { HistorialPage } from '../pages/historial/historial';
import { NanysfPage } from '../pages/nanysf/nanysf';
import { ContactPage } from '../pages/contact/contact';
import { PoliticasPage } from '../pages/politicas/politicas';
import { PreguntasPage }  from "../pages/preguntas/preguntas";
import { ImgcacheService } from '../global/services';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  home = HomePage;
  perfil = PerfilPage;
  reserved = ReservedPage;
  login = LoginPage;
  historial = HistorialPage;
  nanysf = NanysfPage;
  contactanos = ContactPage;
  preguntas = PreguntasPage;
  rootPage:any;
  uid :string;
  usuario:any = {};
  @ViewChild('nav') nav: Nav;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public   menuCtrl: MenuController,
              public  _pushProvider: PushnotificationProvider,
              public  afAuth: AngularFireAuth,
              public  afDB: AngularFireDatabase,
              public  modalCtrl: ModalController,
              private oneSignal: OneSignal,
              public imgcacheService: ImgcacheService) {

                platform.ready().then(() => {

                  this.rootPage = 'LoginPage';
                  // Okay, so the platform is ready and our plugins are available.
                  // Here you can do any higher level native things you might need.
                  statusBar.styleDefault();
                  splashScreen.hide();

                  this._pushProvider.init_notification();
                  // this._authProvider.users();
                  // Init ImgCache lib
                  imgcacheService.initImgCache().then(() => {
                    // this.nav.setRoot(this.rootPage);
                  });

                });

                this.initializeApp();

  }



  openPage( pagina: any ){
    this.rootPage = pagina;
    this.menuCtrl.close();
  }

  users(){
    this.uid = this.afAuth.auth.currentUser.uid;
    console.log(this.uid);

    this.afDB.object('users/'+this.uid+'/'+'meta').valueChanges().subscribe(data =>{
      this.usuario = data;
      console.log(this.usuario);
    });
  }

  signOut(pagina: any ) {
   this.afAuth.auth.signOut();
   this.rootPage = pagina;
   this.menuCtrl.close();

 }

 modal_politicas(){

   let modal = this.modalCtrl.create( PoliticasPage );
   modal.present();

 }

 initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      if (this.platform.is('cordova')) {
      // Optional OneSignal code for iOS to prompt users later
      // Set your iOS Settings
      var iosSettings = {};
      iosSettings["kOSSettingsKeyAutoPrompt"] = false; // will not prompt users when start app 1st time
      iosSettings["kOSSettingsKeyInAppLaunchURL"] = false; // false opens safari with Launch URL

      // OneSignal Code start:
      // Enable to debug issues.
      // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

      var notificationOpenedCallback = function(jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        if (jsonData.notification.payload.additionalData != null) {
          console.log("Here we access addtional data");
          if (jsonData.notification.payload.additionalData.openURL != null) {
            console.log("Here we access the openURL sent in the notification data");

          }
        }
      };

      window["plugins"].OneSignal
        .startInit('2036276e-adf8-46b2-8040-de0783b2956e', '748451395664')
        .iOSSettings(iosSettings) // only needed if added Optional OneSignal code for iOS above
        .inFocusDisplaying(window["plugins"].OneSignal.OSInFocusDisplayOption.Notification)
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();
      }else{
        console.log('OneSignal no funciona en Chrome');
      }
    });
  }

  init_notification(){

    if (this.platform.is('cordova')) {
        this.oneSignal.startInit('2036276e-adf8-46b2-8040-de0783b2956e', '748451395664');

        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

        this.oneSignal.handleNotificationReceived().subscribe(() => {
          console.log('Notificación Recibida');
        });

        this.oneSignal.handleNotificationOpened().subscribe(() => {
          // do something when a notification is opened
          console.log('Notificación Abierta');
        });

        this.oneSignal.endInit();
    }else{
      console.log('OneSignal no funciona en Chrome');
    }

  }

}
