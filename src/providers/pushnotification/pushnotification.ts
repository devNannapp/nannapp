import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';


@Injectable()
export class PushnotificationProvider {

  constructor(private oneSignal: OneSignal,
              public platform: Platform) {
    console.log('Hello PushnotificationProvider Provider');
  }

  init_notification(){

    if (this.platform.is('cordova')) {
        this.oneSignal.startInit('88f157f1-f2e1-41c4-bfec-b479cb7f4176', '748451395664');

        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

        this.oneSignal.handleNotificationReceived().subscribe(() => {
          console.log('Notificación Recivida');
        });

        this.oneSignal.handleNotificationOpened().subscribe(() => {
          // do something when a notification is opened
          console.log('Notificación Abierta');
        });

        this.oneSignal.endInit();
    }else{
      console.log('OneSignal no funciona en Chrome');
    }

  }

}
