import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartservicePage } from './startservice';

@NgModule({
  declarations: [
    StartservicePage,
  ],
  imports: [
    IonicPageModule.forChild(StartservicePage),
  ],
})
export class StartservicePageModule {}
