import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { OneSignal } from '@ionic-native/onesignal';
import { PayPal } from '@ionic-native/paypal';
import { Geolocation } from '@ionic-native/geolocation';
import { CallNumber } from '@ionic-native/call-number';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AgmCoreModule } from '@agm/core';
import { EmailComposer }  from '@ionic-native/email-composer';
import { IonRating } from '../components/ion-rating/ion-rating';
//import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file';

//pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PerfilPage } from '../pages/perfil/perfil';
import { CategoryPage } from '../pages/category/category';
import { NannyPage } from '../pages/nanny/nanny';
import { RequestPage } from '../pages/request/request';
import { RequestformPage } from '../pages/requestform/requestform';
import { ReservedPage } from '../pages/reserved/reserved';
import { ModalPage } from '../pages/modal/modal';
import { StartservicePage } from '../pages/startservice/startservice';
import { ChatPage } from '../pages/chat/chat';
import { InfoPage } from '../pages/info/info';
import { InformacionPage } from '../pages/informacion/informacion';
import { HistorialPage } from '../pages/historial/historial';
import { RatingPage } from '../pages/rating/rating';
import { NanysfPage } from '../pages/nanysf/nanysf';
import { ContactPage } from '../pages/contact/contact';
import { PoliticasPage } from '../pages/politicas/politicas';
import { IdPage } from "../pages/id/id";
import { ComprobantePage } from "../pages/comprobante/comprobante";
import { PreguntasPage } from "../pages/preguntas/preguntas";
import { CommentsPage } from "../pages/comments/comments";
import { FotoPage } from "../pages/foto/foto";

// firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { Facebook } from '@ionic-native/facebook';

//providers
import { AuthProvider } from '../providers/auth/auth';
import { PushnotificationProvider } from '../providers/pushnotification/pushnotification';
// import { UbicacionProvider } from '../providers/ubicacion/ubicacion';

import { LazyImgComponent }   from '../global/components/';
import { LazyLoadDirective }   from '../global/directives/';
import { ImgcacheService }    from '../global/services/';
import { IonicImageLoader } from 'ionic-image-loader';

export const firebaseConfig = {
  apiKey: "AIzaSyDdOavcjmsfa-KTXW1EbtuHOalvMCdB8Mk",
  authDomain: "nannaapp-1e22e.firebaseapp.com",
  databaseURL: "https://nannaapp-1e22e.firebaseio.com",
  projectId: "nannaapp-1e22e",
  storageBucket: "nannaapp-1e22e.appspot.com",
  messagingSenderId: "748451395664"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    
     PerfilPage,
     CategoryPage,
     NannyPage,
     RequestPage,
     RequestformPage,
     ReservedPage,
     ModalPage,
     StartservicePage,
     ChatPage,
     InfoPage,
     InformacionPage,
     HistorialPage,
     RatingPage,
     NanysfPage,
     ContactPage,
     PoliticasPage,
     IdPage,
     ComprobantePage,
     PreguntasPage,
     CommentsPage,
     FotoPage,
    
     IonRating, //<-- No comentar
    LazyImgComponent,
    LazyLoadDirective
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Atras'
    }),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AgmCoreModule.forRoot({
     apiKey: 'AIzaSyBTAwxynPEvzqFj7RAVwXxYNLUPgx5xwcg'
   }),
   IonicImageLoader.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PerfilPage,
    CategoryPage,
    NannyPage,
    RequestPage,
    RequestformPage,
    ReservedPage,
    ModalPage,
    StartservicePage,
    ChatPage,
    InfoPage,
    InformacionPage,
    HistorialPage,
    RatingPage,
    IonRating,
    NanysfPage,
    ContactPage,
    PoliticasPage,
    IdPage,
    ComprobantePage,
    PreguntasPage,
    FotoPage,
    CommentsPage,
    LazyImgComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    Facebook,
    AuthProvider,
    OneSignal,
    PushnotificationProvider,
    PayPal,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    // UbicacionProvider,
    CallNumber,
    EmailComposer,
    InAppBrowser,
    //BarcodeScanner,
    Camera,
    ImagePicker,
    File,
    ImgcacheService,
  ]
})
export class AppModule {}
