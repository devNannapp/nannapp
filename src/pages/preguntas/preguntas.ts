import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PreguntasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-preguntas',
  templateUrl: 'preguntas.html',
})
export class PreguntasPage {

  items:any;
  shownItem:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.items = [{
         title: '1.	¿Qué servicios ofrece Nannapp?',
         text: 'Nannapp es una plataforma digital que enlaza a familias que requieren a una nanny calificada que cuide de sus hijos en casa de acuerdo a su edad. Cuidamos a niños desde 0 a 12 años. Ofrecemos 3 tarifas dentro del perfil TRADITIONAL, que son: ON DEMAND (nanny por hora), STAND (nanny recurrente, paquete por 50 horas al mes) y DREAM (nanny nocturna, tarifa especial por 12 horas).'
       },{
         title: '2.	¿Cuál es la filosofía de Nannapp?',
         text: 'Vivimos en una era en la que el ritmo de vida es acelerado y los padres de familia pasan mucho tiempo trabajando para tener una mejor calidad de vida. Es importante que mamá y papá también puedan atender sus compromisos personales sin descuidar a sus hijos en el hogar. Nuestras nannies de la guarda son un ángel en casa; además de tutoras, son profesionales en el cuidado de los niños y ellos estará acompañados como merecen, queremos que los niños sean más felices porque ellos son el futuro de nuestra sociedad.'
       },{
         title: '3.	¿Cuál es el proceso de selección de la nanny?',
         text: 'Cada nanny cuenta con licenciatura en enfermería, psicología, pedagogía, puericulturismo o alguna disciplina relacionada con la salud, educación y crianza de los niños. Se les aplican pruebas psicométricas, entrevista y un estudio socioeconómico y laboral. Todas son evaluadas con base a su experiencia y competencias, solo así podrán darse de alta en nuestra plataforma. Nannapp también permite a los usuarios ver las evaluaciones que otras familias les dan después de cada cuidado, así como el rango de edad de los niños que están capacitadas para cuidar (0-1.5, 1.5 a 3, 3-6, 6-9 y 9-12 años) y si son o no amigables con las mascotas (pet friendly), para prevenir alguna contingencia en caso de que la nanny sea alérgica y desempeñe bien su trabajo.'
       },{
         title: '4.	¿Cuál es la ventaja de contratar a una nanny de Nannapp?',
         text: 'La ventaja es que su formación académica y los filtros de selección garantizan su vocación, confiabilidad y capacitación por parte de Nannapp en primeros auxilios, psicopedagogía y atención al cliente. Se presentan uniformadas, están supervisadas constantemente y cumplen con nuestro código de ética.'
       },{
         title: '5.	¿Son privados los datos de la familia que contrata a la nanny?',
         text: 'Sí, tanto los datos de las familias y las nannies están protegidos. Sugerimos leer nuestros avisos y políticas de privacidad.'
     },{
       title: '6.	¿Cómo es el método de pago?',
       text: 'Por medio de tarjeta de crédito. Al momento de ser aceptada una reservación, se hace el cargo por el cuidado solicitado. En caso de cancelación se reembolsará el cargo realizado por el mismo medio que se pagó.'
   },{
     title: '7.	¿En dónde puedo solicitar factura? ',
     text: 'Nuestro sistema de facturación es por medio de la página web nannapp.com.mx y todas nuestras tarifas ya incluyen IVA.'
  },{
    title: '8. ¿Hay penalización en caso de cancelar el servicio y qué pasa si no se cancela pero tampoco llega la nanny?',
    text: 'La nanny debe enviar una notificación avisando a la familia que va en camino. Si la cancelación, por parte de la familia o la nanny, se hace 3 horas antes del servicio, el reembolso será del 100%. Si la cancelación, por parte de la familia ,se hace dentro de las 3 horas previas al cuidado reservado, se le hará el cargo por las horas de la duración del cuidado; si no cancela y la nanny llega pero nadie la recibe; además se levantará un reporte para la familia. Si la cancelación, por parte de la nanny, se hace dentro de las 3 horas previas al cuidado, Nannapp compensará a la familia con una hora gratis en su siguiente contratación al presentarnos su respectiva aclaración por escrito. Si la nanny no cancela y tampoco llega, la familia debe levantar un reporte y puede solicitar una nanny de emergencia, además de ser compensada por Nannapp con 2 horas gratis en su siguiente cuidado. A los 5 reportes, tanto para la familia como para la nanny, Nannapp considerará darlos de baja de la plataforma.'
  },{
    title: '9. ¿Cuánto tiempo de tolerancia hay para que la nanny o la familia lleguen puntuales en el horario contratado?',
    text: 'La tolerancia es de 15 minutos. Si la nanny llega al minuto 16, la primera hora del cuidado es gratis. Si la familia llega al minuto 16 de que venció la contratación, se cobra la hora completa.'
  },{
    title: '10.	¿Qué pasa si una vez que la nanny está cuidando a mis hijo requiero que se quede más tiempo con él?',
    text: 'En el perfil de la nanny existe la opción de agregar más horas.'
  },{
    title: '11.	¿Con cuantas horas de anticipación debo contratar el servicio?',
    text: 'Por motivos de disponibilidad se pide un mínimo de 3 horas de anticipación. Se recomienda hacer las reservaciones que inicien antes de medio dia, al menos la noche de un día antes, esto para que nuestras nannies confirmen el cuidado, mientras se hagan las reservaciones con el mayor tiempo de anticipación posible para encontrar disponible a la nanny de su preferencia.'
  },{
    title: '12.	¿La familia o la nanny cuentan con algún tipo de seguro durante la contratación del servicio?',
    text: 'Sí, Nannapp tiene una póliza de seguro contra accidentes para los niños y la nanny mientras los cuidan. La plataforma incluye un botón que marca directo a la compañía del seguro y los pasos a seguir en caso de suceder algún percance.'
  },{
    title: '13.	¿Puedo contratar a la misma nanny para futuros cuidados?',
    text: 'Sí, búscala en tu historial de cuidados y agrégala a favoritas para que puedas enviarle solicitud de reservación, la cual ella aceptará o declinará según su disponibilidad. También puedes contratar la tarifa STAND que está dentro del perfil TRADITIONAL, ésta permite contratar de manera recurrente a la misma nanny, por un paquete de prepago que incluye 50 horas por $7000 pesos al mes. Lo más recomendable es programar una agenda con anticipación para que ella esté disponible y puedas reservar los días y horas en los que la necesitarás. Si ella no está disponible o deseas cambiar de nanny, debes buscar a otra de tu preferencia que acepte continuar con esa tarifa por las horas que te resten del paquete.'
  },{
    title: '14.	¿Qué pasa si quiero contratar por fuera a la nanny por un mejor precio?',
    text: 'En ese caso, Nannapp no se hace responsable por retrasos, cancelaciones o rembolsos, ni puede sustituirla; además de que el seguro contra accidentes pierde cobertura. La plataforma cuenta con otras ventajas que protegen a la familia y como el sistema de geolocalización y el botón de emergencia que enlaza directamente a las autoridades en caso de algún incidente. Además, tenemos un paquete de recompensas anual para las familias que sumen más horas contratadas, al igual que un bono para las nannies que coticen más cuidados y estén mejor evaluadas.'
  },{
    title: '15.	¿Puedo evaluar a la nanny que cuidó a mi hijo?',
    text: 'Sí, al finalizar cada cuidado aparecerán en el sistema 5 patitos que equivalen a la calidad del servicio, además de poder escribir algún comentario que será público para que otras familias puedan leerlo. Esto ayuda a motivar y mejorar el desempeño de la nanny. De igual manera, la nanny puede evaluar a la familia de manera privada, ya que es importante el respeto entre ambas partes para volver a ser contratadas o que quieran regresar a su hogar para cuidar a sus hijos.'
  },{
    title: '16.	¿Hasta cuántos niños puede cuidar una nanny por la misma tarifa?',
    text: 'El máximo de niños por los que la nanny puede hacerse responsable son 2 (de 0 a 3 años) y 3 (arriba de 6 años), a partir de uno más debe solicitarse otra nanny.'
  },{
    title: '17.	¿Puedo contratar a una nanny para que cuide varios niños en un evento social? ',
    text: 'En caso de requerirlo, debe enviar un email con los datos del evento para solicitar este servicio y esperar nuestra confirmación para reservar las nannies necesarias, por tranquilidad de los padres y seguridad de los niños.'
  },{
    title: '18.	¿Pueden cuidar a mi hijo si tiene alguna discapacidad física o mental?',
    text: 'En caso de que su hijo requiera de cuidados especiales debe especificarlo en el registro de familia y enviarnos un correo solicitando el servicio para buscar a la nanny ideal que pueda cuidarlo de acuerdo a sus necesidades.'
  },{
    title: '19.	¿La nanny también puede hacer el aseo de mi casa o pasear a la mascota?',
    text: 'No, el servicio es exclusivo a las labores que respectan el cuidado de los niños. Esto puede incluir la limpieza del área en donde convivieron o los utensilios de cocina para darle sus alimentos.'
  },{
    title: '20.	¿Qué incluye la tarifa DREAM dentro del perfil TRADITIONAL?',
    text: 'De 8pm a 12am tiene un costo de $1120, el cual equivale a la tarifa ON DEMAND. Apartir de la siguiente hora se cobran $1400 por un horario de 8pm a 8am, en el que se debe brindar hospedaje digno y un alimento a la nanny; quien se encargará de la alimentación, higiene, descanso e integridad de su hijo.'
  },{
    title: '21.	¿Cómo puedo saber lo que mi hijo está haciendo en casa mientras se queda con la nanny?',
    text: 'Nuestra plataforma tiene un chat privado en el que la familia puede solicitarle a la nanny fotos o videos, además de tener un sistema de geolocalización que le permite saber que su hijo está con la nanny en casa.'
  },{
    title: '22.	¿Qué pasa si una nanny no está cómoda con la familia?',
    text: 'La nanny puede suspender el servicio en caso de acoso, violencia o cualquier situación que la ponga en riesgo, así como presionar el botón de emergencia para solicitar ayuda a las autoridades. Se levanta un reporte y Nannapp reembolsa el cargo correspondiente por las horas canceladas.'
  },{
    title: '23.	¿Cómo activo un código QR promocional?',
    text: 'Los códigos QR proporcionados por Nannapp o nuestros aliados comerciales tienen la finalidad de brindar promociones a nuestros usuarios. Se activan en la pestaña que dice código QR en la sección del registro como familia.'
  },{
    title: '24.	¿Cómo funciona la tarifa STAND?',
    text: 'La tarifa STAND es el servicio de una nanny recurrente, el paquete incluye 100 horas, las fechas y horarios de cuidados se acordarán con la nanny. En caso que la familia quiera solicitar un cambio de nanny o cancelar el servicio, se tendrá una penalización de la mitad de horas faltantes de cubrir, se recomienda solicitar el servicio STAND una vez que se conozca a la nanny mediante servicios TRADITIONAL.'
  },];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreguntasPage');
  }

  toggleItem(item){
     if (this.isItemShown(item)) {
       this.shownItem = null;
     } else {
       this.shownItem = item;
     }
   }

  isItemShown(item) {
     return this.shownItem === item;
   }




}
