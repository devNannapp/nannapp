import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController  } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';


@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {

  myForm: FormGroup;
  data:any = {};
  userId:any = {};
  public usuario = {};


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public afDB: AngularFireDatabase,
              public afAuth:AngularFireAuth,
              public fb: FormBuilder,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController) {

                this.userId = this.afAuth.auth.currentUser.uid;

                this.myForm = this.fb.group({
                  ciudad:       ['', [Validators.required]],
                  delegacion:   ['', []],
                  colonia:      ['', [Validators.required]],
                  calle:        ['', [Validators.required]],
                  numEx:        ['', [Validators.required]],
                  numIn:        ['', []],
                  cp:           ['', [Validators.required]],
                  movil:        ['', [Validators.required]]
                 });

                 this.afDB.object('users/'+this.userId+'/'+'info/').valueChanges().subscribe(data =>{
                   this.data = data;
                   if(! this.data){
                     this.data = {};
                     this.data.delegacion = '';
                     this.data.numIn = '';
                   }
                   console.log(this.data);
                 });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoPage');
  }

  showAlertC() {
    let alert = this.alertCtrl.create({
      title: 'Por el momento solo tenemos servicio en: <br> Ciudad de México <br> y <br> Estado de México',
      buttons: ['Aceptar']
    });
    alert.present();

  }

  public addInfo(){

    let loader = this.loadingCtrl.create({
      spinner: 'crescent',
      content: "Actualizando información",
      duration: 3000
    });
    loader.present();

          var info = {
            ciudad  : this.data.ciudad,
            delegacion  : this.data.delegacion,
            colonia : this.data.colonia,
            calle : this.data.calle,
            numEx : this.data.numEx,
            numIn : this.data.numIn,
            cp  : this.data.cp,
            movil: this.data.movil
          };

          var orden = {
            ciudad  : this.data.ciudad,
            delegacion  : this.data.delegacion,
            colonia : this.data.colonia,
            calle : this.data.calle,
            numEx : this.data.numEx,
            numIn : this.data.numIn,
            cp  : this.data.cp,
          };

          this.afDB.database.ref('users'+'/'+this.userId+'/').update({info});

          this.afDB.database.ref('users'+'/'+this.userId+'/').update({orden});

    this.viewCtrl.dismiss();

  }

  close(){
  this.viewCtrl.dismiss();
  }

}
