import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController, AlertController  } from 'ionic-angular';


import { ModalPage } from '../modal/modal';
import { StartservicePage } from '../startservice/startservice';
import { HomePage } from '../home/home';
import { RequestPage } from '../request/request';


import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-reserved',
  templateUrl: 'reserved.html',
})
export class ReservedPage {

  userId: string;
  public usuario = {};
  nanyId:any = {};
  nannyP:any = {};
  ordersN:any = {};
  ordersU:any = {};
  usua:any = {};
  items: Observable<any[]>;
  home:any = {};
  data:any ={};
  uid:any;
  userHours:any;
  total:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public afDB: AngularFireDatabase,
              public afAuth:AngularFireAuth,
              public  modalCtrl: ModalController,
              private viewCtrl: ViewController,
              private alertCtrl: AlertController) {

                this.total=this.navParams.get("total");
        console.log("total pasado a request "+this.total);
            this.nanyId = this.navParams.get("nanyId");
            console.log('El id de la nany', this.nanyId);
            this.userId = this.afAuth.auth.currentUser.uid;
            console.log(this.userId);
            this.home = this.navParams.get("home");
            console.log(this.home);
            this.uid = this.afAuth.auth.currentUser.uid;

            this.afDB.object('products_meta/'+this.nanyId).valueChanges().subscribe(data =>{
              this.nannyP = data;
              console.log(this.nannyP);
            });

            this.afDB.object('users/'+this.userId+'/'+'meta').valueChanges().subscribe(data =>{
              this.usuario = data;
              console.log(this.usuario);
            });

            this.usua = afDB.list('users/').snapshotChanges().map(data => {
              return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
            });

            this.ordersN = afDB.list('orders_n/'+this.nanyId).snapshotChanges().map(data => {
              return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
            });

            this.ordersU = afDB.list('orders/'+this.userId).snapshotChanges().map(data => {
              return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
            });

            this.items = afDB.list('products_meta/').snapshotChanges().map(data => {
              return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
            });

            this.afDB.object('users/'+this.uid+'/meta/standHours').valueChanges().subscribe(data =>{
              this.userHours = data;
              
            });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReservedPage');
    this.viewCtrl.showBackButton(false);
  }

  modal_detalles(key, userId){

    let modal = this.modalCtrl.create( ModalPage, { 'nanyId' : this.nanyId, 'userId': userId, 'key': key } );
    modal.present();

  }

  modal_detallesU(key, nanyId){

    let modal = this.modalCtrl.create( ModalPage, { 'nanyId' : nanyId, 'userId': this.userId, 'key': key } );
    modal.present();

  }

  aceptService(key, userId, idPlayerUser, pago){

      let status = 1;
      this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data/').update({status});

      this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+key+'/data/').update({status});

      this.aceptServiceNoti(idPlayerUser, pago);
  }

  aceptServiceNoti(idPlayerUser, pago){

      console.log('Este es el Plyer id'+idPlayerUser);

        this.afDB.object('products_meta/'+this.nanyId).valueChanges().subscribe(data =>{
          this.data = data;
          if(! this.data){
            this.data={}
          }

          let noti =  {
            app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
            include_player_ids: [idPlayerUser],
            data: {"foo": "bar"},
            contents: {en: "Tu nanny " + this.data.title + " ha aceptado el cuidado."}
          }

          window["plugins"].OneSignal.postNotification(noti,
            function(successResponse) {
              console.log("Notification Post Success:", successResponse);
            },
            function (failedResponse) {
              console.log("Notification Post Failed: ", failedResponse);
              alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
            });
        });

        if (pago == undefined) {

          let noti =  {
            app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
            include_player_ids: [idPlayerUser],
            data: {"foo": "bar"},
            contents: {en: "Por favor realiza tu pago para poder ofrecerte el cuidado."}
          }

          window["plugins"].OneSignal.postNotification(noti,
            function(successResponse) {
              console.log("Notification Post Success:", successResponse);
            },
            function (failedResponse) {
              console.log("Notification Post Failed: ", failedResponse);
              alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
            });

        }

  }

  cancelService(key, userId, nannyId, idPlayerNannyUser, user){
    let category;
    this.afDB.object('orders/'+userId+"/"+key+"/data/category").valueChanges().subscribe(data =>{
      category = data;
      console.log("catego:  " +category);
      if(category=='stand100'){


        let currentMonth=new Date().getMonth()+1;
        let currentHour=new Date().getHours();
        let currentDay= new Date().getDate();
        let currentDate;
        if(currentDay<10){
          currentDate=new Date().getFullYear() + "-"+currentMonth+"-0"+currentDay;
        }else{
          currentDate=new Date().getFullYear() + "-"+currentMonth+"-"+currentDay;
        }
        console.log("la fecha actual es DDMMHH " + currentDate);
  
        let careDate;
        let careTime;
        let careHour;
        this.afDB.object('orders/'+userId+"/"+key+"/data/myDate").valueChanges().subscribe(data =>{
          careDate = data;
          console.log("la fecha del cuidado es  " +careDate);
          //if care date is the same as current
          if(currentDate==careDate){
            this.afDB.object('orders/'+userId+"/"+key+"/data/myTime").valueChanges().subscribe(data =>{
              careTime = data;
              careHour=parseInt( careTime+"".split(":")[0]);
              //if user gets a reembolso
              if(currentHour+3<=careHour){
  


                let alert = this.alertCtrl.create({
                  title: '¿Estas seguro de cancelar el servicio?',
                  message: 'tus horas se reestableceran en tu paquete stand.',
                  buttons: [
                    {
                      text: 'Cancelar',
                      role: 'Cancelar',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    },
                    {
                      text: 'Aceptar',
                      handler: () => {
    
                        let careHours;
                        this.afDB.object('orders/'+userId+"/"+key+"/data/timeCuidado").valueChanges().subscribe(data =>{
                          careHours = data;
                          this.reembolso(careHours);
                        });
    
    
    
    
    
                        let status = 4;
                        let cancel = user;
                        console.log(key);
                        console.log(userId);
                        console.log(nannyId);
          
                        this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data/').update({status, cancel});
          
                        this.afDB.database.ref('orders_n'+'/'+nannyId+'/'+key+'/data/').update({status, cancel});
          
                        //this.ServiceCancelNoti(nannyId, idPlayerNannyUser);
          
                        console.log('Este es el Plyer id'+idPlayerNannyUser);
                      }
                    }
                  ]
                });
                alert.present();
    



                //if user lost hours
              }else{
                let alert = this.alertCtrl.create({
                  title: '¿Estas seguro de cancelar el servicio?',
                  message: 'tus horas se perderan de acuerdo a los terminos del servicio.',
                  buttons: [
                    {
                      text: 'Cancelar',
                      role: 'Cancelar',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    },
                    {
                      text: 'Aceptar',
                      handler: () => {
    
                        let status = 4;
                        let cancel = user;
                        console.log(key);
                        console.log(userId);
                        console.log(nannyId);
          
                        this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data/').update({status, cancel});
          
                        this.afDB.database.ref('orders_n'+'/'+nannyId+'/'+key+'/data/').update({status, cancel});
          
                        //this.ServiceCancelNoti(nannyId, idPlayerNannyUser);
          
                        console.log('Este es el Plyer id'+idPlayerNannyUser);
                      }
                    }
                  ]
                });
                alert.present();
    
              }

            });
            //if care date is before
          }else{

            let alert = this.alertCtrl.create({
              title: '¿Estas seguro de cancelar el servicio?',
              message: 'tus horas se reestableceran en tu paquete stand.',
              buttons: [
                {
                  text: 'Cancelar',
                  role: 'Cancelar',
                  handler: () => {
                    console.log('Cancel clicked');
                  }
                },
                {
                  text: 'Aceptar',
                  handler: () => {

                    let careHours;
                    this.afDB.object('orders/'+userId+"/"+key+"/data/timeCuidado").valueChanges().subscribe(data =>{
                      careHours = data;
                      this.reembolso(careHours);
                    });





                    let status = 4;
                    let cancel = user;
                    console.log(key);
                    console.log(userId);
                    console.log(nannyId);
      
                    this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data/').update({status, cancel});
      
                    this.afDB.database.ref('orders_n'+'/'+nannyId+'/'+key+'/data/').update({status, cancel});
      
                    //this.ServiceCancelNoti(nannyId, idPlayerNannyUser);
      
                    console.log('Este es el Plyer id'+idPlayerNannyUser);
                  }
                }
              ]
            });
            alert.present();

            
            
          }
        });



      }else{



        let alert = this.alertCtrl.create({
          title: 'Confirmar cancelación',
          message: 'Estas seguro de cancelar el servicio',
          buttons: [
            {
              text: 'Cancelar',
              role: 'Cancelar',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Aceptar',
              handler: () => {
                let status = 4;
                let cancel = user;
                console.log(key);
                console.log(userId);
                console.log(nannyId);
  
                this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data/').update({status, cancel});
  
                this.afDB.database.ref('orders_n'+'/'+nannyId+'/'+key+'/data/').update({status, cancel});
  
                //this.ServiceCancelNoti(nannyId, idPlayerNannyUser);
  
                console.log('Este es el Plyer id'+idPlayerNannyUser);
              }
            }
          ]
        });
        alert.present();




      }
    });
      
      
      
      
      

      /*let alert = this.alertCtrl.create({
        title: 'Confirmar cancelación',
        message: 'Estas seguro de cancelar el servicio',
        buttons: [
          {
            text: 'Cancelar',
            role: 'Cancelar',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Aceptar',
            handler: () => {
              let status = 4;
              let cancel = user;
              console.log(key);
              console.log(userId);
              console.log(nannyId);

              this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data/').update({status, cancel});

              this.afDB.database.ref('orders_n'+'/'+nannyId+'/'+key+'/data/').update({status, cancel});

              this.ServiceCancelNoti(nannyId, idPlayerNannyUser);

              console.log('Este es el Plyer id'+idPlayerNannyUser);
            }
          }
        ]
      });
      alert.present();*/
  }

  reembolso(serviceTime){
    let standHours;
    standHours=parseInt(serviceTime)+ parseInt( this.userHours);
    this.afDB.database.ref('users/'+this.userId+'/meta').update({standHours})

  }


  ServiceCancelNoti(nannyId, idPlayerNannyUser){
    console.log(idPlayerNannyUser+'Este es el Player id')
    this.afDB.object('users/'+this.uid+'/'+'meta/').valueChanges().subscribe(data =>{
      this.data = data;
      if(! this.data){
        this.data={}
      }

      console.log('Este es el Plyer id'+idPlayerNannyUser);
      if (this.data.user == 0) {
        let noti =  {
          app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
          include_player_ids: [idPlayerNannyUser],
          data: {"foo": "bar"},
          contents: {en: this.data.displayName + " ha cancelado el cuidado."},
        }

        window["plugins"].OneSignal.postNotification(noti,
          function(successResponse) {
            console.log("Notification Post Success:", successResponse);
          },
          function (failedResponse) {
            console.log("Notification Post Failed: ", failedResponse);
            alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
          }
        );

      }else{
        this.afDB.object('products_meta/'+nannyId).valueChanges().subscribe(data =>{
          this.data = data;
          if(! this.data){
            this.data={}
          }

          let noti =  {
            app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
            include_player_ids: [idPlayerNannyUser],
            data: {"foo": "bar"},
            contents: {en: "Tu nanny "+ this.data.title + " ha cancelado el cuidado."}
          }

          window["plugins"].OneSignal.postNotification(noti,
            function(successResponse) {
              console.log("Notification Post Success:", successResponse);
            },
            function (failedResponse) {
              console.log("Notification Post Failed: ", failedResponse);
              alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
            }
          );
        });
      }

      console.log(this.data.displayName);
    });
  }

  startService(key, userId, idPlayerUser, timeCuidado){

      console.log(timeCuidado);

      let status = 2;

      let product = this.nanyId;

      this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data'+'/').update({status});

      this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+key+'/data/').update({status});

      this.afDB.database.ref('users'+'/'+userId+'/'+'meta/').update({status,key,product});

      this.afDB.database.ref('users'+'/'+this.userId+'/'+'meta/').update({status,key,product});


      this.navCtrl.push( StartservicePage, { 'nanyId' : this.nanyId, 'userId': userId, 'key': key, 'timeCuidado': timeCuidado } );

      this.StartServiceNoti(product, idPlayerUser);

  }

  StartServiceNoti(nannyId, idPlayerUser){

      console.log('Este es el Plyer id'+idPlayerUser);

        this.afDB.object('products_meta/'+nannyId).valueChanges().subscribe(data =>{
          this.data = data;
          if(! this.data){
            this.data={}
          }

          let noti =  {
            app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
            include_player_ids: [idPlayerUser],
            data: {"foo": "bar"},
            contents: {en: "Tu nanny " + this.data.title + " ha comenzado el cuidado."}
          }

          window["plugins"].OneSignal.postNotification(noti,
            function(successResponse) {
              console.log("Notification Post Success:", successResponse);
            },
            function (failedResponse) {
              console.log("Notification Post Failed: ", failedResponse);
              alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
            }
          );
        });

  }



  goToHome(){
      this.navCtrl.setRoot( HomePage );
  }

  goToPaymet(nanyId, codigo, key){

    console.log(nanyId);
    console.log(codigo);
    console.log(key);

    let modal = this.modalCtrl.create( RequestPage, { 'nanyId' : nanyId, 'userId': codigo, 'key' : key });
    modal.present();

  }

}
