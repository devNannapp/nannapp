import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NanysfPage } from './nanysf';

@NgModule({
  declarations: [
    NanysfPage,
  ],
  imports: [
    IonicPageModule.forChild(NanysfPage),
  ],
})
export class NanysfPageModule {}
