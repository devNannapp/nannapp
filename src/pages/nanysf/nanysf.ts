import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireObject } from 'angularfire2/database/interfaces';
import { Observable } from 'rxjs/Observable';

import { NannyPage } from '../nanny/nanny';

@IonicPage()
@Component({
  selector: 'page-nanysf',
  templateUrl: 'nanysf.html',
})
export class NanysfPage {

  items: Observable<any[]>;
  itemsImage: Observable<any[]>;
  usersFa: Observable<any[]>;
  category:any = {};

  usuario: AngularFireObject<any>;
  uid:any;

  constructor(public      navCtrl: NavController,
              public      navParams: NavParams,
              public     afDB: AngularFireDatabase,
              private     afAuth: AngularFireAuth) {

                this.category = this.navParams.get("category");
                this.uid = this.afAuth.auth.currentUser.uid;

                this.items = afDB.list('products_meta').snapshotChanges().map(data => {
                  return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
                });

                this.itemsImage = afDB.list('products_images').snapshotChanges().map(data => {
                  return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
                });

                this.usersFa = afDB.list('users/' + this.uid + '/favorite').snapshotChanges().map(data => {
                  return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
                });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NanysfPage');
  }

  nanny(nanyId){
    this.navCtrl.push( NannyPage, { 'nanyId' : nanyId, 'category' : this.category } );
  }

}
