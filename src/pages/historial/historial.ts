import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController  } from 'ionic-angular';


import { ModalPage } from '../modal/modal';
import { StartservicePage } from '../startservice/startservice';
import { HomePage } from '../home/home';
import { RequestformPage } from '../requestform/requestform';


import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-historial',
  templateUrl: 'historial.html',
})
export class HistorialPage {

  userId: string;
  public usuario = {};
  nanyId:any = {};
  nannyP:any = {};
  ordersN:any = {};
  ordersU:any = {};
  usua:any = {};
  items: Observable<any[]>;
  home:any = {};


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public afDB: AngularFireDatabase,
              public afAuth:AngularFireAuth,
              public  modalCtrl: ModalController,
               private viewCtrl: ViewController) {

            this.nanyId = this.navParams.get("nanyId");
            console.log('El id de la nany', this.nanyId);
            this.userId = this.afAuth.auth.currentUser.uid;
            console.log(this.userId);
            this.home = this.navParams.get("home");
            console.log(this.home);

            this.afDB.object('products_meta/'+this.nanyId).valueChanges().subscribe(data =>{
              this.nannyP = data;
              console.log(this.nannyP);
            });

            this.afDB.object('users/'+this.userId+'/'+'meta').valueChanges().subscribe(data =>{
              this.usuario = data;
              console.log(this.usuario);
            });

            this.usua = afDB.list('users/').snapshotChanges().map(data => {
              return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
            });

            this.ordersN = afDB.list('orders_n/'+this.nanyId).snapshotChanges().map(data => {
              return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
            });

            this.ordersU = afDB.list('orders/'+this.userId).snapshotChanges().map(data => {
              return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
            });

            this.items = afDB.list('products_meta/').snapshotChanges().map(data => {
              return data.map(c => ({ key: c.payload.key, ...c.payload.val() }));
            });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReservedPage');
    this.viewCtrl.showBackButton(false);
  }

  modal_detalles(key, userId){

    let modal = this.modalCtrl.create( ModalPage, { 'nanyId' : this.nanyId, 'userId': this.userId, 'key': key } );
    modal.present();

  }

  modal_detallesU(key, nanyId){

    let modal = this.modalCtrl.create( ModalPage, { 'nanyId' : nanyId, 'userId': this.userId, 'key': key } );
    modal.present();

  }

  aceptService(key, userId){

      let status = 1;

      this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data/').update({status});

      this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+key+'/data/').update({status});


  }

  cancelService(key, userId){

      let status = 4;

      this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data/').update({status});

      this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+key+'/data/').update({status});


  }

  startService(key, userId){

      let status = 2;

      let product = this.nanyId;

      this.afDB.database.ref('orders'+'/'+userId+'/'+key+'/data/').update({status});

      this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+key+'/data/').update({status});

      this.afDB.database.ref('users'+'/'+userId+'/'+'meta/').update({status,key,product});

      this.afDB.database.ref('users'+'/'+this.userId+'/'+'meta/').update({status,key,product});


      this.navCtrl.push( StartservicePage, { 'nanyId' : this.nanyId, 'userId': userId, 'key': key } );

  }

  goToHome(){
      this.navCtrl.setRoot( HomePage );
  }

  goRequestForm(nanyId, childrens, price, category){
    this.navCtrl.push( RequestformPage, { 'nanyId' : nanyId, 'childrens': childrens, 'price' : price, 'category': category } );
  }

}
