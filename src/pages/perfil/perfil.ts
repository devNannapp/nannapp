import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
// import { PushnotificationProvider } from '../../providers/pushnotification/pushnotification';
import { Geolocation } from '@ionic-native/geolocation';
//import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { InformacionPage } from '../informacion/informacion';
import { OneSignal } from '@ionic-native/onesignal';
import { AngularFireObject } from 'angularfire2/database/interfaces';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
// import * as firebase from 'firebase/app';
// import { Observable } from 'rxjs/Observable';
declare var google:any;

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  public usuario = {};
  uid : string;

  latitud: any;
  longitude: any;
  map: any;
  user: AngularFireObject<any>;
  idPlayer: any;
  type: any;
  product: any;
  CodPromo: any;


  @ViewChild('map') mapRef:ElementRef;

  //options: BarcodeScannerOptions;
  //encodeText:string='';
  //encodedData:any={};
  //scannedData:any={};
  //CodigoPromo:any={};
  mn:any;

  constructor(
              public navCtrl: NavController,
              public navParams: NavParams,
              public afDB: AngularFireDatabase,
              private afAuth: AngularFireAuth,
              public  modalCtrl: ModalController,
              public  oneSignal: OneSignal,
              private geolocation: Geolocation,
              //public barcodeScanner: BarcodeScanner,
              public toastCtrl: ToastController
            ) {

                this.uid = this.afAuth.auth.currentUser.uid;
                this.mn = '{ \"referencia\":\"MN_Nannapp\"}'

                this.afDB.object('users/'+this.uid+'/'+'meta').valueChanges().subscribe(data =>{
                  this.usuario = data;
                  console.log(this.usuario);
                });

              this.afDB.object('users/'+this.uid+'/'+'idPlayer').valueChanges().subscribe(data =>{
                   this.idPlayer = data;
                  console.log(this.idPlayer);
                });

              /*this.afDB.object('users/'+this.uid+'/'+'codigo/').valueChanges().subscribe(data =>{
                   this.CodigoPromo = data;
                   this.CodPromo = this.CodigoPromo.text;
                   console.log(this.CodPromo);
                   if(! this.CodigoPromo){
                     this.CodigoPromo = false;
                   }
                  console.log(this.CodigoPromo);
                });*/

                // this.oneSignal.init_notification();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
    console.log(this.mapRef);
    this.iniciarUser();
    this.iniciarGeolocalizacion();
    this.PlayerIDNanny();
    // this.displayMap();
  }

  startN(){
    this.oneSignal.getIds().then((id) => {
      console.log('MI ID de la suerte'+id.userId);

          var idPlayer = id.userId;

          this.afDB.database.ref('users'+'/'+this.uid+'/').update({idPlayer});

          let noti =  {
                        app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                        include_player_ids: [id.userId],
                        data: {"foo": "bar"},
                        contents: {en: "Notificaciones activadas."},
                        headings: {en: "Aviso"}
                      }


          window["plugins"].OneSignal.postNotification(noti,
      			function(successResponse) {
        				console.log("Notification Post Success:", successResponse);
      			},
      			function (failedResponse) {
        				console.log("Notification Post Failed: ", failedResponse);
      			}
    			);
    });
  }

  PlayerIDNanny(){

    this.afDB.object('users/'+this.uid).valueChanges().subscribe(data =>{
         this.idPlayer = data;
         if (this.idPlayer.meta.user == 1) {
           console.log(this.idPlayer.idPlayer);
           var idPlayer = this.idPlayer.idPlayer;
           this.afDB.database.ref('products_meta'+'/'+this.idPlayer.meta.product+'/').update({idPlayer});
         }else{
           console.log('No es Nanny');
         }

      });

  }

  goInfo(){

    let modal = this.modalCtrl.create( InformacionPage, { 'userId': this.uid } );
    modal.present();

  }

  iniciarUser(){

    this.uid = this.afAuth.auth.currentUser.uid;
    this.user = this.afDB.object( 'users/' + this.uid + '/geo/' );

  }

  iniciarGeolocalizacion(){

    this.geolocation.getCurrentPosition().then((resp) => {

      // resp.coords.latitude
      // resp.coords.longitude
      console.log(resp.coords);



      let watch = this.geolocation.watchPosition();

      watch.subscribe((data) => {
        // data can be a set of coordinates, or an error (if an error occurred).
        // data.coords.latitude
        // data.coords.longitude
        console.log('watch: ', data.coords );

        this.user.update({lat: data.coords.latitude, lng: data.coords.longitude});

      });

    }).catch((error) => {

      console.log('Error getting location', error);

    });


  }

  displayMap(){
    const location = new google.maps.LatLng( '42.598726', '-5.567096');

    const options = {
      center: location,
      zoom:10
    };

    this.map = new google.maps.Map(this.mapRef.nativeElement,options);
  }

/*scanners(){

    this.barcodeScanner.scan().then(barcodeData => {

      console.log('result:', barcodeData.text);
      console.log('format:', barcodeData.format);
      console.log('cancelled:', barcodeData.cancelled);

    }).catch(err => {
        console.log('Error', err);
    });

  }

  scan(){

    this.options = {
      prompt: 'Escanea tu QR'
    };

    this.barcodeScanner.scan(this.options).then((data) =>{
      this.scannedData = data;
      let codigo = this.scannedData
      this.afDB.database.ref('users'+'/'+this.uid+'/').update({codigo});

    }, (err) => {
      console.log('Error :', err);
    })

  }

  encode(){

    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE, this.encodeText).then((data) => {
      this.encodedData = data;
    }, (err) => {
      console.log('Error :', err);
    })

  }*/

}
