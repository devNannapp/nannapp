import { Component } from '@angular/core';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { IonicPage, NavController, NavParams, LoadingController, ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
// import { RequestformPage } from '../requestform/requestform';
import { ReservedPage } from '../reserved/reserved';
import { InAppBrowser } from '@ionic-native/in-app-browser';


import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { database } from 'firebase';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';

@IonicPage()
@Component({
  selector: 'page-request',
  templateUrl: 'request.html',
})
export class RequestPage {

  nannyP:any = {};
  nanyId:any = {};
  userId:any = {};
  ordersU:any = {};
  key:any = {};
  CodigoPromo:any={};
  dosBasic:any;
  dosLuxe:any;
  dosHrsBasic:any;
  tresLuxe:any;
  tresBasic:any;
  unaLuxe:any;
  unaBasic:any;
  tarifa:any;
  standHours:any = {};
  cpr:any;
  PromoCodes:any={};
  promoText:any;
  

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public afDB: AngularFireDatabase,
      public afAuth:AngularFireAuth,
      public alertCtrl: AlertController,
      public loadingCtrl: LoadingController,
      public payPal:  PayPal,
      public iab: InAppBrowser,
      public viewCtrl: ViewController) {
        this.promoText=this.navParams.get("promotext");
        this.cpr=this.navParams.get("cpr");
        this.standHours = this.navParams.get("standHours");
        this.nanyId = this.navParams.get("nanyId");
        this.userId = this.navParams.get("userId");
        console.log(this.userId);
        this.key = this.navParams.get("key");
        console.log(this.key);

        this.dosBasic = '2 Hrs Gratis BASIC'; //360
        this.dosHrsBasic = '2 horas BASIC'; //360
        this.dosLuxe = '2 horas LUXE'; //560
        this.tresLuxe = '3 horas LUXE'; //840
        this.tresBasic = '3 horas BASIC'; //540
        this.unaLuxe = '1 hora LUXE'; //280
        this.unaBasic = '1 hora BASIC'; //180

        this.afDB.object('products_meta/'+this.nanyId).valueChanges().subscribe(data =>{
          this.nannyP = data;
          console.log(this.nannyP);
        });

        this.afDB.object('promoCodes/'+this.cpr).valueChanges().subscribe(data =>{
          this.PromoCodes = data;
          
          if(this.PromoCodes != null || this.PromoCodes != undefined){
            console.log("////////////////////////////////////////"+this.PromoCodes.discount+" ha entrado");
            
          }else{
            console.log("/*********/******/*****"+this.PromoCodes);
          }
        });

        







        this.afDB.object('orders/'+this.userId+'/'+this.key+'/data').valueChanges().subscribe(data =>{
          this.ordersU = data;
          console.log(this.ordersU);
        });

        this.afDB.object('users/'+this.userId+'/'+'codigo/').valueChanges().subscribe(data =>{
             this.CodigoPromo = data;
             if(! this.CodigoPromo){
               this.CodigoPromo={};
             }
            console.log(this.CodigoPromo);
          });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RequestPage');
  }

  close(home){
    this.viewCtrl.dismiss();
  }

  pago(forma_pago, costo, home, descuento){
    console.log(forma_pago);
    console.log(home);
    console.log(costo.toString());
    
    /*let discount=(100-this.PromoCodes.discount)/100;
      let discountedPrice=costo*discount;
      console.log("precio con descuento "+ discount + " = "+discountedPrice);
    if(this.PromoCodes != null || this.PromoCodes != undefined){
      
      this.tarifa=discountedPrice;
      //update cost
    }*/




    //if(this.unaLuxe == descuento){

    //  this.tarifa = costo - 280;
    //  console.log(this.tarifa);
    //}else if(this.unaBasic == descuento){

    //  this.tarifa = costo - 180;
    //  console.log(this.tarifa);
    //}else if(this.dosLuxe == descuento){

      //this.tarifa = costo - 560;
      //console.log(this.tarifa);
    //}else if(this.dosBasic == descuento|| this.dosHrsBasic == descuento){

    //  this.tarifa = costo - 360;
    //  console.log(this.tarifa);
    //}else if(this.tresLuxe == descuento){

    //  this.tarifa = costo - 840;
    //  console.log(this.tarifa);
    //}else if(this.tresBasic == descuento){

    //  this.tarifa = costo - 540;
    //  console.log(this.tarifa);
    //}
    //else{

      this.tarifa = costo;
      console.log(this.tarifa);
    //}
  }

  pay(forma_pago, costo, home, descuento){
    console.log(forma_pago);
    console.log(home);
    console.log(costo.toString());
    /*
    let discount=(100-this.PromoCodes.discount)/100;
    let discountedPrice=costo*discount;
    console.log("precio con descuento "+ discount + " = "+discountedPrice);
  if(this.PromoCodes != null || this.PromoCodes != undefined){
    
    this.tarifa=discountedPrice;
  }else{

      this.tarifa = costo;
      console.log(this.tarifa);

    }*/
    this.tarifa = costo;
    console.log(this.tarifa);


    this.payPal.init({
      PayPalEnvironmentProduction: 'AVhw-GRoXY-7xXsyNXbwLj9PDrSfnISAQp7JgwAx7EYjv-LXzUYxsMjLdLvwUbwKNYpjrkhUv0BLxpl0',
      PayPalEnvironmentSandbox: 'ATdR1VentY1ZMhRaDvBXVRdVyU8jQf-YLusfzO1oNzgFuAZcGRsdJKij1Al-CU8l_E7-GGVg1mIaLvw4'
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentProduction', new PayPalConfiguration({
      // Only needed if you get an "Internal Service Error" after PayPal login!
      //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal this.tarifa.toString()
      })).then(() => {
        let payment = new PayPalPayment(this.tarifa.toString(), 'MXN',"Nanny: "+ this.nannyP.nickname, 'sale');
        payment.shortDescription="Nanny: "+this.nannyP.nickname+" fecha: "+this.ordersU.myDate+" hora: "+this.ordersU.myTime;
        this.payPal.renderSinglePaymentUI(payment).then((data) => {
          // Successfully paid
          console.log(JSON.stringify(data));
          // Example sandbox response
          //
          // {
          //   "client": {
          //     "environment": "sandbox",
          //     "product_name": "PayPal iOS SDK",
          //     "paypal_sdk_version": "2.16.0",
          //     "platform": "iOS"
          //   },
          //   "response_type": "payment",
          //   "response": {
          //     "id": "PAY-1AB23456CD789012EF34GHIJ",
          //     "state": "approved",
          //     "create_time": "2016-10-03T13:33:33Z",
          //     "intent": "sale"
          //   }
          // }

              
          //let status=3;
          let standNanny=this.nanyId;
          let standHours=this.standHours;

          this.afDB.database.ref('users/'+this.userId+'/meta').update({standNanny})
          this.afDB.database.ref('users/'+this.userId+'/meta').update({standHours})
          //this.afDB.database.ref('orders'+'/'+this.userId+'/'+this.key+'/data').update({status});
              
          let pago = 'payment';

          let codigo = false;

          this.afDB.database.ref('orders'+'/'+this.userId+'/'+this.key+'/data/').update({pago});


          this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+this.key+'/data/').update({pago});

          this.afDB.database.ref('users'+'/'+this.userId+'/codigo/').update({text: codigo});

          this.navCtrl.push( ReservedPage, { 'nanyId' :   this.nanyId, 'userId' : this.userId, 'home' : home } );

        }, () => {
          // Error or render dialog closed without being successful
        });
      }, () => {
        // Error in configuration
      });
    }, () => {
      // Error in initialization, maybe PayPal isn't supported or something else
    });





  }









}
