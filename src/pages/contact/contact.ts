import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { EmailComposer } from '@ionic-native/email-composer';

import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  data:any = {};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public emailComposer: EmailComposer,
              public callNumber: CallNumber) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

  sendEmail(){

    let email = {
      to: 'contacto@nannapp.com.mx',
      cc: 'nannappcompany@gmail.com',

      subject: this.data.asunto,
      body: this.data.mensaje
    };

    this.emailComposer.open(email);
  }

  call(telephoneNumber) {
        this.callNumber.callNumber(telephoneNumber, true);
        this.callNumber.isCallSupported()
        .then(function (response) {
            if (response == true) {
              // do something
            }
            else {
              // do something else
            }
        });
  }

}
