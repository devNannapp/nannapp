import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
// import { Geolocation } from '@ionic-native/geolocation';

import { RequestPage } from '../request/request';
import { ReservedPage } from '../reserved/reserved';


//import { AngularFireObject } from 'angularfire2/database/interfaces';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { withLatestFrom } from 'rxjs/operator/withLatestFrom';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
import { FirebaseDatabase } from '@firebase/database-types';



@IonicPage()
@Component({
  selector: 'page-requestform',
  templateUrl: 'requestform.html',
})
export class RequestformPage {


  childrens:any = {};
  codigo: string;
  data:any = {};
  key:any = {};
  users:any = {};
  displayName: string;
  nanyId:any = {};
  obj:any = {};
  userGeo:any = {};
  myForm: FormGroup;
  fechaActual:any = {};
  horaActual:any={};
  price:any = {};
  idPlayer:any = '';
  idPlayerUser:any = '';
  form:any = '';
  operacion:any;

  latitud: any;
  longitude: any;
  usuario: any = {};
  uid:any;
  category:any = {};
  total:any;
  orden:any;
  discount:any;
  promoText:any;
  //promocodes
  
  PromoCodes:any={};
  //UserTop:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public afDB: AngularFireDatabase,public afAuth:AngularFireAuth,
    public fb: FormBuilder,public loadingCtrl: LoadingController,public alertCtrl: AlertController,
    // private geolocation: Geolocation,
    public  modalCtrl: ModalController) 
    {
      this.promoText="";
      this.total=0;
      this.nanyId = this.navParams.get("nanyId");

      this.childrens = this.navParams.get("childrens");

      this.price = this.navParams.get("price");

      console.log("precio recibido "+this.price);

      this.category = this.navParams.get("category");

      this.idPlayer = this.navParams.get("idPlayer");

      this.idPlayerUser = this.navParams.get("idPlayerUser");
      console.log(this.idPlayerUser);

      console.log(this.price);
      console.log(this.category);
      console.log(this.idPlayer+'Nanny');
      console.log(this.idPlayerUser+'user')

      this.fechaActual = new Date().toJSON().split('T')[0];
      this.horaActual = new Date().getHours();

      this.uid = this.afAuth.auth.currentUser.uid;

      afAuth.authState.subscribe(user => {
        if (!user) {
          this.displayName = null;
        }
        this.codigo = user.uid;
        console.log(this.codigo);
      });

      this.discount=1;
      this.afDB.object('users/'+this.uid+'/'+'geo').valueChanges().subscribe(data =>{
        this.userGeo = data;
        console.log(this.userGeo);
      });

      /*this.afDB.object('users/'+this.uid).valueChanges().subscribe(data =>{
        this.UserTop = data;
        console.log(this.userGeo);
      });*/


      this.afDB.object('users/'+this.uid+'/meta').valueChanges().subscribe(data =>{
        this.usuario = data;
          console.log("numero de horas stand "+this.usuario.standHours);
        });

      

      this.afDB.object('users/'+this.uid+'/'+'orden/').valueChanges().subscribe(data =>{
        this.data = data;
        this.data.cpr = "";
        this.data.note = "";

        if (!this.data) {
          this.data = {};
          this.data.ciudad = '';
        }

        if (this.category == 'stand' || this.category == 'dream') {
          this.data.timeCuidado = "";
        }

        console.log(this.data);
      });

      this.data.ciudad = '';

      if (this.childrens == 1 && this.category == 'traditional') {
                  this.myForm = this.fb.group({
                    myDate:       ['', [Validators.required]],
                    myTime:       ['', [Validators.required]],
                    timeCuidado:  ['', [Validators.required]],
                    nombre1:      ['', [Validators.required]],
                    apellidop1:   ['', [Validators.required]],
                    apellidom1:   ['', [Validators.required]],
                    edad1:        ['', [Validators.required]],
                    fechan1:      ['', [Validators.required]],
                    nombre2:      ['', []],
                    apellidop2:   ['', []],
                    apellidom2:   ['', []],
                    edad2:        ['', []],
                    fechan2:      ['', []],
                    nombre3:      ['', []],
                    apellidop3:   ['', []],
                    apellidom3:   ['', []],
                    edad3:        ['', []],
                    fechan3:      ['', []],
                    note:         ['', []],
                    ciudad:       ['', [Validators.required]],
                    delegacion:   ['', []],
                    colonia:      ['', [Validators.required]],
                    calle:        ['', [Validators.required]],
                    numEx:        ['', [Validators.required]],
                    numIn:        ['', []],
                    cp:           ['', [Validators.required]],
                    movil:        ['', [Validators.required]],
                    cpr:          ['', []]
                   });
                   console.log('hola1');
                }else if(this.childrens == 1){
                  this.myForm = this.fb.group({
                    myDate:       ['', [Validators.required]],
                    myTime:       ['', [Validators.required]],
                    timeCuidado:  ['', []],
                    nombre1:      ['', [Validators.required]],
                    apellidop1:   ['', [Validators.required]],
                    apellidom1:   ['', [Validators.required]],
                    edad1:        ['', [Validators.required]],
                    fechan1:      ['', [Validators.required]],
                    nombre2:      ['', []],
                    apellidop2:   ['', []],
                    apellidom2:   ['', []],
                    edad2:        ['', []],
                    fechan2:      ['', []],
                    nombre3:      ['', []],
                    apellidop3:   ['', []],
                    apellidom3:   ['', []],
                    edad3:        ['', []],
                    fechan3:      ['', []],
                    note:         ['', []],
                    ciudad:       ['', [Validators.required]],
                    delegacion:   ['', []],
                    colonia:      ['', [Validators.required]],
                    calle:        ['', [Validators.required]],
                    numEx:        ['', [Validators.required]],
                    numIn:        ['', []],
                    cp:           ['', [Validators.required]],
                    movil:        ['', [Validators.required]],
                    cpr:          ['', []]
                   });
                   console.log('hola12');
                }
                if (this.childrens == 2 && this.category == 'traditional') {
                  this.myForm = this.fb.group({
                    myDate:       ['', [Validators.required]],
                    myTime:       ['', [Validators.required]],
                    timeCuidado:  ['', [Validators.required]],
                    nombre1:      ['', [Validators.required]],
                    apellidop1:   ['', [Validators.required]],
                    apellidom1:   ['', [Validators.required]],
                    edad1:        ['', [Validators.required]],
                    fechan1:      ['', [Validators.required]],
                    nombre2:      ['', [Validators.required]],
                    apellidop2:   ['', [Validators.required]],
                    apellidom2:   ['', [Validators.required]],
                    edad2:        ['', [Validators.required]],
                    fechan2:      ['', [Validators.required]],
                    nombre3:      ['', []],
                    apellidop3:   ['', []],
                    apellidom3:   ['', []],
                    edad3:        ['', []],
                    fechan3:      ['', []],
                    note:         ['', []],
                    ciudad:       ['', [Validators.required]],
                    delegacion:   ['', []],
                    colonia:      ['', [Validators.required]],
                    calle:        ['', [Validators.required]],
                    numEx:        ['', [Validators.required]],
                    numIn:        ['', []],
                    cp:           ['', [Validators.required]],
                    movil:        ['', [Validators.required]],
                    cpr:          ['', []]

                   });
                   console.log('hola2');
                }else if(this.childrens == 2){
                  this.myForm = this.fb.group({
                    myDate:       ['', [Validators.required]],
                    myTime:       ['', [Validators.required]],
                    timeCuidado:  ['', []],
                    nombre1:      ['', [Validators.required]],
                    apellidop1:   ['', [Validators.required]],
                    apellidom1:   ['', [Validators.required]],
                    edad1:        ['', [Validators.required]],
                    fechan1:      ['', [Validators.required]],
                    nombre2:      ['', []],
                    apellidop2:   ['', []],
                    apellidom2:   ['', []],
                    edad2:        ['', []],
                    fechan2:      ['', []],
                    nombre3:      ['', []],
                    apellidop3:   ['', []],
                    apellidom3:   ['', []],
                    edad3:        ['', []],
                    fechan3:      ['', []],
                    note:         ['', []],
                    ciudad:       ['', [Validators.required]],
                    delegacion:   ['', []],
                    colonia:      ['', [Validators.required]],
                    calle:        ['', [Validators.required]],
                    numEx:        ['', [Validators.required]],
                    numIn:        ['', []],
                    cp:           ['', [Validators.required]],
                    movil:        ['', [Validators.required]],
                    cpr:          ['', []]
                   });
                   console.log('hola22');
                }
                if (this.childrens == 3 && this.category == 'traditional') {
                  this.myForm = this.fb.group({
                    myDate:       ['', [Validators.required]],
                    myTime:       ['', [Validators.required]],
                    timeCuidado:  ['', [Validators.required]],
                    nombre1:      ['', [Validators.required]],
                    apellidop1:   ['', [Validators.required]],
                    apellidom1:   ['', [Validators.required]],
                    edad1:        ['', [Validators.required]],
                    fechan1:      ['', [Validators.required]],
                    nombre2:      ['', [Validators.required]],
                    apellidop2:   ['', [Validators.required]],
                    apellidom2:   ['', [Validators.required]],
                    edad2:        ['', [Validators.required]],
                    fechan2:      ['', [Validators.required]],
                    nombre3:      ['', [Validators.required]],
                    apellidop3:   ['', [Validators.required]],
                    apellidom3:   ['', [Validators.required]],
                    edad3:        ['', [Validators.required]],
                    fechan3:      ['', [Validators.required]],
                    note:         ['', []],
                    ciudad:       ['', [Validators.required]],
                    delegacion:   ['', []],
                    colonia:      ['', [Validators.required]],
                    calle:        ['', [Validators.required]],
                    numEx:        ['', [Validators.required]],
                    numIn:        ['', []],
                    cp:           ['', [Validators.required]],
                    movil:        ['', [Validators.required]],
                    cpr:          ['', []]

                   });
                   console.log('hola3');
                }else if(this.childrens == 3){
                  this.myForm = this.fb.group({
                    myDate:       ['', [Validators.required]],
                    myTime:       ['', [Validators.required]],
                    timeCuidado:  ['', []],
                    nombre1:      ['', [Validators.required]],
                    apellidop1:   ['', [Validators.required]],
                    apellidom1:   ['', [Validators.required]],
                    edad1:        ['', [Validators.required]],
                    fechan1:      ['', [Validators.required]],
                    nombre2:      ['', []],
                    apellidop2:   ['', []],
                    apellidom2:   ['', []],
                    edad2:        ['', []],
                    fechan2:      ['', []],
                    nombre3:      ['', []],
                    apellidop3:   ['', []],
                    apellidom3:   ['', []],
                    edad3:        ['', []],
                    fechan3:      ['', []],
                    note:         ['', []],
                    ciudad:       ['', [Validators.required]],
                    delegacion:   ['', []],
                    colonia:      ['', [Validators.required]],
                    calle:        ['', [Validators.required]],
                    numEx:        ['', [Validators.required]],
                    numIn:        ['', []],
                    cp:           ['', [Validators.required]],
                    movil:        ['', [Validators.required]],
                    cpr:          ['', []]
                   });
                   console.log('hola33');
                }


                

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RequestformPage');
    // this.iniciarUser();
    // this.iniciarGeolocalizacion();

  }

  // iniciarUser(){
  //
  //   this.uid = this.afAuth.auth.currentUser.uid;
  //   this.usuario = this.afDB.object( 'users/' + this.uid + '/geo/' );
  //
  // }

  // iniciarGeolocalizacion(){
  //
  //   this.geolocation.getCurrentPosition().then((resp) => {
  //
  //     // resp.coords.latitude
  //     // resp.coords.longitude
  //     console.log(resp.coords);
  //
  //
  //
  //     let watch = this.geolocation.watchPosition();
  //
  //     watch.subscribe((data) => {
  //       // data can be a set of coordinates, or an error (if an error occurred).
  //       // data.coords.latitude
  //       // data.coords.longitude
  //       console.log('watch: ', data.coords );
  //
  //       this.usuario.update({lat: data.coords.latitude, lng: data.coords.longitude});
  //
  //     });
  //
  //   }).catch((error) => {
  //
  //     console.log('Error getting location', error);
  //
  //   });
  //
  //
  // }

  // count(ev: any){
  //   this.childrens = ev;
  //   console.log(this.childrens);
  // }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Tu Nanny debe ser solicitada mínimo con 3 horas de anticipación',
      buttons: ['Aceptar']
    });
    alert.present();
  }

  showAlertC() {
    /*let alert = this.alertCtrl.create({
      title: 'Por el momento solo tenemos servicio en: <br> Ciudad de México <br> y <br> Estado de México',
      buttons: ['Aceptar']
    });
    alert.present();*/
  }

  onChoseLocation(event){

    console.log(event);

    this.latitud = event.coords.lat;
    this.longitude = event.coords.lng;

  }

  //method to check if promocode is valid
  checkPromo(children){
    var revised=false;
    
    
    this.afDB.object('promoCodes/'+this.data.cpr).valueChanges().subscribe(data =>{
      this.PromoCodes = data;
    
      //if the promo discount does not exist
      if(!this.PromoCodes){
        this.discount=1;
        this.promoText="Tu codigo de promoción es incorrecto, el código es sensible a mayúsculas.";
        //console.log(this.PromoCodes + "  check promo discount " + this.discount);
        
        //if promo is empty
      }else if(this.data.cpr == ""){
        this.discount=1;
        this.promoText=" ";
        //if promo discount exists
      }else{
        //if is nanny recommendation
        if(this.PromoCodes.discount ==0){
        
          this.discount=1;
          this.promoText="Tu codigo se ha aplicado. "+this.PromoCodes.text;
          //console.log("the discount is  "+this.discount);
          

        //if it really has a discount
        }else{
          
          //console.log(this.PromoCodes);
          //if the code is unlimited apply it
          if(this.PromoCodes.availability == "unlimited"){
            this.discount=(100-this.PromoCodes.discount)/100;
            this.promoText="Tu código se ha aplicado. "+this.PromoCodes.text;
            //console.log("/////////Unlimited Promo Code");
          
          }else if(this.PromoCodes.availability == "limited"){
            //if is limited promoCode and the limit is not 0
            if(revised==false){
              if(this.PromoCodes.limit>1){
                this.discount=(100-this.PromoCodes.discount)/100;
                this.promoText="Tu código se ha aplicado. "+this.PromoCodes.text;
                //get limit 
                var limit=this.PromoCodes.limit;
                //update limit variable
                limit--;
                //update limit 
                this.afDB.database.ref('promoCodes/'+this.data.cpr+'/').update({limit});
                revised=true;

              }else{
                this.discount=(100-this.PromoCodes.discount)/100;
                this.promoText="Tu código se ha aplicado. "+this.PromoCodes.text;
                //erase promoCode
                this.afDB.database.ref('promoCodes/'+this.data.cpr+'/').remove();
                revised=true;
              }
            }

          //if the code is once it is needed to check if it was redeemed before
          }else if(this.PromoCodes.availability=="once"){
            //var promosUsed=this.UserTop.PromoCodesUsed;
            var invalid;
            
            /*console.log("length "+promosUsed.length);
            for (i=0;i<promosUsed.length;i++){
              //you used the code already
              if(promosUsed[i]==this.data.cpr){
                console.log(promosUsed[i]);
                console.log("el codigo ya fue utilizado");
                //then this discount has already redeamed and the promoCode.
                this.discount=1;
                //Text needs to change to "este código ya ha sido usado anteriormente"
                this.promoText="Este código ha sido utilizado anteriormente.";
                invalid=true;
              }*/
              console.log("availability "+this.PromoCodes.availability+"code "+this.PromoCodes.code);
              var codeText=this.PromoCodes.code;
              this.afDB.database.ref('users/').child(this.uid).child("PromoCodesUsed").child(codeText).once("value",snapshot => {
                  console.log("snapshot exists? "+snapshot.exists());
                  //console.log("ciclo para ver si existe "+ snapshot.val());
                  if (snapshot.exists()){
                    invalid=true;
                    console.log("invalid= "+invalid);
                    //console.log("el codigo ya fue utilizado");
                    //then this discount has already redeamed and the promoCode.
                    this.discount=1;
                    //Text needs to change to "este código ya ha sido usado anteriormente"
                    this.promoText="Este código ha sido utilizado anteriormente.";
                  }else{
                    invalid=false;
                    console.log("invalid= "+invalid+ "discount "+ this.PromoCodes.discount);
                    this.discount=(100-this.PromoCodes.discount)/100;
                    this.promoText="Tu código se ha aplicado. "+this.PromoCodes.text;
                    var codeData=this.PromoCodes;
                    //register to the user profile
                    this.afDB.database.ref('users'+'/'+this.codigo+'/PromoCodesUsed/'+this.data.cpr+'/').update({codeData});
                    //console.log("**************"+ this.UserTop.PromoCodesUsed);  
                  }
                
              });
              

            }
            
          }
        
        /*
          }else{
            this.afDB.object('users/'+this.codigo+"/PromoObject").valueChanges().subscribe(data =>{
              this.CodesUsed = data;
              
              //if this code is in user profile used code list
              if(this.CodesUsed){
                if(this.CodesUsed.availability=="used" && this.CodesUsed.code==this.data.cpr){
                  console.log("el codigo ya fue utilizado");
                  //then this discount has already redeamed and the promoCode.
                  this.discount=1;
                  //Text needs to change to "este código ya ha sido usado anteriormente"
                  this.promoText="Este código ha sido utilizado anteriormente.";
                //else the discount is applied and it is registered as used in user profile
                }else{
                  this.discount=(100-this.PromoCodes.discount)/100;
                  this.promoText="Tu código se ha aplicado. "+this.PromoCodes.text;
                  var data=this.PromoCodes;
                  //var availability = "used";
                  //register to the user profile
                  this.afDB.database.ref('users'+'/'+this.codigo+'/PromoCodesUsed/'+this.data.cpr+'/').update({PromoObject});
                  //this.afDB.database.ref('users'+'/'+this.codigo+'/PromoObject').update({availability});
                
                }
              }else{
                
                this.discount=(100-this.PromoCodes.discount)/100;
                this.promoText="Tu código se ha aplicado. "+this.PromoCodes.text;
                var PromoObject=this.PromoCodes;
                var availability = "used";
                //register to the user profile
                this.afDB.database.ref('users'+'/'+this.codigo+'/').update({PromoObject});
                this.afDB.database.ref('users'+'/'+this.codigo+'/PromoObject').update({availability});
                
              }
            });
          }
          */

        }
      

      console.log("descuento"+this.discount);
    if (this.category == 'dream' || this.category == 'stand'){

      this.total = this.price*this.discount;

    }else if(this.category == 'traditional' || this.category == 'special' /*&& this.data.timeCuidado != '24'*/ || this.category == 'grand'){

      this.total = this.price * this.data.timeCuidado*this.discount;
      console.log(this.total+" precio "+this.price+"*tiempo de cuidado "+this.data.timeCuidado+"*descuento "+this.discount);

    }else if(this.category == 'stand100'){
      //si aun tiene horas su paquete
      
      if(this.usuario.standHours - this.data.timeCuidado<0){
        var horasFaltantes=this.data.timeCuidado-this.usuario.standHours;
        this.total = this.price * horasFaltantes*this.discount;








      }else{
        this.total=0;
      }
    }
    console.log("total con descuento= "+ this.total);
    var flag=false;
    var i=0;
    while(flag==false){
      console.log(i++ + "total: "+this.total);
      //if(this.total != 0){

        if(children==1){
          console.log("entra a ejecutar el método");
          flag=true;
          this.addSer();
          
        }else if (children==2){
          console.log("entra a ejecutar el método");
          flag=true;
          this.addServ();
          
        }else if (children==3){
          console.log("entra a ejecutar el método");
          flag=true;
          this.addServi();
          
        }
        
      //}
    }


    });
    
}

addSer(){
  let pagado=false;
  let key = this.afDB.createPushId();
  //console.log(this.idPlayer+'Nanny');
  //console.log(this.idPlayerUser+'User');
  var data;
  var orden;
  if (this.data.genero2 == null) {
    //if (this.category == 'dream' || this.category == 'stand'){
    //  this.total = this.price*this.discount;
    //}else if(this.category == 'traditional' || this.category == 'special' /*&& this.data.timeCuidado != '24'*/ || this.category == 'grand'){

    //this.total = this.price * this.data.timeCuidado*this.discount;
    //          console.log(this.total+" costo ");
    //      }else 
    if(this.category == 'stand100'){
      //si aun tiene horas su paquete 
      if(this.usuario.standHours - this.data.timeCuidado>=0){
        pagado=true;
        var standHours=this.usuario.standHours - this.data.timeCuidado;
        this.afDB.database.ref('users/'+this.codigo+'/meta').update({standHours});
        this.total=0;
        data = {
          idUser:       this.codigo,
          idNanny:      this.nanyId,
          idPlayerUser: this.idPlayerUser,
          idPlayerNanny:this.idPlayer,
          numChil:      this.childrens,
          tarifa:       this.price,
          category:     this.category,
          costo:        this.total,
          myDate:       this.data.myDate,
          myTime:       this.data.myTime,
          timeCuidado:  this.data.timeCuidado,
          nombre1:      this.data.nombre1,
          apellidop1:   this.data.apellidop1,
          apellidom1:   this.data.apellidom1,
          edad1:        this.data.edad1,
          fechan1:      this.data.fechan1,
          note:         this.data.note,
          ciudad:       this.data.ciudad,
          delegacion:   this.data.delegacion,
          colonia:      this.data.colonia,
          calle:        this.data.calle,
          numEx:        this.data.numEx,
          numIn:        this.data.numIn,
          cp:           this.data.cp,
          movil:        this.data.movil,
          cpr:          this.data.cpr,
          status:       0,
          pago:         'payment'
        };
        orden = {
          idUser:       this.codigo,
          idNanny:      this.nanyId,
          numChil:      this.childrens,
          costo:        this.total,
          nombre1:      this.data.nombre1,
          apellidop1:   this.data.apellidop1,
          apellidom1:   this.data.apellidom1,
          edad1:        this.data.edad1,
          fechan1:      this.data.fechan1,
          ciudad:       this.data.ciudad,
          delegacion:   this.data.delegacion,
          colonia:      this.data.colonia,
          calle:        this.data.calle,
          numEx:        this.data.numEx,
          numIn:        this.data.numIn,
          cp:           this.data.cp,
          movil:        this.data.movil,
          status:       0
        };
      }
    }
    //else{
    //          var horasFaltantes=this.data.timeCuidado-this.usuario.standHours;
    //        this.total = this.price * horasFaltantes*this.discount;
    //    }
    //}
    /*else if(this.category == 'special' && this.data.timeCuidado == '24'){
    this.operacion = this.price * this.data.timeCuidado;
    this.total = this.operacion - 2780;
    }else if(this.category == 'grand' && this.data.timeCuidado != '12'){
    this.total = this.price * this.data.timeCuidado;
    }else if(this.category == 'grand' && this.data.timeCuidado == '12'){
    this.total = this.data.timeCuidado * 100;
    }*/
    if(this.category != 'stand100' || this.total>0){
      data = {
        idUser:       this.codigo,
        idNanny:      this.nanyId,
        idPlayerUser: this.idPlayerUser,
        idPlayerNanny:this.idPlayer,
        numChil:      this.childrens,
        tarifa:       this.price,
        category:     this.category,
        costo:        this.total,
        myDate:       this.data.myDate,
        myTime:       this.data.myTime,
        timeCuidado:  this.data.timeCuidado,
        nombre1:      this.data.nombre1,
        apellidop1:   this.data.apellidop1,
        apellidom1:   this.data.apellidom1,
        edad1:        this.data.edad1,
        fechan1:      this.data.fechan1,
        note:         this.data.note,
        ciudad:       this.data.ciudad,
        delegacion:   this.data.delegacion,
        colonia:      this.data.colonia,
        calle:        this.data.calle,
        numEx:        this.data.numEx,
        numIn:        this.data.numIn,
        cp:           this.data.cp,
        movil:        this.data.movil,
        cpr:          this.data.cpr,
        status:       0
      };
      orden = {
        idUser:       this.codigo,
        idNanny:      this.nanyId,
        numChil:      this.childrens,
        costo:        this.total,
        nombre1:      this.data.nombre1,
        apellidop1:   this.data.apellidop1,
        apellidom1:   this.data.apellidom1,
        edad1:        this.data.edad1,
        fechan1:      this.data.fechan1,
        ciudad:       this.data.ciudad,
        delegacion:   this.data.delegacion,
        colonia:      this.data.colonia,
        calle:        this.data.calle,
        numEx:        this.data.numEx,
        numIn:        this.data.numIn,
        cp:           this.data.cp,
        movil:        this.data.movil,
        status:       0
      };

    }
    this.afDB.database.ref('orders'+'/'+this.codigo+'/'+key).set({data});
    this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+ key).set({data});
    this.afDB.database.ref('users'+'/'+this.codigo+'/').update({orden});
    this.ServiceSoli();
    if(!pagado){
      let modal = this.modalCtrl.create( RequestPage, { 'nanyId' : this.nanyId, 'userId': this.codigo, 'key' : key, 'standHours': 0, 'cpr': this.data.cpr, 'total': this.total,'promotext':this.promoText});
      modal.present();
    }else{
      let modal = this.modalCtrl.create( ReservedPage, { 'nanyId': this.nanyId, 'userId' : this.codigo, 'total': this.total,'promotext':this.promoText } );
      modal.present();
    }
    this.navCtrl.setRoot( ReservedPage, { 'nanyId' :   this.nanyId, 'userId' : this.codigo, 'total': this.total,'promotext':this.promoText } );
  }
}

  addServ(){
    let pagado=false;
      let key = this.afDB.createPushId();
    var data;
    var orden;

      
    
      if (this.data.genero3 == null) {


        if(this.category == 'stand100'){
          //si aun tiene horas su paquete
          if(this.usuario.standHours - this.data.timeCuidado>=0){
            pagado=true;
            var standHours=this.usuario.standHours - this.data.timeCuidado;
            this.afDB.database.ref('users/'+this.codigo+'/meta').update({standHours});
            this.total=0;

            data = {
              idUser:       this.codigo,
              idNanny:      this.nanyId,
              idPlayerUser: this.idPlayerUser,
              idPlayerNanny:this.idPlayer,
              numChil:      this.childrens,
              tarifa:       this.price,
              category:     this.category,
              costo:        this.total,
              myDate:       this.data.myDate,
              myTime:       this.data.myTime,
              timeCuidado:  this.data.timeCuidado,
              nombre1:      this.data.nombre1,
              apellidop1:   this.data.apellidop1,
              apellidom1:   this.data.apellidom1,
              edad1:        this.data.edad1,
              fechan1:      this.data.fechan1,
              note:         this.data.note,
              ciudad:       this.data.ciudad,
              delegacion:   this.data.delegacion,
              colonia:      this.data.colonia,
              calle:        this.data.calle,
              numEx:        this.data.numEx,
              numIn:        this.data.numIn,
              cp:           this.data.cp,
              movil:        this.data.movil,
              cpr:          this.data.cpr,
              status:       0,
              pago:         'payment'
            };
  
            orden = {
              idUser:       this.codigo,
              idNanny:      this.nanyId,
              numChil:      this.childrens,
              costo:        this.total,
              nombre1:      this.data.nombre1,
              apellidop1:   this.data.apellidop1,
              apellidom1:   this.data.apellidom1,
              edad1:        this.data.edad1,
              fechan1:      this.data.fechan1,
              ciudad:       this.data.ciudad,
              delegacion:   this.data.delegacion,
              colonia:      this.data.colonia,
              calle:        this.data.calle,
              numEx:        this.data.numEx,
              numIn:        this.data.numIn,
              cp:           this.data.cp,
              movil:        this.data.movil,
              status:       0
            };
          }
        }
        if(this.category != 'stand100' || this.total>0){
          data = {
            idUser:       this.codigo,
            idNanny:      this.nanyId,
            idPlayerUser: this.idPlayerUser,
            idPlayerNanny:this.idPlayer,
            numChil:      this.childrens,
            tarifa:       this.price,
            category:     this.category,
            costo:        this.total,
            myDate:       this.data.myDate,
            myTime:       this.data.myTime,
            timeCuidado:  this.data.timeCuidado,
            nombre1:      this.data.nombre1,
            apellidop1:   this.data.apellidop1,
            apellidom1:   this.data.apellidom1,
            edad1:        this.data.edad1,
            fechan1:      this.data.fechan1,
            nombre2:      this.data.nombre2,
            apellidop2:   this.data.apellidop2,
            apellidom2:   this.data.apellidom2,
            edad2:        this.data.edad2,
            fechan2:      this.data.fechan2,
            note:         this.data.note,
            ciudad:       this.data.ciudad,
            delegacion:   this.data.delegacion,
            colonia:      this.data.colonia,
            calle:        this.data.calle,
            numEx:        this.data.numEx,
            numIn:        this.data.numIn,
            cp:           this.data.cp,
            movil:        this.data.movil,
            cpr:          this.data.cpr,
            status:       0
          };

          orden = {
            idUser:       this.codigo,
            idNanny:      this.nanyId,
            numChil:      this.childrens,
            costo:        this.total,
            nombre1:      this.data.nombre1,
            apellidop1:   this.data.apellidop1,
            apellidom1:   this.data.apellidom1,
            edad1:        this.data.edad1,
            fechan1:      this.data.fechan1,
            nombre2:      this.data.nombre2,
            apellidop2:   this.data.apellidop2,
            apellidom2:   this.data.apellidom2,
            edad2:        this.data.edad2,
            fechan2:      this.data.fechan2,
            ciudad:       this.data.ciudad,
            delegacion:   this.data.delegacion,
            colonia:      this.data.colonia,
            calle:        this.data.calle,
            numEx:        this.data.numEx,
            numIn:        this.data.numIn,
            cp:           this.data.cp,
            movil:        this.data.movil,
            status:       0
          };
        }

          this.afDB.database.ref('orders'+'/'+this.codigo+'/'+key).set({data});

          this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+ key).set({data});

          this.afDB.database.ref('users'+'/'+this.codigo+'/').update({orden});


          this.ServiceSoli();
          if(!pagado){
            let modal = this.modalCtrl.create( RequestPage, { 'nanyId' : this.nanyId, 'userId': this.codigo, 'key' : key, 'standHours': 0 , 'cpr': this.data.cpr, 'total': this.total,'promotext':this.promoText});
            modal.present();
          }else{
            let modal = this.modalCtrl.create( ReservedPage, { 'nanyId': this.nanyId, 'userId' : this.codigo, 'total': this.total,'promotext':this.promoText } );
            modal.present();
          }
          this.navCtrl.setRoot( ReservedPage, { 'nanyId' :   this.nanyId, 'userId' : this.codigo, 'total': this.total,'promotext':this.promoText } );

      }
  }

  addServi(){
    let pagado=false;
      let key = this.afDB.createPushId();
    var data;
    var orden;

      if (this.data.genero4 == null) {


        if(this.category == 'stand100'){
          //si aun tiene horas su paquete
          if(this.usuario.standHours - this.data.timeCuidado>=0){
            pagado=true;
            var standHours=this.usuario.standHours - this.data.timeCuidado;
            this.afDB.database.ref('users/'+this.codigo+'/meta').update({standHours});
            this.total=0;

            data = {
              idUser:       this.codigo,
              idNanny:      this.nanyId,
              idPlayerUser: this.idPlayerUser,
              idPlayerNanny:this.idPlayer,
              numChil:      this.childrens,
              tarifa:       this.price,
              category:     this.category,
              costo:        this.total,
              myDate:       this.data.myDate,
              myTime:       this.data.myTime,
              timeCuidado:  this.data.timeCuidado,
              nombre1:      this.data.nombre1,
              apellidop1:   this.data.apellidop1,
              apellidom1:   this.data.apellidom1,
              edad1:        this.data.edad1,
              fechan1:      this.data.fechan1,
              note:         this.data.note,
              ciudad:       this.data.ciudad,
              delegacion:   this.data.delegacion,
              colonia:      this.data.colonia,
              calle:        this.data.calle,
              numEx:        this.data.numEx,
              numIn:        this.data.numIn,
              cp:           this.data.cp,
              movil:        this.data.movil,
              cpr:          this.data.cpr,
              status:       0,
              pago:         'payment'
            };
  
            orden = {
              idUser:       this.codigo,
              idNanny:      this.nanyId,
              numChil:      this.childrens,
              costo:        this.total,
              nombre1:      this.data.nombre1,
              apellidop1:   this.data.apellidop1,
              apellidom1:   this.data.apellidom1,
              edad1:        this.data.edad1,
              fechan1:      this.data.fechan1,
              ciudad:       this.data.ciudad,
              delegacion:   this.data.delegacion,
              colonia:      this.data.colonia,
              calle:        this.data.calle,
              numEx:        this.data.numEx,
              numIn:        this.data.numIn,
              cp:           this.data.cp,
              movil:        this.data.movil,
              status:       0
            };
          }
        }
        
        if(this.category != 'stand100' || this.total>0){
          data = {
            idUser:       this.codigo,
            idNanny:      this.nanyId,
            idPlayerUser: this.idPlayerUser,
            idPlayerNanny:this.idPlayer,
            numChil:      this.childrens,
            tarifa:       this.price,
            category:     this.category,
            costo:        this.total,
            myDate:       this.data.myDate,
            myTime:       this.data.myTime,
            timeCuidado:  this.data.timeCuidado,
            nombre1:      this.data.nombre1,
            apellidop1:   this.data.apellidop1,
            apellidom1:   this.data.apellidom1,
            edad1:        this.data.edad1,
            fechan1:      this.data.fechan1,
            nombre2:      this.data.nombre2,
            apellidop2:   this.data.apellidop2,
            apellidom2:   this.data.apellidom2,
            edad2:        this.data.edad2,
            fechan2:      this.data.fechan2,
            nombre3:      this.data.nombre3,
            apellidop3:   this.data.apellidop3,
            apellidom3:   this.data.apellidom3,
            edad3:        this.data.edad3,
            fechan3:      this.data.fechan3,
            note:         this.data.note,
            ciudad:       this.data.ciudad,
            delegacion:   this.data.delegacion,
            colonia:      this.data.colonia,
            calle:        this.data.calle,
            numEx:        this.data.numEx,
            numIn:        this.data.numIn,
            cp:           this.data.cp,
            movil:        this.data.movil,
            cpr:          this.data.cpr,
            status:       0
          }

          orden = {
            idUser:       this.codigo,
            idNanny:      this.nanyId,
            numChil:      this.childrens,
            costo:        this.total,
            nombre1:      this.data.nombre1,
            apellidop1:   this.data.apellidop1,
            apellidom1:   this.data.apellidom1,
            edad1:        this.data.edad1,
            fechan1:      this.data.fechan1,
            nombre2:      this.data.nombre2,
            apellidop2:   this.data.apellidop2,
            apellidom2:   this.data.apellidom2,
            edad2:        this.data.edad2,
            fechan2:      this.data.fechan2,
            nombre3:      this.data.nombre3,
            apellidop3:   this.data.apellidop3,
            apellidom3:   this.data.apellidom3,
            edad3:        this.data.edad3,
            fechan3:      this.data.fechan3,
            ciudad:       this.data.ciudad,
            delegacion:   this.data.delegacion,
            colonia:      this.data.colonia,
            calle:        this.data.calle,
            numEx:        this.data.numEx,
            numIn:        this.data.numIn,
            cp:           this.data.cp,
            movil:        this.data.movil,
            status:       0
          };

        }
          this.afDB.database.ref('orders'+'/'+this.codigo+'/'+key).set({data});

          this.afDB.database.ref('orders_n'+'/'+this.nanyId+'/'+ key).set({data});

          this.afDB.database.ref('users'+'/'+this.codigo+'/').update({orden});


          this.ServiceSoli();
        if(!pagado){
          let modal = this.modalCtrl.create( RequestPage, { 'nanyId' : this.nanyId, 'userId': this.codigo, 'key' : key, 'standHours': 0, 'cpr': this.data.cpr, 'total': this.total ,'promotext':this.promoText});
          modal.present();
        }else{
          let modal = this.modalCtrl.create( ReservedPage, { 'nanyId': this.nanyId, 'userId' : this.codigo, 'total': this.total,'promotext':this.promoText } );
          modal.present();
        }
          this.navCtrl.setRoot( ReservedPage, { 'nanyId' :   this.nanyId, 'userId' : this.codigo, 'total': this.total,'promotext':this.promoText } );


      }
  }

  ServiceSoli(){
      this.afDB.object('users/'+this.uid+'/'+'meta/').valueChanges().subscribe(data =>{
        this.data = data;
        if(! this.data){
          this.data={}
        }
        let noti =  {
                      app_id: "2036276e-adf8-46b2-8040-de0783b2956e",
                      include_player_ids: [this.idPlayer],
                      data: {"foo": "bar"},
                      contents: {en: "Nueva solicitud de cuidado de " + this.data.displayName},
                    }

        window["plugins"].OneSignal.postNotification(noti,
          function(successResponse) {
              console.log("Notification Post Success:", successResponse);
          },
          function (failedResponse) {
              console.log("Notification Post Failed: ", failedResponse);
              alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
          }
        );
        console.log(this.data.displayName);
      });


  }

}
