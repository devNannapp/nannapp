import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComprobantePage } from './comprobante';

@NgModule({
  declarations: [
    ComprobantePage,
  ],
  imports: [
    IonicPageModule.forChild(ComprobantePage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class ComprobantePageModule {}
