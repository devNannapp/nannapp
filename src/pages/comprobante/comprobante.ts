import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-comprobante',
  templateUrl: 'comprobante.html',
})
export class ComprobantePage {

  data:any = {};
  userId:any = {};
  dataUser:any = {};
  imagenCom: any;
  imagenCom64: string;
  typeUser: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public afDB: AngularFireDatabase,
              public afAuth:AngularFireAuth,
              public camera: Camera,
              public toastCtrl: ToastController) {

                this.userId = this.navParams.get("userId");
                this.typeUser = this.afAuth.auth.currentUser.uid;

                this.afDB.object('users/'+this.userId+'/'+'info/comprobante').valueChanges().subscribe(data =>{
                  this.data = data;
                  if(! this.data){
                    this.data = false;
                  }
                  console.log(this.data);
                });

                this.afDB.object('users/'+this.typeUser+'/meta/user').valueChanges().subscribe(data =>{
                  this.dataUser = data;
                  if(! this.dataUser){
                    this.dataUser = false;
                  }
                  console.log(this.dataUser, 'que es esto');
                });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComprobantePage');
  }

  close(){
    this.viewCtrl.dismiss();
  }

  mostrar_camara(){

    this.camera.getPicture({
      quality : 100,
      destinationType : this.camera.DestinationType.DATA_URL,
      sourceType : this.camera.PictureSourceType.CAMERA,
      allowEdit : true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 720,
      targetHeight: 720,
      saveToPhotoAlbum: true
    }).then(foto => {

            this.mostrar_toast('Subiendo archivo');

            let nombreArchivo:string = new Date().valueOf().toString();
            let fotoref = firebase.storage().ref('usuarios/comprobante/'+nombreArchivo);
            fotoref.putString(foto, 'base64', {contentType: 'image/jpg'}).then(foto_guardad => {
              firebase.database().ref('users/'+this.userId+'/info').update({comprobante: foto_guardad.downloadURL});
              this.mostrar_toast('Archivo subido con exito');
            });
    }, error => {
      // Log an error to the console if something goes wrong.
      console.log("ERROR -> " + JSON.stringify(error));
    });

  }

  seleccionar_foto(){

    this.camera.getPicture({
      quality : 100,
      destinationType : this.camera.DestinationType.DATA_URL,
      sourceType : this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit : true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 720,
      targetHeight: 720,
      saveToPhotoAlbum: true
    }).then(foto => {

            this.mostrar_toast('Subiendo archivo');

            let nombreArchivo:string = new Date().valueOf().toString();
            let fotoref = firebase.storage().ref('usuarios/comprobante/'+nombreArchivo);
            fotoref.putString(foto, 'base64', {contentType: 'image/jpg'}).then(foto_guardad => {
              firebase.database().ref('usuarios/'+this.userId+'/info').update({comprobante: foto_guardad.downloadURL});

              this.mostrar_toast('Archivo subido con exito');
            });
    }, error => {
      // Log an error to the console if something goes wrong.
      console.log("ERROR -> " + JSON.stringify(error));
    });

  }

  mostrar_toast( mensaje: string ){

    this.toastCtrl.create({
      message: mensaje,
      duration: 2000
    }).present();

  }

}
