import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IdPage } from './id';

@NgModule({
  declarations: [
    IdPage,
  ],
  imports: [
    IonicPageModule.forChild(IdPage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class IdPageModule {}
